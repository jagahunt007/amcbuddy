package com.amcbuddy.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


@Entity  
@Table(name="amc_leadfollowup")
public class Leads {

	
	@Id
	@Column(name="followup_id")
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
	
	private int  followup_id;
	public int getFollowup_id() {
		return followup_id;
	}
	public void setFollowup_id(int followup_id) {
		this.followup_id = followup_id;
	}


	public String getLead_status() {
		return lead_status;
	}
	public void setLead_status(String lead_status) {
		this.lead_status = lead_status;
	}
	public String getNext_follwupdate() {
		return next_follwupdate;
	}
	public void setNext_follwupdate(String next_follwupdate) {
		this.next_follwupdate = next_follwupdate;
	}
	private String lead_status;
	private String next_follwupdate;
	public String imagepath;
	
	public String getImagepath() {
		return imagepath;
	}
	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}
	@OneToOne(targetEntity=Enquiry.class,cascade=CascadeType.ALL)
	@JoinColumn(name="enquiry_id", referencedColumnName="enquiry_id")
    private Enquiry enquiry;
	
	
	public Enquiry getEnquiry() {
		return enquiry;
	}
	public void setEnquiry(Enquiry enquiry) {
		this.enquiry = enquiry;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public ServiceMaster getServiceMaster() {
		return serviceMaster;
	}
	public void setServiceMaster(ServiceMaster serviceMaster) {
		this.serviceMaster = serviceMaster;
	}
	@OneToOne(targetEntity=Role.class,cascade=CascadeType.ALL)
	@JoinColumn(name="role_id", referencedColumnName="role_id")
    private Role role;
	
	@OneToOne(targetEntity=ServiceMaster.class,cascade=CascadeType.ALL)
	@JoinColumn(name="service_id", referencedColumnName="service_id")
    private ServiceMaster serviceMaster;
	

}
