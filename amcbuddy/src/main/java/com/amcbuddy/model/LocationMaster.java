package com.amcbuddy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

@Entity
@Table(name="amc_location_master")
//@Cache(usage=CacheConcurrencyStrategy.READ_ONLY,region="hbentity")
public class LocationMaster 
{
	
	@Id
	@Column(name="location_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer locationid;
	
	
	public Integer getLocationid() {
		return locationid;
	}

	public void setLocationid(Integer locationid) {
		this.locationid = locationid;
	}

	@Override
	public String toString() {
		return "Location [locationid=" + locationid + ", locationname="
				+ locationname + ", pincode=" + pincode +
				 ", status=" + status + ", created_by=" + created_by
				+ ", modified_by=" + modified_by + ", createdOn=" + createdOn
				+ ", modifiedOn=" + modifiedOn + "]";
	}

	@Column(name="locationname")
	private String locationname;
	
	
	@Column(name="pincode")
	private long pincode; 
	
	
	
	
	public long getPincode() {
		return pincode;
	}

	public void setPincode(long pincode) {
		this.pincode = pincode;
	}

	@Column(name="locationstatus")
	private String status;
	
	@Column(name="created_by")
	private String created_by;

	@Column(name="modified_by")
	private String modified_by;
	
	@Column(name="created_date")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime createdOn;
	
	@Column(name="modified_date")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime modifiedOn;

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getModified_by() {
		return modified_by;
	}

	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDateTime getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(LocalDateTime modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	

	public String getLocationname() {
		return locationname;
	}

	public void setLocationname(String locationname) {
		this.locationname = locationname;
	}


	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	

}
