package com.amcbuddy.model;


	import java.io.Serializable;
	import java.util.List;

	import javax.persistence.CascadeType;
	import javax.persistence.Column;
	import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.GenerationType;
	import javax.persistence.Id;
	import javax.persistence.JoinColumn;
	import javax.persistence.JoinTable;
	import javax.persistence.OneToMany;
	import javax.persistence.OneToOne;
	import javax.persistence.Table;
	
	@Entity  
	@Table(name="amc_roles")
	//@Cache(usage=CacheConcurrencyStrategy.READ_ONLY,region="hbentity")
	public class Role implements Serializable{
		
		 /**
		 * 
		 */
		private static final long serialVersionUID = -287969975617168055L;
		
		
		
		@Id
		 @Column(name="role_id")
		@GeneratedValue(strategy=GenerationType.AUTO)
		  private int role_id;
		
		  private String role_name;
		  private String role_type;
		  private String description;
		   private String is_role;
		  private String status;
		
		
		
		
		
		
		public int getRole_id() {
			return role_id;
		}
		 
		 
		public void setRole_id(int role_id) {
			this.role_id = role_id;
		}
		
		public String getRole_name() {
			return role_name;
		}


		public void setRole_name(String role_name) {
			this.role_name = role_name;
		}


		
		
		  public String getRole_type() {
			return role_type;
		}
		public void setRole_type(String role_type) {
			this.role_type = role_type;
		}
		public String getDescription() {
			return description;
		}


		public void setDescription(String description) {
			this.description = description;
		}
		
		  
		  public String getIs_role() {
			return is_role;
		}


		public void setIs_role(String is_role) {
			this.is_role = is_role;
		}
		


		public String getStatus() {
			return status;
		}


		public void setStatus(String status) {
			this.status = status;
		}


		@OneToMany()  
	    @JoinTable(name="amc_admin_module_section_access",  
	    joinColumns={@JoinColumn(name="role_id", referencedColumnName="role_id")},  
	    inverseJoinColumns={@JoinColumn(name="section_id", referencedColumnName="section_id")}) 
	    private List<AdminSection> adminSection;
		public List<AdminSection> getAdminSection() {
			return adminSection;
		}


		public void setAdminSection(List<AdminSection> adminSection) {
			this.adminSection = adminSection;
		}
		
		@OneToMany( )  
	    @JoinTable(name="amc_admin_module_access",  
	    joinColumns={@JoinColumn(name="role_id", referencedColumnName="role_id")},  
	    inverseJoinColumns={@JoinColumn(name="module_id", referencedColumnName="module_id")}) 
	    private List<AdminModule> adminModule;
		public List<AdminModule> getAdminModule() {
			return adminModule;
		}


		public void setAdminModule(List<AdminModule> adminModule) {
			this.adminModule = adminModule;
		}
	        
		@OneToMany(cascade=CascadeType.ALL)  
		  @JoinTable(name="amc_registration_role",  
		  joinColumns={@JoinColumn(name="role_id", referencedColumnName="role_id")},  
		  inverseJoinColumns={@JoinColumn(name="backend_user_id", referencedColumnName="backend_user_id")})  
	      private List<BackendUsers> backendusers;
		  
		  public List<BackendUsers> getBackendusers() {
			return backendusers;
		}


		public void setBackendusers(List<BackendUsers> backendusers) {
			this.backendusers = backendusers;
		}

	}

