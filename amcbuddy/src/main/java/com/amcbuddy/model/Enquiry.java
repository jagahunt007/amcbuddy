package com.amcbuddy.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

@Entity  
@Table(name="amc_enquiries")
public class Enquiry implements Serializable{
	
	@Override
	public String toString() {
		return "Enquiry [enquiry_id=" + enquiry_id + ", enquiry_name="
				+ enquiry_name + ", enq_mobilenumber=" + enq_mobilenumber
				+ ", enq_email=" + enq_email + ", enq_address=" + enq_address
				+ ", enq_followupdate=" + enq_followupdate
				+ ", enq_description=" + enq_description + ", created_by="
				+ created_by + ", modified_by=" + modified_by + ", role="
				+ role + ", serviceMaster=" + serviceMaster + "]";
	}
	@Id
	@Column(name="enquiry_id")
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
	
	private int enquiry_id;
	private String  enquiry_name;
	private String enq_mobilenumber;
	private String enq_email;
	private  String enq_address;
	private java.sql.Date enq_followupdate;
	private String enq_description;
	private String is_lead;
	

	
	
	public String getIs_lead() {
		return is_lead;
	}
	public void setIs_lead(String is_lead) {
		this.is_lead = is_lead;
	}
	public String getEnq_description() {
		return enq_description;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public void setEnq_description(String enq_description) {
		this.enq_description = enq_description;
	}
	public java.sql.Date getEnq_followupdate() {
		return enq_followupdate;
	}
	public void setEnq_followupdate(java.sql.Date enq_followupdate) {
		this.enq_followupdate = enq_followupdate;
	}
	public int getEnquiry_id() {
		return enquiry_id;
	}
	public void setEnquiry_id(int enquiry_id) {
		this.enquiry_id = enquiry_id;
	}
	public String getEnquiry_name() {
		return enquiry_name;
	}
	public void setEnquiry_name(String enquiry_name) {
		this.enquiry_name = enquiry_name;
	}
	public String getEnq_mobilenumber() {
		return enq_mobilenumber;
	}
	public void setEnq_mobilenumber(String enq_mobilenumber) {
		this.enq_mobilenumber = enq_mobilenumber;
	}
	public String getEnq_email() {
		return enq_email;
	}
	public void setEnq_email(String enq_email) {
		this.enq_email = enq_email;
	}
	public String getEnq_address() {
		return enq_address;
	}
	public void setEnq_address(String enq_address) {
		this.enq_address = enq_address;
	}

	public LocalDateTime getCreated_by() {
		return created_by;
	}
	public void setCreated_by(LocalDateTime created_by) {
		this.created_by = created_by;
	}
	public LocalDateTime getModified_by() {
		return modified_by;
	}
	public void setModified_by(LocalDateTime modified_by) {
		this.modified_by = modified_by;
	}
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime created_by;
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime modified_by;
	@OneToOne(targetEntity=Role.class,cascade=CascadeType.ALL)
	@JoinColumn(name="role_id", referencedColumnName="role_id")
    private Role role;
	
	@OneToOne(targetEntity=ServiceMaster.class,cascade=CascadeType.ALL)
	@JoinColumn(name="service_id", referencedColumnName="service_id")
    private ServiceMaster serviceMaster;


	public ServiceMaster getServiceMaster() {
		return serviceMaster;
	}
	public void setServiceMaster(ServiceMaster serviceMaster) {
		this.serviceMaster = serviceMaster;
	}
	
}
