package com.amcbuddy.model;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity  
@Table(name="amc_admin_modules")

public class AdminModule implements Serializable{
	
	
	@Id
	@Column(name="module_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int module_id;
	
	private String modulename;
	private String module_status;
	
	public String getModule_status() {
		return module_status;
	}

	public void setModule_status(String module_status) {
		this.module_status = module_status;
	}

	

	public int getModule_id() {
		return module_id;
	}

	public void setModule_id(int module_id) {
		this.module_id = module_id;
	}

	public String getModulename() {
		return modulename;
	}

	public void setModulename(String modulename) {
		this.modulename = modulename;
	}

	public List<AdminSection> getAdminsection() {
		return adminsection;
	}

	public void setAdminsection(List<AdminSection> adminsection) {
		this.adminsection = adminsection;       
	}

	@OneToMany(targetEntity=AdminSection.class)
	@JoinColumn(name="module_id", referencedColumnName="module_id")
    private List<AdminSection> adminsection;
	
	@Override
	public boolean equals(Object obj) {
	    boolean r=false;
	    if(obj instanceof AdminModule)
	    {
	        AdminModule temp = (AdminModule) obj;
	        if(this.module_id == temp.module_id && this.modulename== temp.modulename && this.module_status == temp.module_status && this.adminsection == temp.adminsection){
	            r=true;
	        }
	        else{
	        	r=false;
	        }
	    
	    }
	    System.out.println("r value is"+r);
	    return r;
	}
	@Override
	public int hashCode() {
	    // TODO Auto-generated method stub

	    return (this.modulename.hashCode() + this.module_status.hashCode() + this.adminsection.hashCode());        
	}
	
	
	
	

}
