package com.amcbuddy.model;




import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


@Entity  
@Table(name="amc_deal")
public class Deal {



	
	@Id
	@Column(name="deal_id")
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
	
	private int  deal_id;
	private String deal_status;
	public int getDeal_id() {
		return deal_id;
	}
	public void setDeal_id(int deal_id) {
		this.deal_id = deal_id;
	}
	public String getDeal_status() {
		return deal_status;
	}
	public void setDeal_status(String deal_status) {
		this.deal_status = deal_status;
	}

	
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public ServiceMaster getServiceMaster() {
		return serviceMaster;
	}
	public void setServiceMaster(ServiceMaster serviceMaster) {
		this.serviceMaster = serviceMaster;
	}
	@OneToOne(targetEntity=Role.class,cascade=CascadeType.ALL)
	@JoinColumn(name="role_id", referencedColumnName="role_id")
    private Role role;
	
	@OneToOne(targetEntity=ServiceMaster.class,cascade=CascadeType.ALL)
	@JoinColumn(name="service_id", referencedColumnName="service_id")
    private ServiceMaster serviceMaster;
	
	@OneToOne(targetEntity=Enquiry.class,cascade=CascadeType.ALL)
	@JoinColumn(name="enquiry_id", referencedColumnName="enquiry_id")
    private Enquiry enquiry;
	
	
	public Enquiry getEnquiry() {
		return enquiry;
	}
	public void setEnquiry(Enquiry enquiry) {
		this.enquiry = enquiry;
	}
}



