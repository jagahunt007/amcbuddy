package com.amcbuddy.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;


	@Entity  
	@Table(name="amc_registration")
	public class Registration {
		
		@Id
		@Column(name="reg_id")
		@GenericGenerator(name = "generator", strategy = "increment")
		@GeneratedValue(generator = "generator")
		private int reg_id;
		
		
		private String username;
		private String email;
		private String phonenumber;
		
		
		@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
		private LocalDateTime created_by;
		@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
		private LocalDateTime modified_by;
		

		private String address;
		
		private String unique_id;
		
		private String category_name;
		
		private int subcat_id;
		
		
		
		



		private String followup_date;
		public String getFollowup_date() {
			return followup_date;
		}
		public void setFollowup_date(String followup_date) {
			this.followup_date = followup_date;
		}
	
	
		public LocalDateTime getCreated_by() {
			return created_by;
		}
		public void setCreated_by(LocalDateTime created_by) {
			this.created_by = created_by;
		}
		public LocalDateTime getModified_by() {
			return modified_by;
		}
		public void setModified_by(LocalDateTime modified_by) {
			this.modified_by = modified_by;
		}
		public int getReg_id() {
			return reg_id;
		}
		public void setReg_id(int reg_id) {
			this.reg_id = reg_id;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		
		
		public String getPhonenumber() {
			return phonenumber;
		}
		public void setPhonenumber(String phonenumber) {
			this.phonenumber = phonenumber;
		}
		
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getCategory_name() {
			return category_name;
		}
		public void setCategory_name(String category_name) {
			this.category_name = category_name;
		}
		public int getSubcat_id() {
			return subcat_id;
		}
		public void setSubcat_id(int subcat_id) {
			this.subcat_id = subcat_id;
		}
		
		public String getUnique_id() {
			return unique_id;
		}
		public void setUnique_id(String unique_id) {
			this.unique_id = unique_id;
		}






	
	

}
