package com.amcbuddy.model;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity  
@Table(name="amc_admin_module_section")  
//@Cache(usage=CacheConcurrencyStrategy.READ_ONLY,region="hbentity")
public class AdminSection implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="section_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int section_id;
	@Override
	public String toString() {
		return "AdminSection [section_id=" + section_id + "]";
	}


	public int getSection_id() {
		return section_id;
	}


	public void setSection_id(int section_id) {
		this.section_id = section_id;
	}


	public String getSectionname() {
		return sectionname;
	}


	public void setSectionname(String sectionname) {
		this.sectionname = sectionname;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}

    


	private String sectionname;
	private String url;
	


	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToOne
	@JoinColumn(name = "module_id")

	private AdminModule menuo=new AdminModule();
	public AdminModule getMenuo() {
		return menuo;
	}


	public void setMenuo(AdminModule menuo) {
		this.menuo = menuo;
		
		
	}
	

	
	

}
