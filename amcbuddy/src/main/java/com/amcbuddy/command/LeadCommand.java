package com.amcbuddy.command;


public class LeadCommand {
	
private int followup_id;
private String status;
private String followup_date;
public int getFollowup_id() {
	return followup_id;
}
public void setFollowup_id(int followup_id) {
	this.followup_id = followup_id;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getFollowup_date() {
	return followup_date;
}
public void setFollowup_date(String followup_date) {
	this.followup_date = followup_date;
}

}
