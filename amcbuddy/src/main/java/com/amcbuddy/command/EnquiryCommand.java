package com.amcbuddy.command;


import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.springframework.web.multipart.MultipartFile;

public class EnquiryCommand {
	private int enq_id;

	private int deal_id;
	public int getDeal_id() {
		return deal_id;
	}

	public void setDeal_id(int deal_id) {
		this.deal_id = deal_id;
	}
	private int lead_id;
	public int getLead_id() {
		return lead_id;
	}

	public void setLead_id(int lead_id) {
		this.lead_id = lead_id;
	}
	private String name;
	private String email;
	private String phonenumber;
	private String address;

	private int assignedrole;
	private Long amount;
	private String imagepath;

	public String getImagepath() {
		return imagepath;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}
	public MultipartFile getMultipart() {
		return multipart;
	}
	public void setMultipart(MultipartFile multipart) {
		this.multipart = multipart;
	}
	private int service;
	private String status;
	private MultipartFile multipart;
	private MultipartFile paymentmultipart;
	private MultipartFile agreementmultipart;
	public MultipartFile getPaymentmultipart() {
		return paymentmultipart;
	}
	public void setPaymentmultipart(MultipartFile paymentmultipart) {
		this.paymentmultipart = paymentmultipart;
	}
	public MultipartFile getAgreementmultipart() {
		return agreementmultipart;
	}
	public void setAgreementmultipart(MultipartFile agreementmultipart) {
		this.agreementmultipart = agreementmultipart;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getService() {
		return service;
	}
	public void setService(int service) {
		this.service = service;
	}
	public int getAssignedrole() {
		return assignedrole;
	}
	public void setAssignedrole(int assignedrole) {
		this.assignedrole = assignedrole;
	}
	private String description;
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime created_by;
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime modified_by;
	public LocalDateTime getCreated_by() {
		return created_by;
	}
	public void setCreated_by(LocalDateTime created_by) {
		this.created_by = created_by;
	}
	public LocalDateTime getModified_by() {
		return modified_by;
	}
	public void setModified_by(LocalDateTime modified_by) {
		this.modified_by = modified_by;
	}
	private String followup_date;
	public String getFollowup_date() {
		return followup_date;
	}
	public void setFollowup_date(String followup_date) {
		this.followup_date = followup_date;
	}
	
	public int getEnq_id() {
		return enq_id;
	}
	public void setEnq_id(int enq_id) {
		this.enq_id = enq_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

}

