package com.amcbuddy.command;

import java.util.List;

import com.amcbuddy.model.AdminSection;

public class AdminModuleCommand {
	
	public AdminModuleCommand()
	{
		
		
		System.out.println("AdminModuleCommand class------------------");
	}
	
	
    private int module_id;
	
	private String modulename;
	
	private String module_status;
	
	private String sectionname;
	private String url;
	private int section_id;
	
	private List<AdminSection> adminSection;

	public String getSectionname() {
		return sectionname;
	}

	public void setSectionname(String sectionname) {
		this.sectionname = sectionname;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getSection_id() {
		return section_id;
	}

	public void setSection_id(int section_id) {
		this.section_id = section_id;
	}

	public int getModule_id() {
		return module_id;
	}

	public void setModule_id(int module_id) {
		this.module_id = module_id;
	}

	public String getModulename() {
		return modulename;
	}

	public void setModulename(String modulename) {
		this.modulename = modulename;
	}

	public String getModule_status() {
		return module_status;
	}

	public void setModule_status(String module_status) {
		this.module_status = module_status;
	}

	public List<AdminSection> getAdminSection() {
		return adminSection;
	}

	public void setAdminSection(List<AdminSection> adminSection) {
		this.adminSection = adminSection;
	}
	
	
	


}
