package com.amcbuddy.serviceImpl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.amcbuddy.command.EnquiryCommand;
import com.amcbuddy.command.LeadCommand;
import com.amcbuddy.dao.CrmDao;
import com.amcbuddy.model.Deal;
import com.amcbuddy.model.Enquiry;
import com.amcbuddy.model.Leads;

import com.amcbuddy.model.Role;
import com.amcbuddy.model.ServiceMaster;
import com.amcbuddy.service.CrmService;


@Lazy
@Service
@Transactional


public class CrmServiceImpl implements CrmService{

	@Autowired
	private CrmDao crmdao;

	@Override
	public boolean saveEnquiry(EnquiryCommand enquiry) {
		return crmdao.saveEnquiry(enquiry) ;
	}

	@Override
	public List<Enquiry> getAllEnquiries() {
		return crmdao.getAllEnquiries();
	}

	@Override
	public Enquiry editenquiry(int enquiryId) {
		return crmdao.editenquiry(enquiryId);
	}

	

	@Override
	public Enquiry leads(int enquiryId) {
		return crmdao.leads(enquiryId);
	}

	@Override
	public List<ServiceMaster> getServices() {
		return crmdao.getServices();
	}

	@Override
	public boolean saveLead(Leads lead) {
		return crmdao.saveLead(lead);
	}

	@Override
	public List<Leads> getAllLeads() {
		return crmdao.getAllLeads();
	}

	@Override
	public List<Role> getRoles() {
		return crmdao.getRoles();
	}

	@Override
	public boolean saveConvertedLead(EnquiryCommand enquiry) {
		return crmdao.saveConvertedLead(enquiry);
	}


	@Override
	public Enquiry deals(int enquiryId) {
		return crmdao.deals(enquiryId);
	}

	@Override
	public boolean update(EnquiryCommand command) {
		return crmdao.update(command);
	}


	@Override
	public boolean saveConverteddeal(EnquiryCommand enquiry) {
		return crmdao.saveConverteddeal(enquiry);
	}

	@Override
	public List<Deal> getAllDeals() {
		return crmdao.getAllDeals();
	}




	
}
