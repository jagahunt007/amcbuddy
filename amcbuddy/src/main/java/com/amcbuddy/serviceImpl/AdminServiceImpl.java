package com.amcbuddy.serviceImpl;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.amcbuddy.service.AdminService;

import java.util.List;
import java.util.Map;

	import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

	import com.amcbuddy.command.AdminModuleCommand;
import com.amcbuddy.dao.AdminDao;
import com.amcbuddy.model.AdminModule;
import com.amcbuddy.model.AdminSection;
import com.amcbuddy.model.BackendUsers;
import com.amcbuddy.model.CategoryMaster;
import com.amcbuddy.model.LocationMaster;
import com.amcbuddy.model.Role;
import com.amcbuddy.model.ServiceMaster;
import com.amcbuddy.service.AdminService;

	@Lazy
	@Service
	@Transactional

	public class AdminServiceImpl implements AdminService{
		@Autowired
		private AdminDao adminDao;
		
		
	/*Module viewing,saving,editing and deleting*/
		public List<AdminModule> getadminmodules() {
			 
			return adminDao.getadminmodules();
		}

		public void savemodule(AdminModuleCommand amcommand) {
			
			adminDao.savemodule(amcommand);
		}
		@Override
		public AdminModule getModule(int mid) {
			
			return adminDao.getModule(mid);
		}

		@Override
		public void updatemodule(AdminModuleCommand amcommand) {
			
			adminDao.updatemodule(amcommand);
		}

		@Override
		public void deleteModule(int mid) {
			
			adminDao.deleteModule(mid);
		}
		
		
		
	/*SubModule viewing,saving,editing and deleting*/
      	
        public List<AdminSection> getsubmodules() {
			
			return adminDao.getsubmodules();
		}

		public void savesubmodules(AdminModuleCommand amcommand) {
			
			adminDao.savesubmodules(amcommand);
			
		}
		@Override
		public AdminSection getSubModule(int sid) {
			
			return adminDao.getSubModule(sid);
		}

		@Override
		public void updatesubmodule(AdminModuleCommand amcommand) {
			
			adminDao.updatesubmodule(amcommand);
		}

		@Override
		public void deleteSubModule(int sid) {
			
			adminDao.deleteSubModule(sid);
		}
		
		
		
	/*Category viewing,saving,editing and deleting*/
		
		public List<CategoryMaster> getCategories() {
			
			return adminDao.getCategories();
		}

		public void saveCategorie(CategoryMaster categorymaster) {
			
			adminDao.saveCategorie(categorymaster);
			
		}
		@Override
		public CategoryMaster getCategory(int cid) {
			
			return adminDao.getCategory(cid);
		}

		@Override
		public void updateCategory(CategoryMaster categorymaster) {
			
			adminDao.updateCategory(categorymaster);
		}

		@Override
		public void deleteCategory(int cid) {
			adminDao.deleteCategory(cid);
			
		}

		public List<ServiceMaster> getServices() {
			
			return adminDao.getServices();
		}

		public void saveService(ServiceMaster servicemaster) {
			
			adminDao.saveService(servicemaster);
			
		}

		public List<LocationMaster> getLocations() {
			
			return adminDao.getLocations();
		}

		public void saveLocation(LocationMaster locationmaster) {
		 
			adminDao.saveLocation(locationmaster);
			
		}

		@Override
		public List<Role> getRoles() {
			
			return adminDao.getRoles();
		}

		@Override
		public void saveRole(Role role) {
			
			adminDao.saveRole(role);
			
		}

		@Override
		public List<BackendUsers> getbackendusers() {
			
			return adminDao.getbackendusers();
		}

		@Override
		public void savebackenduser(BackendUsers backenduser) {
			adminDao.savebackenduser(backenduser);
			
		}

		@Override
		public Role getRole(String rolename) {
			
			return adminDao.getRole(rolename);
		}
		
		@Override
		public String[] getPermissions(int rid) {
			
			return adminDao.getPermissions(rid);
		}


		@Override
		public List<AdminSection> getRoleSubModules(int[] subids) {
			
			return adminDao.getRoleSubModules(subids);
		}

		@Override
		public void setAdminSection(List<AdminModule> modlist,List<AdminSection> sublist,int roleid) {
			
			adminDao.setAdminSection(modlist,sublist,roleid);
		}

		@Override
		public List<AdminModule> getRoleModules(int[] m) {
			return adminDao.getRoleModules(m);
		}

		@Override
		public Map<String, List<AdminSection>> getuserrole(String uname) {
			return adminDao.getuserrole(uname);
		}

		@Override
		public int checkUser(String uname, String password) {
			return adminDao.checkUser(uname,password);
		}

		

	}


