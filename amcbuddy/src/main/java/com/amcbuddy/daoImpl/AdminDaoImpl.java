package com.amcbuddy.daoImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.amcbuddy.command.AdminModuleCommand;
import com.amcbuddy.dao.AdminDao;
import com.amcbuddy.model.AdminModule;
import com.amcbuddy.model.AdminSection;
import com.amcbuddy.model.BackendUsers;
import com.amcbuddy.model.CategoryMaster;
import com.amcbuddy.model.LocationMaster;
import com.amcbuddy.model.Role;
import com.amcbuddy.model.ServiceMaster;


	@Repository
	@Transactional
	public class AdminDaoImpl  implements AdminDao {

		
		@Autowired
		private SessionFactory sessionFactory;
		

		/*Module viewing,saving,editing and deleting*/
		
	     public List<AdminModule> getadminmodules(){
			
	        String ss="from AdminModule";
			Query q=sessionFactory.openSession().createQuery(ss);
			
			List<AdminModule> mresult=q.list();
			System.out.println("module result in dao impl"+mresult);
			
			return mresult;
		 }
         public void savemodule(AdminModuleCommand amcommand) {
	 		
	 		try{
	 			AdminModule module=new AdminModule();
	 			module.setModulename(amcommand.getModulename());
	 			
	 			module.setModule_status(amcommand.getModule_status());
	 			sessionFactory.openSession().save(module);
	 		}
	 		catch(Exception e){
	 		}
	 	 }
         public AdminModule getModule(int mid){
		 
	 	
	 	       String ss="from AdminModule where module_id=:moid";
	 		  Query q=sessionFactory.openSession().createQuery(ss);
	 		  q.setParameter("moid",mid);
	 		 List<AdminModule> moresult=q.list();
	 		 AdminModule mo=null;
	 		 for(AdminModule x:moresult){
	 			 mo=x;
	 			 
	 		 }
	 		  return mo;
	 		  
	 	 }
	 	 @Override
		 public void updatemodule(AdminModuleCommand amcommand) {
			
	 		try{
	 			AdminModule module=new AdminModule();
	 			System.out.println("the value of module id in edit in dao impl is"+amcommand.getModule_id());
	 			module.setModule_id(amcommand.getModule_id());

	 			System.out.println("the value of module id in edit in dao impl is"+module.getModule_id());
	 			module.setModulename(amcommand.getModulename());
	 			
	 			module.setModule_status(amcommand.getModule_status());
	 			
	 			
	 			sessionFactory.getCurrentSession().update(module);
	 		}
	 		catch(Exception e){
	 		}
		
	 	 
	 	 
	 	 }
         public void deleteModule(int mid){
			  
			  String ss="delete from AdminModule where module_id=:moid";
	 		  Query q=sessionFactory.openSession().createQuery(ss);
	 		  q.setParameter("moid",mid);
			  
			  q.executeUpdate();
			  
			  
		  }
	 	 
		  
         /*SubModule viewing,saving,editing and deleting*/
 		 public List<AdminSection> getsubmodules() {
			
			
	 	      String ss="from AdminSection";
	 		  Query q=sessionFactory.openSession().createQuery(ss);
	 			
	 		  List<AdminSection> sresult=q.list();
	 			
	 	  return sresult;
	 	 }
	 	 public void savesubmodules(AdminModuleCommand amcommand) {
	 		
	 		try{
	 			AdminSection submodule=new AdminSection();
	 			int mid=amcommand.getModule_id();
	 			System.out.println("the module id  is"+amcommand.getModule_id());
	 			AdminModule module=(AdminModule) sessionFactory.openSession().load(AdminModule.class, mid);
	 			
	 			
	 			submodule.setMenuo(module);
	 			submodule.setSectionname(amcommand.getSectionname());
	 			submodule.setUrl(amcommand.getUrl());
	 			sessionFactory.openSession().save(submodule);
	 			System.out.println("the section name is"+amcommand.getSectionname());
	 		}
	 		catch(Exception e){
	 		}
	 	 }
	 	public AdminSection getSubModule(int sid){
			 
		 	
	 	       String ss="from AdminSection where section_id=:soid";
	 		  Query q=sessionFactory.openSession().createQuery(ss);
	 		  q.setParameter("soid",sid);
	 		 List<AdminSection> soresult=q.list();
	 		AdminSection so=null;
	 		 for(AdminSection x:soresult){
	 			 so=x;
	 			 
	 		 }
	 		  return so;
	 		  
	 	 }
	 	 public void updatesubmodule(AdminModuleCommand amcommand) {
			
	 		try{
	 			AdminSection submodule=new AdminSection();
	 			int mid=amcommand.getModule_id();
	 			System.out.println("the module id  is"+amcommand.getModule_id());
	 			AdminModule module=(AdminModule) sessionFactory.openSession().load(AdminModule.class, mid);
	 			
	 			
	 			submodule.setMenuo(module);
	 			submodule.setSectionname(amcommand.getSectionname());
	 			submodule.setUrl(amcommand.getUrl());
	 			sessionFactory.getCurrentSession().update(submodule);
	 			System.out.println("the section name is"+amcommand.getSectionname());
	 		}
	 		catch(Exception e){
	 		}
		
	 	 
	 	 
	 	 }
         public void deleteSubModule(int sid){
			  
			  String ss="delete from AdminSection where section_id=:soid";
	 		  Query q=sessionFactory.openSession().createQuery(ss);
	 		  q.setParameter("soid",sid);
			  
			  q.executeUpdate();
			  
			  
		  }


		 
         /*Category viewing,saving,editing and deleting*/
 	    public List<CategoryMaster> getCategories() {
				String ss="from CategoryMaster";
				Query q=sessionFactory.openSession().createQuery(ss);
				
				List<CategoryMaster> cresult=q.list();
				System.out.println("category result in dao impl"+cresult);
				
				return cresult;
		  }
         public void saveCategorie(CategoryMaster categorymaster) {
				try{
		 			
					sessionFactory.openSession().save(categorymaster);
		 		}
		 		catch(Exception e){
		 		}
		}
		public CategoryMaster getCategory(int cid){
			
	    	   String ss="from CategoryMaster where categoryid=:catid";
				Query q=sessionFactory.openSession().createQuery(ss);
				q.setParameter("catid",cid);
				List<CategoryMaster> clist=q.list();
				CategoryMaster m=null;
				for(CategoryMaster c:clist){
					
					m=c;
				}
				System.out.println("category result in dao impl"+m);
				
				return m;
		}
        @Override
		public void updateCategory(CategoryMaster categorymaster) {
			
        	System.out.println("updating category");
    	   sessionFactory.getCurrentSession().update(categorymaster);
		}
        public void deleteCategory(int cid){
			 
        	String ss="delete from CategoryMaster where categoryid=:catid";
				Query q=sessionFactory.openSession().createQuery(ss);
				q.setParameter("catid",cid);
				q.executeUpdate();
		 }


		
        
        public List<ServiceMaster> getServices() {
			String ss="from ServiceMaster";
			Query q=sessionFactory.openSession().createQuery(ss);
			
			List<ServiceMaster> sresult=q.list();
			System.out.println("service result in dao impl"+sresult);
			
			return sresult;
		}


		public void saveService(ServiceMaster servicemaster) {
	        try{
	 			
	        	sessionFactory.openSession().save(servicemaster);
	 		}
	 		catch(Exception e){
	 		}
		}


		public List<LocationMaster> getLocations() {
			String ss="from LocationMaster";
			Query q=sessionFactory.openSession().createQuery(ss);
			
			List<LocationMaster> lresult=q.list();
			System.out.println("location result in dao impl"+lresult);
			
			return lresult;
		}


		public void saveLocation(LocationMaster locationmaster) {
	        try{
	 			
	        	sessionFactory.openSession().save(locationmaster);
	        	System.out.println(locationmaster);
	 		}
	 		catch(Exception e){
	 		}
		}


		@Override
		public List<Role> getRoles() {
			String ss="from Role";
			Query q=sessionFactory.openSession().createQuery(ss);
			
			List<Role> rresult=q.list();
			System.out.println("role result in dao impl"+rresult);
			
			return rresult;
		}


		@Override
		public void saveRole(Role role) {
            
			sessionFactory.openSession().save(role);	        	
			System.out.println(role.getRole_name());
	 		
	 		
			
		}


		@Override
		public List<BackendUsers> getbackendusers() {
			String ss="from BackendUsers";
			Query q=sessionFactory.openSession().createQuery(ss);
			
			List<BackendUsers> buresult=q.list();
			System.out.println("backendusers result in dao impl"+buresult);
			
			return buresult;
		}


		@Override
		public void savebackenduser(BackendUsers backenduser) {
           
			System.out.println(backenduser);
	        	sessionFactory.openSession().save(backenduser);
	        	
	 		
		}


		@Override
		public Role getRole(String rolename) {
			String ss="from Role where role_name=:rn";
			Query q=sessionFactory.openSession().createQuery(ss);
			q.setParameter("rn",rolename);
			Role role=null;
			List<Role> r=q.list();
			for(Role x:r){
				
				role=x;
			
				
			}
			System.out.println("role result in dao impl is"+role.getRole_id()+role.getRole_name());
			sessionFactory.openSession().close();
						return role;
		}


		@Override
		public List<AdminSection> getRoleSubModules(int[] subids) {
			List<AdminSection> aslist=new ArrayList<AdminSection>();
			AdminSection as=null;
			for(int x:subids){
			String ss="from AdminSection where section_id=:sid";
			Query q=sessionFactory.openSession().createQuery(ss);
			q.setParameter("sid",x);
			List<AdminSection> l=q.list();
			
			for(AdminSection y:l){
				 as=y;
				 System.out.println("in dao impl"+as);
				 
				 
				 
				 System.out.println("the aslist elements are"+aslist.size());
	        }
			 aslist.add(as);
			 System.out.println("the aslist elements are"+aslist.size());
			
			}
		
			
			
			return aslist;
		}

		
		@Override
		public List<AdminModule> getRoleModules(int[] m) {
			List<AdminModule> amlist=new ArrayList<AdminModule>();
			AdminModule am=null;
			for(int x:m){
			String ss="from AdminModule where module_id=:mid";
			Query q=sessionFactory.openSession().createQuery(ss);
			q.setParameter("mid",x);
			List<AdminModule> l=q.list();
			
			for(AdminModule y:l){
				 am=y;
				 System.out.println("in dao impl module list without duplicates is"+am);
				 
				 
				 
				 System.out.println("the amlist elements are"+amlist.size());
	        }
			 amlist.add(am);
			 System.out.println("the amlist elements are"+amlist.size());
			
			}
		
			
			
			return amlist;
		}
		
		@Override
		public String[] getPermissions(int rid) {
			String ss="select r.adminSection from Role r where r.role_id=:rd";
			Query q=sessionFactory.openSession().createQuery(ss);
			q.setParameter("rd",rid);
			
			List<AdminSection> s=q.list();
			int k=s.size();
			int r=s.size();
			System.out.println("k value"+k);
			int a[]=new int[k];
			for(AdminSection x:s){
				 a[k-1]=x.getSection_id();
				 k--;
			}
			String m[]=new String[r];
			for(int x:a){
				m[r-1]=Integer.toString(x);
				r--;
			}
			return m;	
			}
			
			
			

		

		@Override
		public void setAdminSection(List<AdminModule> modlist,List<AdminSection> sublist,int roleid) {
			
			List<AdminModule> am=new ArrayList<AdminModule>();
			am.addAll(modlist);
			Role role=(Role)sessionFactory.getCurrentSession().get(Role.class, roleid);
			role.setAdminSection(sublist);
			role.setAdminModule(modlist);
			
			sessionFactory.getCurrentSession().update(role);
			
			
		}


		@Override
		public Map<String, List<AdminSection>> getuserrole(String username) {
			List<AdminModule> aml=null;
			List<AdminSection> asl=null;
		    Map<String,List<AdminSection>> hm=new TreeMap<String,List<AdminSection>>();
			String ss="select r from Role r,BackendUsers b  where b.username=:uname and b.work=r.role_name";
			Query q=sessionFactory.openSession().createQuery(ss);
			q.setParameter("uname",username);
			
			List<Role> l=q.list();
			
			for(Role x:l){
				int r=x.getRole_id();
				      aml=x.getAdminModule();
				      asl=x.getAdminSection();
				
			}
			
	           for(AdminModule y:aml){	
	        	   List<AdminSection> asfor=new ArrayList<AdminSection>();
	               
				for(AdminSection z:asl){
					   
					System.out.println("c"+z.getSectionname());
					if(y.getModule_id()==z.getMenuo().getModule_id()){
						asfor.add(z);
						
					}
					System.out.println("asfor size is in"+asfor.size());
				
			    }
			     
				System.out.println("asfor size is out"+asfor.size());
				hm.put(y.getModulename(),asfor);
			
				
				
				
	          }
			
			
			return hm;
		}


		@Override
		public int checkUser(String uname, String password) {
			
			String ss="from BackendUsers where username=:un and password=:pw";
			Query q=sessionFactory.openSession().createQuery(ss);
			q.setParameter("un",uname);
			q.setParameter("pw", password);
			int l=q.list().size();
			
			return l;
		}
		


		


		


		
	     
	     

	}


