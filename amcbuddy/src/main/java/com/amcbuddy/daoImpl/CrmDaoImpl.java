package com.amcbuddy.daoImpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.amcbuddy.command.EnquiryCommand;
import com.amcbuddy.dao.CrmDao;
import com.amcbuddy.model.BackendUsers;
import com.amcbuddy.model.Deal;
import com.amcbuddy.model.Enquiry;
import com.amcbuddy.model.Leads;
import com.amcbuddy.model.Role;
import com.amcbuddy.model.ServiceMaster;
import com.amcbuddy.utility.Utility;

import freemarker.template.Configuration;

@Repository
@Transactional


public class CrmDaoImpl implements CrmDao{
    @Autowired
	private JavaMailSender sender;
	@Autowired
	private Configuration freeMemarkerConfiguration;

	@Autowired
	private SessionFactory sessionFactory;
	@Value("${username}")
	private String username;

	@Value("${password}")
	private String smspassword;
	
    @Value("${url}")
	private String url;
	@Override
	public boolean saveEnquiry(EnquiryCommand enquiry) {
		boolean flag=false;
	    try {
			Enquiry en =new Enquiry();
			ArrayList<String> mailids=new ArrayList<String>();
			ArrayList<Long> numbers=new ArrayList<Long>();
			
			
			mailids.add(enquiry.getEmail());
			numbers.add(Long.parseLong(enquiry.getPhonenumber()));
			
			//assigned role users details
			Role role=(Role) sessionFactory.getCurrentSession().load(Role.class ,enquiry.getAssignedrole());
			ServiceMaster service=(ServiceMaster) sessionFactory.getCurrentSession().load(ServiceMaster.class ,enquiry.getService());
			en.setEnquiry_name(enquiry.getName());
			en.setEnq_email(enquiry.getEmail());
			en.setEnq_mobilenumber(enquiry.getPhonenumber());
			en.setEnq_address(enquiry.getAddress());
			System.out.println(enquiry.getFollowup_date()+"daaaaaaaaaaaaa");
			en.setEnq_followupdate(Utility.convertStringToSqlDate(enquiry.getFollowup_date()));
			en.setEnq_description(enquiry.getDescription());
			en.setRole(role);
			en.setIs_lead("N");
			en.setServiceMaster(service);
			 
		     LocalDateTime date=new LocalDateTime();
		     en.setCreated_by(date);
		     en.setModified_by(date);
	        System.out.println("datecreate/..........."+enquiry.getCreated_by());
			System.out.println("emai......"+enquiry.getEmail());
			System.out.println("name......"+enquiry.getName());
			System.out.println("adress,.............."+enquiry.getAddress());
			System.out.println("date..........."+enquiry.getFollowup_date());
			System.out.println("phone no......"+enquiry.getPhonenumber());
			System.out.println("heelllo amc............................................"+en);
			sessionFactory.getCurrentSession().save(en);
			
			// assigned  role user details like mobile and email
			List<BackendUsers> backend=role.getBackendusers();
			for(BackendUsers user:backend)
			{
				mailids.add(user.getEmail());
				numbers.add(user.getMobileno());
			}
			
			
			//  super admin role user details like mobile and email
			
			List<Role> adminrole=sessionFactory.getCurrentSession().createQuery("from Role where role_name='SUPER _ADMIN'").list();

			
			System.out.println("roles.........."+adminrole.size());

			Role adminrl=adminrole.get(0);
			
			List<BackendUsers> backenduser=adminrl.getBackendusers();
			for(BackendUsers rol:backenduser)
			{
				mailids.add(rol.getEmail());
				numbers.add(rol.getMobileno());
			}
			
			
			mailSendingForAddEnquiry(mailids,en);
			regSend(username,smspassword,url,numbers);

	System.out.println(numbers);		

			
			
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	     flag=true;
		
		return flag;
		
	
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Enquiry> getAllEnquiries() {
		
		 
		String sql="from Enquiry where is_lead=:value";
		
		Query query=sessionFactory.getCurrentSession().createQuery(sql);
		query.setParameter("value", "N");
       return query.list();
	}
	
	
	
	@Override
	public Enquiry editenquiry(int enquiryId) {
		  return (Enquiry) sessionFactory.getCurrentSession().get( Enquiry.class, enquiryId);
		  
	    }


	

	@Override
	public Enquiry leads(int enquiryId) {
		  return (Enquiry) sessionFactory.getCurrentSession().get( Enquiry.class, enquiryId);
		  
    }


	@Override
	public boolean saveLead(Leads lead) {
		boolean flag=false;
	    try {
		
			sessionFactory.getCurrentSession().save(lead);
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	     flag=true;
		
		return flag;
		
	
	}

	@Override
	public List<Leads> getAllLeads() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Leads").list();
	}


	@Override
	public List<ServiceMaster> getServices() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from ServiceMaster").list();
	}


	@Override
	public List<Role> getRoles() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Role").list();
	}
	
	
	
	public void mailSendingForAddEnquiry(ArrayList<String> mails,Enquiry enquiry)
	{
		try
		{
			
			 Map<String,String> datamodel=new HashMap<String,String>();
			 datamodel.put("name", enquiry.getEnquiry_name());
			 datamodel.put("email", enquiry.getEnq_email());
			 datamodel.put("phonenumber", enquiry.getEnq_mobilenumber());
			 datamodel.put("address", enquiry.getEnq_address());
			 datamodel.put("service", enquiry.getServiceMaster().getServicename());
						 
			String content = FreeMarkerTemplateUtils.processTemplateIntoString(
					freeMemarkerConfiguration.getTemplate("CandMailTemplate.html"),datamodel);
			
			for(String to:mails)
			{
				InternetAddress[] receipients = new InternetAddress[] { new InternetAddress(to) };

				sender.send(getMimeMessagePreparator(receipients, "info@jagahunt.com", "Adding Enquiry mail",
						content));
				
				
			}
			
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			
		}
		
		
		
		
		
		
	}
	
	public MimeMessagePreparator getMimeMessagePreparator(final InternetAddress[] receipients, final String from,
		     final String subject, final String msgText) {

			MimeMessagePreparator preparator = new MimeMessagePreparator() {

				public void prepare(MimeMessage mimeMessage) throws Exception {

					BodyPart body = new MimeBodyPart();
					body.setContent(msgText, "text/html");

					Multipart multipart = new MimeMultipart();
					multipart.addBodyPart(body);

					mimeMessage.setRecipients(Message.RecipientType.TO, receipients);
					mimeMessage.setFrom(new InternetAddress(from, "JAGAHUNT"));
					mimeMessage.setSubject(subject);
					mimeMessage.setSentDate(new Date());

					mimeMessage.setContent(multipart);
				}
			};

			return preparator;
		}
	
	
	 public  void regSend(String uasername,String smspassword,String urls,ArrayList<Long> numbers){
     	try 
     	{
     		for(Long number:numbers)
     		{
     		// Construct data
        		 String User =uasername;
        		  String passwd =smspassword;
        		  String mobilenumber =number.toString(); 
        		  String message = "Dear Amc User";
        		  
        	String data="user=" + URLEncoder.encode(User, "UTF-8");
        	data +="&password=" + URLEncoder.encode(passwd, "UTF-8");
        	data +="&message=" + URLEncoder.encode(message, "UTF-8");
        	data +="&sender=" + URLEncoder.encode("JAHUNT", "UTF-8");
        	data +="&mobile=" + URLEncoder.encode(mobilenumber, "UTF-8");
        	data +="&type=" + URLEncoder.encode("3", "UTF-8");
        	// Send data 
        	URL url = new URL(urls+data);
        	URLConnection conn = url.openConnection();
        	conn.setDoOutput(true);
        	OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        	wr.write(data);
        	wr.flush();
        	// Get the response
        	BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        	String line;
        	String sResult1="";
        	while ((line = rd.readLine()) != null) 
        	{
        	// Process line...
        	sResult1=sResult1+line+" ";
        	}
        	wr.close();
        	rd.close();
        	
     			
     		}
     		
     	
     	} 
     	catch (Exception e)
     	{
     	System.out.println("Error SMS "+e);
     
     	}
     	}


	@Override
	public boolean saveConvertedLead(EnquiryCommand enquiry) {
		// TODO Auto-generated method stub
		
		
		ArrayList<String> mailids=new ArrayList<String>();
		ArrayList<Long> numbers=new ArrayList<Long>();
		//add clienf detail for sending mail and message
		mailids.add(enquiry.getEmail());
		numbers.add(Long.parseLong(enquiry.getPhonenumber()));
		
		
		Enquiry role=(Enquiry) sessionFactory.getCurrentSession().load(Enquiry.class ,enquiry.getEnq_id());
		sessionFactory.getCurrentSession().update(role);

		Role roleassigned=(Role) sessionFactory.getCurrentSession().load(Role.class ,enquiry.getAssignedrole());
		
		sessionFactory.getCurrentSession().update(roleassigned);
		ServiceMaster serviceMaster=(ServiceMaster) sessionFactory.getCurrentSession().load(ServiceMaster.class ,enquiry.getService());
		sessionFactory.getCurrentSession().update(serviceMaster);
		Leads lead=new Leads();
		
		lead.setEnquiry(role);
		lead.setRole(roleassigned);
		lead.setNext_follwupdate(enquiry.getFollowup_date());
		lead.setLead_status(enquiry.getStatus());
		lead.setServiceMaster(serviceMaster);
		lead.setImagepath(enquiry.getImagepath());
		sessionFactory.getCurrentSession().save(lead);
		
		// assigned  role user details like mobile and email
					List<BackendUsers> backend=roleassigned.getBackendusers();
					for(BackendUsers user:backend)
					{
						mailids.add(user.getEmail());
						numbers.add(user.getMobileno());
					}
					
					
					//  super admin role user details like mobile and email
					
					List<Role> adminrole=sessionFactory.getCurrentSession().createQuery("from Role where role_name='SUPER _ADMIN'").list();
					Role adminrl=adminrole.get(0);
					
					List<BackendUsers> backenduser=adminrl.getBackendusers();
					for(BackendUsers rol:backenduser)
					{
						mailids.add(rol.getEmail());
						numbers.add(rol.getMobileno());
					}
					
					
					mailSendingForAddEnquiry(mailids,role);
					regSend(username,smspassword,url,numbers);

		return true;
	}



	@Override
	public Enquiry deals(int enquiryId) {
return (Enquiry) sessionFactory.getCurrentSession().get( Enquiry.class, enquiryId);

}


	@Override
	public boolean update(EnquiryCommand command) {
		boolean flag=false;
		
		try {
			Enquiry enquiry=(Enquiry) sessionFactory.getCurrentSession().load(Enquiry.class, command.getEnq_id());
			
			enquiry.setEnquiry_id(command.getEnq_id());
			enquiry.setEnquiry_name(command.getName());
			enquiry.setEnq_email(command.getEmail());
			enquiry.setEnq_address(command.getAddress());
			enquiry.setEnq_description(command.getDescription());
			enquiry.setEnq_followupdate(Utility.convertStringToSqlDate(command.getFollowup_date()));
			
			System.out.println("update-------------------"+command.getAddress());
			
	
      sessionFactory.getCurrentSession().update(enquiry);
      
      
      
      
      System.out.println("saveme..............."+enquiry);
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}


	@Override
	public boolean saveConverteddeal(EnquiryCommand enquiry) {
		
		System.out.println("enquiry.getEnq_id()........................"+enquiry.getEnq_id());

		
		Enquiry role=(Enquiry) sessionFactory.getCurrentSession().load(Enquiry.class ,enquiry.getEnq_id());
		sessionFactory.getCurrentSession().update(role);

		Role roleassigned=(Role) sessionFactory.getCurrentSession().load(Role.class ,enquiry.getAssignedrole());
		
		sessionFactory.getCurrentSession().update(roleassigned);
		ServiceMaster serviceMaster=(ServiceMaster) sessionFactory.getCurrentSession().load(ServiceMaster.class ,enquiry.getService());
		sessionFactory.getCurrentSession().update(serviceMaster);
	    Deal deal=new Deal();
		deal.setDeal_id(enquiry.getDeal_id());
		deal.setDeal_status(enquiry.getStatus());
		deal.setRole(roleassigned);
		deal.setEnquiry(role);
		deal.setServiceMaster(serviceMaster);
		System.out.println("enquiry..........."+role);
		System.out.println("deal.........."+enquiry.getStatus());
	
		sessionFactory.getCurrentSession().save(deal);
		System.out.println("savedeallllllllllllllllllllllllll.........."+deal);
		return true;
		
	}


	@Override
	public List<Deal> getAllDeals() {

	
			// TODO Auto-generated method stub
			return sessionFactory.getCurrentSession().createQuery("from Deal").list();
		}


	
	}








