package com.amcbuddy.service;

import java.util.List;

import com.amcbuddy.command.EnquiryCommand;
import com.amcbuddy.model.Deal;
import com.amcbuddy.model.Enquiry;
import com.amcbuddy.model.Leads;

import com.amcbuddy.model.Role;
import com.amcbuddy.model.ServiceMaster;

public interface CrmService {

	boolean saveEnquiry(EnquiryCommand enquiry);

	List<Enquiry> getAllEnquiries();

	Enquiry editenquiry(int enquiryId);


	Enquiry leads(int enquiryId);

	boolean saveLead(Leads lead);

	List<Leads> getAllLeads();
	List<Role> getRoles();
	List<ServiceMaster> getServices();
	
	boolean saveConvertedLead(EnquiryCommand enquiry);


	Enquiry deals(int enquiryId);

	boolean update(EnquiryCommand command);

		boolean saveConverteddeal(EnquiryCommand enquiry);

		List<Deal> getAllDeals();



	



 


}
