package com.amcbuddy.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amcbuddy.command.AdminModuleCommand;
import com.amcbuddy.model.AdminModule;
import com.amcbuddy.model.AdminSection;
import com.amcbuddy.model.BackendUsers;
import com.amcbuddy.model.CategoryMaster;
import com.amcbuddy.model.LocationMaster;
import com.amcbuddy.model.Role;
import com.amcbuddy.model.ServiceMaster;
import com.amcbuddy.service.AdminService;


	@Controller
	@Lazy
	public class AdminController {
		
		
		public static Logger debug = Logger.getLogger(AdminController.class);
		
	  	
		@Autowired
		private AdminService adminService;
		
		@RequestMapping("/adminlogin")
		public String admindashboard(){
			
			
			
			return "adminpages/login";
		}
		
		
		
		
		@RequestMapping("/index")
		public String index(Model model,HttpServletRequest req){
			
			 HttpSession session=req.getSession();
			String uname=(String)session.getAttribute("un");
		Map<String, List<AdminSection>> am=adminService.getuserrole(uname);
			
		    model.addAttribute("modulelist",am);
			
		  
		    
			session.setAttribute("modulelist",am);
			
			session.removeAttribute("result");
			return "adminpages/dashboard";
		}
		
		
		
		
		
		
		@RequestMapping(value = "/checkuser", method ={RequestMethod.GET,RequestMethod.POST})
		public  String checkuser(Model model,@RequestParam("username")String uname,@RequestParam("password")String password,HttpServletRequest req){
			HttpSession session=req.getSession();
			int i=adminService.checkUser(uname,password);
			System.out.println("users size in controller is"+i);
			if(i==1){
				session.setAttribute("un",uname);
				
				return "redirect:index.do";
			}
			else{
				String s="Invalid User Credentials";
				session.setAttribute("result",s);
				return "redirect:adminlogin.do";
			}
			
			
			
		}
		
		
		
		@RequestMapping("/orders")
		public String orders(){
			
			
			
			return "adminpages/orders";
		}
		
		
		
		@RequestMapping("/panels")
		public String panels(){
			System.out.println("panels page");
			
			return "adminpages/panels";
		}
		
		@RequestMapping("/widgets")
		public String widgets(){
			
			
			
			return "adminpages/widgets";
		}
		
   /*Module viewing,saving,editing and deleting*/
		
		@RequestMapping("/viewmodules")
		public String viewmodule(Model model){
			
			 List<AdminModule> mlist=adminService.getadminmodules();
			  model.addAttribute("modules", "modules");
			   model.addAttribute("modlist",mlist);
			
			return "adminpages/viewmodulespage";
		}
		
		@RequestMapping("/addmodule")
		public String shome(Model model) {
			
			AdminModuleCommand amcommand=new AdminModuleCommand();
			
			 try {
				 model.addAttribute("amcommand", amcommand);
			     model.addAttribute("module", "module");
				 } 
			 catch (Exception e) {
					debug.error("exception raised in adminmodulecontroller------------ ", e);
				}
			 
		return "adminpages/addmodulepage"; 
			 
		}
		
		@RequestMapping(value = "/savemodule", method ={RequestMethod.GET,RequestMethod.POST})
		public String savemodule(@ModelAttribute AdminModuleCommand amcommand,HttpServletRequest req) {
			
			if(amcommand.getModule_id()==0){
				adminService.savemodule(amcommand);
			 }
			else{
				adminService.updatemodule(amcommand);
				}
			
			
			return "redirect:viewmodules.do";
		}
		
		@RequestMapping("/editmodule")
		public String editmodule(Model model,@RequestParam("modid")int mid){
			 
			
              AdminModule mli=adminService.getModule(mid);
			
			   model.addAttribute("mli",mli);
			
			 return "adminpages/addmodulepage";
		}
		
		@RequestMapping(value = "/deletemodule", method = {RequestMethod.GET,RequestMethod.POST})
		public String deleteModules(Model model,@RequestParam("modid")int mid){
			
			
			adminService.deleteModule(mid);
			
			
			return "redirect:viewmodules.do";
		}
		
		
   /*SubModule viewing,saving,editing and deleting*/
		
		@RequestMapping("/viewsubmodules")
		public String viewsubmodule(Model model){
			 
			
			List<AdminSection> sublist=adminService.getsubmodules();
			
			model.addAttribute("submodules", "submodules");
			 
			model.addAttribute("submodulelist",sublist);
			
			return "adminpages/viewsubmodulespage";
		}                                     

		@RequestMapping("/addsubmodule")
		public String addsubmodule(Model model) {
			
			AdminModuleCommand amcommand=new AdminModuleCommand();
			
			    try {
			    	
			    List<AdminModule> mlist=adminService.getadminmodules();
					 
				model.addAttribute("modl", mlist);
				
				 model.addAttribute("amcommand", amcommand);
				 model.addAttribute("submodule", "submodule");
				
				 } 
			    catch (Exception e) {
					debug.error("exception raised in adminmodulecontroller------------ ", e);
				 }
			 
		  return "adminpages/addsubmodulespage"; 
			 
		 }
		
		@RequestMapping(value = "/savesubmodule", method ={RequestMethod.GET,RequestMethod.POST})
		public String savesubmodule(@ModelAttribute AdminModuleCommand amcommand,HttpServletRequest req) {
			
			try{
			   String commandurl=amcommand.getUrl();
			   String newurl=commandurl.concat("");
			   
			   amcommand.setUrl(newurl);
			   if(amcommand.getSection_id()==0){
				   adminService.savesubmodules(amcommand);
				 }
				else{
					adminService.updatesubmodule(amcommand);
					}
			   
			  
			   System.out.println("the addsubmodule is"+amcommand.getSectionname());
			}
			catch(Exception e){
				
				debug.error("exception raised in adminmodulecontroller------------ ", e);
			}
		 return "redirect:viewsubmodules.do";
		
	     }
		 
		@RequestMapping("/editsubmodule")
		public String editsubmodule(Model model,@RequestParam("subid")int sid){
			List<AdminModule> mlis=adminService.getadminmodules();
			 
			   model.addAttribute("modl", mlis);
			
			 AdminSection sli=adminService.getSubModule(sid);
			
			   model.addAttribute("sli",sli);
			
			 return "adminpages/addsubmodulespage";
		}
		
		@RequestMapping(value = "/deletesubmodule", method = {RequestMethod.GET,RequestMethod.POST})
		public String deleteSubModules(Model model,@RequestParam("subid")int sid){
			
			
			adminService.deleteSubModule(sid);
			
			
			return "redirect:viewsubmodules.do";
		}
		
		
		
	/*Category viewing,saving,editing and deleting*/
		
		
		@RequestMapping(value = "/viewcategories", method = {RequestMethod.GET,RequestMethod.POST})
		public String showCategories(Model model){
			
			List<CategoryMaster> clist=adminService.getCategories();
			
			model.addAttribute("catlist",clist);
			
			return "adminpages/viewcategoriespage";
		}
		
		
		@RequestMapping(value = "/addcategory", method = {RequestMethod.GET,RequestMethod.POST})
		public String addCategories( ){
			
			
			
			
			return "adminpages/addcategorypage";
		}
		
		
		@RequestMapping(value = "/savecategory", method = {RequestMethod.GET,RequestMethod.POST})
		public String saveCategories(@ModelAttribute CategoryMaster categorymaster){
			
			if(categorymaster.getCategoryid()==0){
			adminService.saveCategorie(categorymaster);
			}
			else{
				adminService.updateCategory(categorymaster);
			}
			
			return "redirect:viewcategories.do";
		}
		
		@RequestMapping(value = "/editcategory", method = {RequestMethod.GET,RequestMethod.POST})
		public String editCategories(Model model,@RequestParam("catid")int cid){
			
			
			CategoryMaster catmlist=adminService.getCategory(cid);
			
			model.addAttribute("catmlist",catmlist);
			
			return "adminpages/addcategorypage";
		}
		
		@RequestMapping(value = "/deletecategory", method = {RequestMethod.GET,RequestMethod.POST})
		public String deleteCategories(Model model,@RequestParam("catid")int cid){
			
			
			adminService.deleteCategory(cid);
			
			
			return "redirect:viewcategories.do";
		}
		
		
		
		
		
		
		
		
		
		
		
		
		@RequestMapping(value = "/viewservices", method = {RequestMethod.GET,RequestMethod.POST})
		public String showServices(Model model){
			
			List<ServiceMaster> slist=adminService.getServices();
			
			model.addAttribute("servicelist",slist);
			
			return "adminpages/viewservicespage";
		}
		
		
		@RequestMapping(value = "/addservice", method = {RequestMethod.GET,RequestMethod.POST})
		public String addServices( ){
			
			
			
			
			return "adminpages/addservicepage";
		}
		
		
		@RequestMapping(value = "/saveservice", method = {RequestMethod.GET,RequestMethod.POST})
		public String saveServices(@ModelAttribute  ServiceMaster servicemaster){
			
			
			adminService.saveService(servicemaster);
			
			
			return "redirect:viewservices.do";
		}
		
		
		@RequestMapping(value = "/viewlocations", method = {RequestMethod.GET,RequestMethod.POST})
		public String showLocations(Model model){
			
			List<LocationMaster> loclist=adminService.getLocations();
			
			model.addAttribute("locationlist",loclist);
			
			return  "adminpages/ viewlocationspage";
		}
		
		
		@RequestMapping(value = "/addlocations", method = {RequestMethod.GET,RequestMethod.POST})
		public String addLocations( ){
			
			
			
			
			return "adminpages/addlocationpage";
		}
		
		
		@RequestMapping(value = "/savelocations", method ={RequestMethod.GET,RequestMethod.POST})
		public String saveLocations(@ModelAttribute  LocationMaster locationmaster){
			
			LocalDateTime ldt = new LocalDateTime();
		    locationmaster.setCreatedOn(ldt);;
		    locationmaster.setModifiedOn(ldt);
			
			
			adminService.saveLocation(locationmaster);
			
			
			return "redirect:viewlocations.do";
		}
		
		@RequestMapping(value = "/viewroles", method = {RequestMethod.GET,RequestMethod.POST})
		public String showRoles(Model model){
			
			List<Role> rolelist=adminService.getRoles();
			
			model.addAttribute("rolelist",rolelist);
			
			return  "adminpages/viewrolespage";
		}
		
		
		@RequestMapping(value = "/addrole", method = {RequestMethod.GET,RequestMethod.POST})
		public String addRole( ){
			
			
			
			
			return "adminpages/addrolepage";
		}
		
		
		@RequestMapping(value = "/saverole", method ={RequestMethod.GET,RequestMethod.POST})
		public String saveRole(@ModelAttribute  Role role){
			
	
			
			System.out.println("role name is"+role.getRole_name());
			
			adminService.saveRole(role);
			
		
			
			return "redirect:viewroles.do";
		}
		
		
		@RequestMapping(value = "/viewbackendusers", method = {RequestMethod.GET,RequestMethod.POST})
		public String showbausers(Model model){
			
			List<BackendUsers> buserlist=adminService.getbackendusers();
			
			model.addAttribute("buserlist",buserlist);
			
			return  "adminpages/viewbackenduserspage";
		}
		
		
		@RequestMapping(value = "/addbackenduser", method = {RequestMethod.GET,RequestMethod.POST})
		public String addbauser(Model model){
			
               List<Role> rlist=adminService.getRoles();
			
			   model.addAttribute("rlist",rlist);
			
			
			return "adminpages/addbackenduserpage";
		}
		
		
		@RequestMapping(value = "/savebackenduser", method ={RequestMethod.GET,RequestMethod.POST})
		public String saveUser(@ModelAttribute BackendUsers backenduser){
			
			LocalDateTime ldt = new LocalDateTime();
			

			backenduser.setCreatedOn(ldt);;
			backenduser.setModifiedOn(ldt);
		
			String rolename=backenduser.getWork();
			Role role=adminService.getRole(rolename);
			
			backenduser.setRole(role);
			
			/*System.out.println("role name in controller is"+role.getRole_name());
			
			System.out.println("backenduser role in controller is"+backenduser.getRole().getRole_id());*/
			
			adminService.savebackenduser(backenduser);
			
			
			return "redirect:viewbackendusers.do";
		}
		
		
		
		@RequestMapping(value = "/viewpermissions", method = {RequestMethod.GET,RequestMethod.POST})
		public String showPermissions(Model model){
			 List<Role> rplist=adminService.getRoles();
				model.addAttribute("rplist",rplist);
			   
			  
			   List<AdminModule> mplist=adminService.getadminmodules();
			   
			 
			 model.addAttribute("mplist",mplist);
			return  "adminpages/viewpermissionspage";
		}
		
		@RequestMapping("/checkpermissions")
		public @ResponseBody String[] checkPermissions(@RequestParam("id")String rid){
			
			
			System.out.println("check permissions in controller page");
		
			int r=Integer.parseInt(rid);
		 System.out.println("r value is"+r);
		 
		 
		 
		  
		 
		  
		 
		 
		 return adminService.getPermissions(r);
		}
		
		@RequestMapping(value = "/savepermission", method ={RequestMethod.GET,RequestMethod.POST})
		public String savePermission(@RequestParam("rolesub[]")int[] subids,@RequestParam("roleid")int roleid){
			
			
		   
		    List<AdminSection> sublist=adminService.getRoleSubModules(subids);
		  
		      Set<Integer> mids=new HashSet<Integer>();
		       for(AdminSection x:sublist){
			           mids.add(x.getMenuo().getModule_id());
			           
			         System.out.println(x.getMenuo());
			    }
		       
		       
		      int s=mids.size();
		       
		       int[] m=new int[s];
		       
		       Iterator<Integer> i=mids.iterator();
		       while(i.hasNext()){
		    	  m[s-1]= i.next();
		    	  s--; 
		       }
		       
		       
		       List<AdminModule> modlist=adminService.getRoleModules(m);
			
			adminService.setAdminSection(modlist,sublist,roleid);
	
			
			
			
			return "redirect:viewpermissions.do";
		}
		
		
		
		
		
		
		
		
		
		/*@RequestMapping(value = "/adduser", method = {RequestMethod.GET,RequestMethod.POST})
		public String adduser( ){
			
               List<Role> rlist=adminService.getRoles();
			
			   model.addAttribute("rlist",rlist);
			
			
			return "adduserpage";
		}
		
		
		@RequestMapping(value = "/saveuser", method ={RequestMethod.GET,RequestMethod.POST})
		public String saveUser(@ModelAttribute Registration registration){
			
			LocalDateTime ldt = new LocalDateTime();
			registration.setCreatedOn(ldt);;
			registration.setModifiedOn(ldt);
			
			
			adminService.saveuser(registration);
			
			
			return "redirect:adduser.do";
		}
		*/
		
		
		
		
		
		
		

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

	}


