package com.amcbuddy.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.amcbuddy.command.EnquiryCommand;
import com.amcbuddy.dto.DealDto;
import com.amcbuddy.dto.LeadsDto;
import com.amcbuddy.model.Deal;
import com.amcbuddy.model.Enquiry;
import com.amcbuddy.model.Leads;
import com.amcbuddy.model.Role;
import com.amcbuddy.service.AdminService;
import com.amcbuddy.service.CrmService;
import com.amcbuddy.utility.Constant;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;


@Controller
@Lazy
public class CrmController {
	
	Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
			  "cloud_name", Constant.CLOUD_NAME,
			  "api_key", Constant.API_KEY,
			  "api_secret", Constant.API_SECRET_KEY));
	@Autowired
	private CrmService crmService;
	@Autowired
	private AdminService adminService;
	
	
	@RequestMapping(value="/addEnquiry", method = RequestMethod.GET)
	public String enquiry(Model model){
		
		EnquiryCommand enquiry = new EnquiryCommand();
		model.addAttribute("enquiry",enquiry);
		model.addAttribute("assigned",crmService.getRoles());
		model.addAttribute("services",crmService.getServices());
		
	
		return "adminpages/addEnquiry";
	}
	

	@RequestMapping(value="/saveenquiry", method = RequestMethod.GET)
	public String enquirysave(Model model,EnquiryCommand enquiry){
		boolean flag =  crmService.saveEnquiry(enquiry);
		List<Role> rolelist=adminService.getRoles();

		System.out.println("helooooooooooooooooo"+enquiry.getEmail());
		return "adminpages/listofenquiry";
	}
	
	
	@RequestMapping("/listofenquiry")
	public String listofenquiry(Model model,HttpServletRequest request){
		List<Enquiry> listEnquiry = crmService.getAllEnquiries();
        model.addAttribute("listenquiry", listEnquiry);
           System.out.println("swathi................"+listEnquiry.size());
		return "adminpages/listofenquiry";
	}
	

	@RequestMapping("/editenquiry")
	public String editenquiry(Model model,HttpServletRequest request){
	
		try {
		    int enquiryId = Integer.parseInt(request.getParameter("id"));
			Enquiry	qry = crmService.editenquiry(enquiryId);
			EnquiryCommand enquiry=new EnquiryCommand();
			enquiry.setEnq_id(qry.getEnquiry_id());
			enquiry.setName(qry.getEnquiry_name());
			enquiry.setAddress(qry.getEnq_address());
			enquiry.setPhonenumber(qry.getEnq_mobilenumber());
			enquiry.setEmail(qry.getEnq_email());
			enquiry.setDescription(qry.getEnq_description());
			enquiry.setAssignedrole(qry.getRole().getRole_id());
			System.out.println(qry.getRole().getRole_id());
		  //System.out.println(datamodel.getEnq_followupdate());
			
			enquiry.setService(qry.getServiceMaster().getService_id());
			//enquiry.setFollowup_date(datamodel.);
			model.addAttribute("assigned",crmService.getRoles());
			model.addAttribute("services",crmService.getServices());
			model.addAttribute("enquiry", enquiry);
			
			model.addAttribute("enquiry", enquiry);
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
				return "adminpages/editenquiry";
	}
	
	
	
	@RequestMapping("/enquiryupdate")
	public String update(Model model,HttpServletRequest request,EnquiryCommand command){
		           try {
					boolean flag=crmService.update(command);
					   model.addAttribute("assigned",crmService.getRoles());
					   model.addAttribute("services",crmService.getServices());
				} catch (Exception e) {
					e.printStackTrace();
				}
				

		return"adminpages/listofenquiry" ;

		
	}
	
	@RequestMapping("/leads")
	public String leads(Model model,HttpServletRequest request){
		
		try {
		    int enquiryId = Integer.parseInt(request.getParameter("id"));
			Enquiry	datamodel = crmService.leads(enquiryId);
			
			EnquiryCommand enquiry=new EnquiryCommand();
			enquiry.setEnq_id(datamodel.getEnquiry_id());
			enquiry.setName(datamodel.getEnquiry_name());
			enquiry.setAddress(datamodel.getEnq_address());
			enquiry.setPhonenumber(datamodel.getEnq_mobilenumber());
			enquiry.setEmail(datamodel.getEnq_email());
			enquiry.setDescription(datamodel.getEnq_description());
			enquiry.setAssignedrole(datamodel.getRole().getRole_id());
			System.out.println(datamodel.getRole().getRole_id());
		  //System.out.println(datamodel.getEnq_followupdate());
			
			enquiry.setService(datamodel.getServiceMaster().getService_id());
			//enquiry.setFollowup_date(datamodel.);
			model.addAttribute("assigned",crmService.getRoles());
			model.addAttribute("services",crmService.getServices());
			
			
			
			
			model.addAttribute("enquiry", enquiry);
		    } catch (NumberFormatException e) {
			e.printStackTrace();
		   }		
		return "adminpages/leads";

	}
	
	@RequestMapping("/deals")
	public String deals(Model model,HttpServletRequest request){
		
		try {
		    int enquiryId = Integer.parseInt(request.getParameter("id"));
			Enquiry	datamodel = crmService.leads(enquiryId);
			
			EnquiryCommand enquiry=new EnquiryCommand();
			
			enquiry.setEnq_id(datamodel.getEnquiry_id());
			enquiry.setName(datamodel.getEnquiry_name());
			enquiry.setAddress(datamodel.getEnq_address());
			enquiry.setPhonenumber(datamodel.getEnq_mobilenumber());
			enquiry.setEmail(datamodel.getEnq_email());
			enquiry.setDescription(datamodel.getEnq_description());
			enquiry.setAssignedrole(datamodel.getRole().getRole_id());
			System.out.println(datamodel.getRole().getRole_id());
		  //System.out.println(datamodel.getEnq_followupdate());
			
			enquiry.setService(datamodel.getServiceMaster().getService_id());
			//enquiry.setFollowup_date(datamodel.);
			model.addAttribute("assigned",crmService.getRoles());
			model.addAttribute("services",crmService.getServices());
			
			
			
			
			
			model.addAttribute("enquiry", enquiry);
		    } catch (NumberFormatException e) {
			e.printStackTrace();
		   }		
		return "adminpages/deals";
	}
	
/*	@RequestMapping("/leadsave")
	public String leadsave(Model model,HttpServletRequest request,LeadCommand lead){
		String followup_date = request.getParameter("followup_date");
		
		String status = request.getParameter("status");
		Leads led = new Leads();
		
		led.setNext_follwupdate(followup_date);
		led.setLead_status(status);
		
		boolean flag =  crmService.saveLead(led);	
	
			
		
		return "adminpages/leads";
	}
	*/
	
	@RequestMapping("/listofleads")
	public String listofleads(Model model){
		List<Leads> listleads = crmService.getAllLeads();
		System.out.println(listleads.size()+"sizeeeeeeeeeeeee");
		System.out.println(listleads);
		List<LeadsDto> leadsDto = new ArrayList<LeadsDto>();
		for(Leads lead:listleads)
		{
			LeadsDto l=new LeadsDto();
			l.setLead_id(lead.getFollowup_id());
			l.setEnquiry_id(lead.getEnquiry().getEnquiry_id());
			l.setAddress(lead.getEnquiry().getEnq_address());
			l.setEmail(lead.getEnquiry().getEnq_email());
			l.setMobile(lead.getEnquiry().getEnq_mobilenumber());
			l.setService(lead.getServiceMaster().getServicename());
			l.setUsename(lead.getEnquiry().getEnquiry_name());
			leadsDto.add(l);
			
		}
        model.addAttribute("listleads", leadsDto);
        
        
		return "adminpages/listofleads";
	}
		@RequestMapping(value="/convertleadsave", method = RequestMethod.POST)
	    public String leadsave(@ModelAttribute("enquiry")EnquiryCommand enquiry){
		
		System.out.println("ENTER");
		String imageurl = null;
		byte[] l = null;
		
	MultipartFile multipartFile =enquiry.getMultipart();
			
			try{
			   Random rand = new Random();
                   int imagerandom = rand.nextInt(9000000) + 1000000;
				l = multipartFile.getBytes();


				Map params = ObjectUtils.asMap("public_id",multipartFile .getOriginalFilename().split("\\.[^\\.]*$")[0]+"-JH-"+imagerandom+""+"");
				String string = multipartFile .getOriginalFilename();
				 System.out.println("params..........."+params);
				/*String basename = FilenameUtils.getBaseName(string);*/
				String extension = FilenameUtils.getExtension(string);
				params.put("resource_type", "auto");
				params.put("format", extension);
				Map uploadResult = null;
      			uploadResult = cloudinary.uploader().upload(l, params);
      			
				/*String d =	(String) uploadResult.get("url");*/
				String pid =(String) uploadResult.get("public_id");
				imageurl= pid;
         
	             }
	             
	             catch(Exception ex)
	             {
	            	 ex.printStackTrace();
	              
	             }
			enquiry.setImagepath(imageurl);
			crmService.saveConvertedLead(enquiry);
			
			
		   return "redirect:listofenquiry.do";
	}
		
		@RequestMapping(value="/convertdealsave", method = RequestMethod.POST)
	    public String dealsave(@ModelAttribute("enquiry")EnquiryCommand enquiry){
		System.out.println("controller......................"+enquiry.getEnq_id());
	
			crmService.saveConverteddeal(enquiry);
			
			
		   return "redirect:listofleads.do";
	}
		
		
		
	@RequestMapping(value="/listofdeals", method = RequestMethod.GET)
	    public String listofdeals(Model model){
		
		List<Deal> listdeals = crmService.getAllDeals();
		
		
		System.out.println(listdeals.size()+"sizeeeeeeeeeeeee");
		System.out.println(listdeals);
		
		
		List<DealDto> dealdto = new ArrayList<DealDto>();
		for(Deal deal:listdeals)
		{
			DealDto l=new DealDto();
			l.setDeal_id(deal.getDeal_id());
			l.setEnquiry_id(deal.getEnquiry().getEnquiry_id());
			l.setAddress(deal.getEnquiry().getEnq_address());
			l.setEmail(deal.getEnquiry().getEnq_email());
			l.setMobile(deal.getEnquiry().getEnq_mobilenumber());
			l.setService(deal.getServiceMaster().getServicename());
			l.setUsename(deal.getEnquiry().getEnquiry_name());
			dealdto.add(l);
			
		}
        model.addAttribute("listdeals", dealdto);
	
			return "adminpages/listofdeals";

	
}

}