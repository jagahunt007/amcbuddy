package com.amcbuddy.dto;

import java.io.Serializable;

public class LeadsDto implements Serializable {
	
	
	

	@Override
	public String toString() {
		return "LeadsDto [lead_id=" + lead_id + ", enquiry_id=" + enquiry_id
				+ ", usename=" + usename + ", mobile=" + mobile + ", address="
				+ address + ", email=" + email + ", service=" + service + "]";
	}
	private int lead_id;
	public int getLead_id() {
		return lead_id;
	}
	public void setLead_id(int lead_id) {
		this.lead_id = lead_id;
	}
	private int enquiry_id;
	public int getEnquiry_id() {
		return enquiry_id;
	}
	public void setEnquiry_id(int enquiry_id) {
		this.enquiry_id = enquiry_id;
	}
	private String usename;
	private String mobile;
	private String address;
	private String email;
	private String service;
	
	public String getUsename() {
		return usename;
	}
	public void setUsename(String usename) {
		this.usename = usename;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	
	

}
