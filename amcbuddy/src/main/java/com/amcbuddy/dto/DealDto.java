
package com.amcbuddy.dto;

public class DealDto {
	
	private int deal_id;
	private int enquiry_id;
	private String usename;
	@Override
	public String toString() {
		return "DealDto [deal_id=" + deal_id + ", enquiry_id=" + enquiry_id
				+ ", usename=" + usename + ", mobile=" + mobile + ", address="
				+ address + ", email=" + email + ", service=" + service + "]";
	}
	public int getDeal_id() {
		return deal_id;
	}
	public void setDeal_id(int deal_id) {
		this.deal_id = deal_id;
	}
	public int getEnquiry_id() {
		return enquiry_id;
	}
	public void setEnquiry_id(int enquiry_id) {
		this.enquiry_id = enquiry_id;
	}
	public String getUsename() {
		return usename;
	}
	public void setUsename(String usename) {
		this.usename = usename;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	private String mobile;
	private String address;
	private String email;
	private String service;


}
