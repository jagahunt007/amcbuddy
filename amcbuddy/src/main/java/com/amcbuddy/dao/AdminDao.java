package com.amcbuddy.dao;


	import java.util.List;
import java.util.Map;

	import com.amcbuddy.command.AdminModuleCommand;
import com.amcbuddy.model.AdminModule;
import com.amcbuddy.model.AdminSection;
import com.amcbuddy.model.BackendUsers;
import com.amcbuddy.model.CategoryMaster;
import com.amcbuddy.model.LocationMaster;
import com.amcbuddy.model.Role;
import com.amcbuddy.model.ServiceMaster;

	public interface AdminDao {

		  
		 /*Module viewing,saving,editing and deleting*/
				
			  public List<AdminModule> getadminmodules();
			  public void savemodule(AdminModuleCommand amcommand);
			  public AdminModule getModule(int mid);
			  public void updatemodule(AdminModuleCommand amcommand);
	          public void deleteModule(int mid);
			
			  
	      /*SubModule viewing,saving,editing and deleting*/
	      	  
	          public List<AdminSection> getsubmodules();
			  public void savesubmodules(AdminModuleCommand amcommand);
			  public AdminSection getSubModule(int sid);
			  public void updatesubmodule(AdminModuleCommand amcommand);
			  public void deleteSubModule(int sid);

		  
		   /*Category viewing,saving,editing and deleting*/
				 
			  public List<CategoryMaster> getCategories();
	          public void saveCategorie(CategoryMaster categorymaster);
			  public CategoryMaster getCategory(int cid);
			  public void updateCategory(CategoryMaster categorymaster);
	          public void deleteCategory(int cid);

			  public List<ServiceMaster> getServices();

			  public void saveService(ServiceMaster servicemaster);

			  public List<LocationMaster> getLocations();

			  public void saveLocation(LocationMaster locationmaster);

			 public List<Role> getRoles();

			 public void saveRole(Role role);

			 public List<BackendUsers> getbackendusers();

			 public void savebackenduser(BackendUsers backenduser);

			public Role getRole(String rolename);
			
			public String[] getPermissions(int rid);
			

			public List<AdminSection> getRoleSubModules(int[] subids);

			public void setAdminSection(List<AdminModule> modlist,List<AdminSection> sublist, int roleid);

			public List<AdminModule> getRoleModules(int[] m);

			public Map<String, List<AdminSection>> getuserrole(String uname);

			public int checkUser(String uname, String password);
			
			
			

			

			
	

	}


