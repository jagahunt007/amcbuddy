
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>AMC : Annual Maintance Contract</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Innovate Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="amcDesign/styles/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="amcDesign/styles/font-awesome.css" rel="stylesheet"> 
<link href='amcDesign/styles/simplelightbox.min.css' rel='stylesheet' type='text/css'>
<link href="amcDesign/styles/team.css" rel="stylesheet" type="text/css">
<link href="amcDesign/styles/style.css" rel="stylesheet" type="text/css" media="all" />


<!--web-fonts-->
<!-- <link href="//fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> -->
<!--//web-fonts-->
<!--//fonts-->
<!-- js -->

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<script type='text/javascript' src='amcDesign/script/jquery-2.2.3.min.js'></script>
<script type='text/javascript' src='amcDesign/script/bootstrap.js'></script>
<script>
$(document).ready(function(){
   $(".biliboard").click(function(){
   	alert("hello");
   	$(".board").show(500);
   });
       
    });

</script>
<style>

body {
    font-family: 'Segoe UI' !important;
}
@media ( min-width :1920px) {

	.w3-agileits-our-advantages-grids {
    position: absolute;
    bottom: 2em;
    width: 100%;
}
.w3layouts-our-advantages-grid {
    width: 24.5%;
    margin: 0 3px;
    background: rgba(0, 0, 0, 0.68);
    padding: 1em;
    border-bottom: 3px solid transparent;
}
.content {
  position: absolute;
  top: 50%;
  left:50%;
  transform: translate(-50%,-50%);
  }







/*a {
	display: block;
	margin: auto;
	margin-top: 50px;
	text-decoration: none;
	color: inherit;
}*/

@keyframes linear {
	
	from {
		background-position: 0 0;
	}
	to {
		background-position: 200px 0;
	}
	
}







.biliboard {
	width: 200px;
	padding: 20px;
	text-align: center;
	position: relative;
	background: #fff;
	color: #333;
	font: 20px;
}
.biliboard:before {
	content: '';
	display: block;
	height: 100%;
	width: 100%;
	border-radius: 3px;
	transform: scale( 1.02, 1.08 );
	position: absolute;
	background: #f00;
	background: linear-gradient( 90deg, #fafafa, #fafafa, #1D8EF7, #fafafa, #fafafa );
	background-position: 55px 0;
	top: 0;
	animation: linear 1s infinite linear;
	left: 0;
	z-index: -1;
}

a:hover,a:focus{
    text-decoration: none;
    outline: none;
}
.tab .nav-tabs{
    border-bottom: 1px solid #b6367f;
}
.tab .nav-tabs li{
    margin: 334px 33px 0 0;
    position: relative;
}
.tab .nav-tabs li.active:before{
    content: "";
    position: absolute;
    bottom: -28px;
    left: 84px;
    border: 14px solid transparent;
    border-top-color: #b6367f;
}
.tab .nav-tabs li.active:after{
    content: "";
    position: absolute;
    bottom: -24px;
    left: 84px;
    border: 12px solid transparent;
    border-top-color: #fff;
}
.tab .nav-tabs li a{
    border: none;
    padding: 13px 35px;
    font-size: 14px;
    color: #fff;
    background: transparent;
    border-radius: 0;
}
.tab .nav-tabs li a:hover{
    color: #b6367f;
}
.tab .nav-tabs li a i{
    display: block;
    text-align: center;
    margin-bottom: 5px;
}
.tab .nav-tabs li.active a,
.tab .nav-tabs li.active a:focus,
.tab .nav-tabs li.active a:hover{
    border: none;
    background: transparent;
    color: #fff;
    transition: background 0.20s linear 0s;
}
.tab .tab-content{
    font-size: 14px;
    color: #777;
    background: #fff;
    line-height: 25px;
    padding: 10px;
}
.tab .tab-content h3{
    font-size: 26px;
}
@media only screen and (max-width: 479px) {
    .tab .nav-tabs li a{
        padding: 10px;
    }
    .tab .nav-tabs li.active:before{
        left: 28px;
        bottom: -24px;
        border-width: 12px;
    }
    .tab .nav-tabs li.active:after{
        left: 30px;
        bottom: -20px;
        border-width: 10px;
    }
}


</style>
</head>
<body>
<!-- banner -->
	<div class="banner" id="home">
	<div class="header-top-agileits">
	<div class="container">
		 <ul class="agile_forms">
			<li><a class="active" href="#" data-toggle="modal" data-target="#myModal2"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a> </li>
			<li><a href="#" data-toggle="modal" data-target="#myModal3"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Register</a> </li>
		</ul> 
		<ul class="top-right-info-w3ls">
		
		<li><span>Call Us</span>0 123 456 789</li>
		<li><span>Need Any?</span>Book a Complaint</li>
		</ul>
		<div class="clearfix"></div>
	</div>
	</div>
	<div class="container">
		<!-- header -->	
		<header>
			
			<div class="header-bottom-agileits">
			<div class="w3-logo">
					<h1><a href="index.html"><i class="fa fa-cubes" aria-hidden="true"></i>AMC</a></h1>
				</div>
			<!-- navigation -->
			<nav class="navbar navbar-default shift">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>				  
				
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav">
					<li><a class="active" href="index.html">Home</a></li>
					<li><a class="scroll" href="#about">About</a></li>
					<li><a class="scroll" href="#services">Services</a></li>
					<li><a class="scroll" href="#gallery">Gallery</a></li>
					<li><a class="scroll" href="#contact">Contact</a></li>
					
				  </ul>
				
				</div><!-- /.navbar-collapse -->
				 
			</nav>
			
			</div>
			<div class="clearfix"></div>
		<!-- //navigation -->
		</header>


	<div class = "content" >
  	 <a href="#" class="biliboard"><i class="fa fa-location-arrow" aria-hidden="true" style="font-size: 25px;color: #b6367f;"></i>&nbsp; Visakhapatnam</a>
   <a href="#" class="biliboard">Click on AMC Service!</a>
  </div> 


       <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="tab board" role="tabpanel" style="display: none;">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="ElecandPlumb.do"  role="tab" ><i class="fa fa-envelope-o"></i>Electrcian + Plumbing</a></li>
                    <li role="presentation"><a href="liftttt.do"  role="tab" ><i class="fa fa-cube"></i>Lift Repair</a></li>
                    <li role="presentation"><a href="pestt.do"  role="tab" ><i class="fa fa-comment"></i>Pest Control</a></li>
                    <li role="presentation"><a href="register.do?serviceId=4"  role="tab" ><i class="fa fa-envelope-o"></i>Property Managment</a></li>
                    <li role="presentation"><a href="housekeepingg.do"  role="tab" ><i class="fa fa-cube"></i>House Keeping</a></li>
                    <li role="presentation"><a href="securityy.do"  role="tab" ><i class="fa fa-comment"></i>Security</a></li>
                </ul>
                <!-- Tab panes -->
               
            </div>
        </div>
    </div>
</div>            


</div>

	</div>
</div>
<!-- //banner -->
<!-- Modal -->

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Category</h4>
        </div>
        <div class="modal-body">
        		<div class = "text-center">
	          		<a href = "common.html"><button type = "button" class = "btn btn-primary">Common</button></a>
	          		<a href = "individual.html"><button type = "button" class = "btn btn-success">Individual</button></a>
        		</div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- Modal4 -->
		<div class="modal fade" id="myModal4" tabindex="-1" role="dialog">
			<div class="modal-dialog">
														<!-- Modal content-->
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>
																	
																	<div class="modal-info">
																	<img src="amcDesign/img/m1.jpg" alt=" " class="img-responsive" />
																	<h3 class="agileinfo_sign">Free shipping on order over <span>$150</span></h3>
																	<p class="para-agileits-w3layouts">Duis sit amet nisi quis leo fermentum vestibulum vitae eget augue. Nulla quam nunc, vulputate id urna at, tempor tincidunt metus. Sed feugiat quam nec mauris mattis malesuada.</p>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- //Modal4 -->	
<!-- Modal5 -->
		<div class="modal fade" id="myModal5" tabindex="-1" role="dialog">
			<div class="modal-dialog">
														<!-- Modal content-->
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>
																	
																	<div class="modal-info">
																	<img src="amcDesign/img/m2.jpg" alt=" " class="img-responsive" />
																	<h3 class="agileinfo_sign">We improve innovative <span>industrial solutions</span></h3>
																	<p class="para-agileits-w3layouts">Duis sit amet nisi quis leo fermentum vestibulum vitae eget augue. Nulla quam nunc, vulputate id urna at, tempor tincidunt metus. Sed feugiat quam nec mauris mattis malesuada.</p>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- //Modal5 -->
<!-- Modal6 -->
		<div class="modal fade" id="myModal6" tabindex="-1" role="dialog">
			<div class="modal-dialog">
														<!-- Modal content-->
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>
																	
																	<div class="modal-info">
																	<img src="amcDesign/img/m3.jpg" alt=" " class="img-responsive" />
																	<h3 class="agileinfo_sign">Your way to <span>faster productions</span></h3>
																	<p class="para-agileits-w3layouts">Duis sit amet nisi quis leo fermentum vestibulum vitae eget augue. Nulla quam nunc, vulputate id urna at, tempor tincidunt metus. Sed feugiat quam nec mauris mattis malesuada.</p>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- //Modal6 -->																
		<!-- Modal2 -->
		<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
														<div class="modal-dialog">
														<!-- Modal content-->
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>
																	
																	<div class="signin-form profile">
																	<h3 class="agileinfo_sign">Login</h3>	
																			<div class="login-form">
																				<form action="#" method="post">
																					<input type="email" name="email" placeholder="E-mail" required="">
																					<input type="password" name="password" placeholder="Password" required="">
																					<div class="tp">
																						<input type="submit" value="Login">
																					</div>
																				</form>
																			</div>
																			<div class="login-social-grids">
																				<ul>
																					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
																					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
																					<li><a href="#"><i class="fa fa-rss"></i></a></li>
																				</ul>
																			</div>
																			<p><a href="#" data-toggle="modal" data-target="#myModal3" > Don't have an account?</a></p>
																		</div>
																</div>
															</div>
														</div>
													</div>
													<!-- //Modal2 -->	
													<!-- Modal3 -->
													<div class="modal fade" id="myModal3" tabindex="-1" role="dialog">
														<div class="modal-dialog">
														<!-- Modal content-->
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>
																	
																	<div class="signin-form profile">
																	<h3 class="agileinfo_sign">Register</h3>	
																			<div class="login-form">
																				<form action="#" method="post">
																				   <input type="text" name="name" placeholder="Username" required="">
																					<input type="email" name="email" placeholder="Email" required="">
																					<input type="password" name="password" id="password1" placeholder="Password" required="">
																					<input type="password" name="password" id="password2" placeholder="Confirm Password" required="">
																					<input type="submit" value="Register">
																				</form>
																			</div>
																			<p><a href="#"> By clicking register, I agree to your terms</a></p>
																		</div>
																</div>
															</div>
														</div>
													</div>
													
		<!-- //Modal3 -->	
<!-- Modal -->
<!--about-->
 <div class="about-w3layouts" id="about">
<div class="container">
	<div class="tittle-agileinfo">
				<h2 class="title-w3" style="text-align: center;">Welcome to our Innovate AMC Module </h2>
				<div class="about-top">
					<p class="para-agileits-w3layouts">AMC is the most convenient and hassle free way to get your household work done. </p>
				</div>
			</div>
	<div class="about-left-agileits">
	</div>
	<div class="about-right">
			<ul>
					<li style="line-height: 26px;">A one-time fee for year-long assurance with your everyday devices like Television, Refrigerator, Washing Machine, Air-conditioners, kitchen appliances and many more</li>
					<li style="line-height: 26px;">Flexibility to design your own protection service plan.</li>
					<li style="line-height: 26px;">Warranty on workmanship</li>
					<li style="line-height: 26px;">We use only genuine and branded spares.</li>
					<li style="line-height: 26px;">We stand behind our work and offer a 30-day service guarantee on all workmanship</li>
					<li style="line-height: 26px;">Preventive maintenance visit during the year</li>
			</ul>
		<div class="pos-w3ls">
			<h4><span class="red-w3ls">Get</span> in touch</h4><p> with <br>us</p>
			<div class="clearfix"> </div>
		</div>
		<div class="button-w3layouts"><a href="#contact" class="scroll">Contact Us</a></div>
	</div>
	<div class="clearfix"> </div>
</div>
</div> 
<!--//about-->
<!--How We Works-->
<div class="container">
	<div class="tittle-agileinfo">
				<h2 class="title-w3" style="text-align: center;">How We Work</h2>
				<div class="about-top">
					<p class="para-agileits-w3layouts">You can book a call for any home repair service requirement right from your home. It's quite easy and here's how it works</p>
				</div>
			</div>


</div>
<!--End of How We Works-->			
<!--services-->
 <div class="about-w3layouts services" id="services">
<div class="container">
	<div class="col-md-5 services-left">
		<h5 class="title-w3 white-agileits">We Offer Different Services</h5>
	</div>
	<div class="col-md-7 services-info">
		<ul>
			<li><i class="fa fa-star" aria-hidden="true"></i>Quality Work</li>
			<li><i class="fa fa-tasks" aria-hidden="true"></i>Multi Projects</li>
			<li><i class="fa fa-cogs" aria-hidden="true"></i>Updated Tech</li>
			<li><i class="fa fa-users" aria-hidden="true"></i>Client Support</li>
		</ul>
		<ul>
			<li><i class="fa fa-users" aria-hidden="true"></i>Client Support</li>
			<li><i class="fa fa-cogs" aria-hidden="true"></i>Updated Tech</li>
			<li><i class="fa fa-tasks" aria-hidden="true"></i>Multi Projects</li>
			<li><i class="fa fa-star" aria-hidden="true"></i>Quality Work</li>
		</ul>
	</div>
	<div class="clearfix"> </div>
</div>
</div> 


	<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->

	

<script type="text/javascript" src="amcDesign/script/modernizr-2.6.2.min.js"></script>
<script src="amcDesign/script/responsiveslides.min.js"></script>
							<script>
								// You can also use "$(window).load(function() {"
								$(function () {
								  // Slideshow 4
								  $("#slider3").responsiveSlides({
									auto: true,
									pager:true,
									nav:false,
									speed: 500,
									namespace: "callbacks",
									before: function () {
									  $('.events').append("<li>before event fired.</li>");
									},
									after: function () {
									  $('.events').append("<li>after event fired.</li>");
									}
								  });
							
								});
							 </script>
<!-- Flexslider-js for-testimonials -->
<script type="text/javascript">
							$(window).load(function() {
								$("#flexiselDemo1").flexisel({
									visibleItems:1,
									animationSpeed: 1000,
									autoPlay: true,
									autoPlaySpeed: 3000,    		
									pauseOnHover: true,
									enableResponsiveBreakpoints: true,
									responsiveBreakpoints: { 
										portrait: { 
											changePoint:480,
											visibleItems: 1
										}, 
										landscape: { 
											changePoint:640,
											visibleItems:1
										},
										tablet: { 
											changePoint:768,
											visibleItems: 1
										}
									}
								});
								
							});
					</script>
					<script type="text/javascript" src="amcDesign/script/jquery.flexisel.js"></script>
<!-- //Flexslider-js for-testimonials -->
<!-- gallery-pop-up -->
	<script type="text/javascript" src="amcDesign/script/simple-lightbox.js"></script>
	<script>
		$(function(){
			var $gallery = $('.gallery a').simpleLightbox();

			$gallery.on('show.simplelightbox', function(){
				console.log('Requested for showing');
			})
			.on('shown.simplelightbox', function(){
				console.log('Shown');
			})
			.on('close.simplelightbox', function(){
				console.log('Requested for closing');
			})
			.on('closed.simplelightbox', function(){
				console.log('Closed');
			})
			.on('change.simplelightbox', function(){
				console.log('Requested for change');
			})
			.on('next.simplelightbox', function(){
				console.log('Requested for next');
			})
			.on('prev.simplelightbox', function(){
				console.log('Requested for prev');
			})
			.on('nextImageLoaded.simplelightbox', function(){
				console.log('Next image loaded');
			})
			.on('prevImageLoaded.simplelightbox', function(){
				console.log('Prev image loaded');
			})
			.on('changed.simplelightbox', function(){
				console.log('Image changed');
			})
			.on('nextDone.simplelightbox', function(){
				console.log('Image changed to next');
			})
			.on('prevDone.simplelightbox', function(){
				console.log('Image changed to prev');
			})
			.on('error.simplelightbox', function(e){
				console.log('No image found, go to the next/prev');
				console.log(e);
			});
		});
	</script>
<!-- //gallery-pop-up -->
<!--search-bar-->
		<script src="amcDesign/script/main.js"></script>	
<!--//search-bar-->

 <!-- start-smoth-scrolling -->
<script type="text/javascript" src="amcDesign/script/move-top.js"></script>
<script type="text/javascript" src="amcDesign/script/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
<!--js for bootstrap working-->
	<script src="amcDesign/script/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- password-script -->  
<script type="text/javascript">
		window.onload = function () {
			document.getElementById("password1").onchange = validatePassword;
			document.getElementById("password2").onchange = validatePassword;
		}
		function validatePassword(){
			var pass2=document.getElementById("password2").value;
			var pass1=document.getElementById("password1").value;
			if(pass1!=pass2)
				document.getElementById("password2").setCustomValidity("Passwords Don't Match");
			else
				document.getElementById("password2").setCustomValidity('');	 
				//empty string means no validation error
		}

</script>
<!-- //password-script --> 
</body>
</html>