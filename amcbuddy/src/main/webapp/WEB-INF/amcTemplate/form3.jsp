<!DOCTYPE html>
<html lang="en">
<head>
<title>AMC : Annual Maintance Contract</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Innovate Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="amcDesign/styles/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="amcDesign/styles/font-awesome.css" rel="stylesheet"> 
<link rel="stylesheet" href="amcDesign/styles/Footer-with-map.css">
<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
<script type='text/javascript' src='amcDesign/script/jquery-2.2.3.min.js'></script>
<script type='text/javascript' src='amcDesign/script/bootstrap.js'></script>
<style>
body, html{
   /*  height: 100%;
 	background-repeat: no-repeat;
 	background-color: #d3d3d3;*/
 	font-family: 'Oxygen', sans-serif;
}/**/
.navbar-default {
    background-color: #146eb4;
    border-color: #146eb4;
}
.logo
{
	color:white !important;
	font-size: 36px;
}
.navbar-default .navbar-nav > li > a
{
	color: white;
	line-height: 25px;
    font-size: 18px;
    letter-spacing: 0.5px;
    padding: 0 15px;
    margin: 10px;
}
.quoat
{
	font-size: 18px;
    color: #787878;
    line-height: 30px;
    padding: 10px 70px 0px 0;
}
.cont
{
	    padding: 12px 0px;
    text-align: left;
    font-size: 20px;
    color: #787878;
}

	   

.main{
 	margin:15px 15px;
}

label{
	margin-bottom: 15px;
}

/*input,
input::-webkit-input-placeholder {
    font-size: 11px;
    padding-top: 3px;
}*/

.main-login{
 	background-color: #fff;
    /* shadows and rounded borders */
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);

}
.form-control {
    height: auto!important;
padding: 8px 12px !important;
}


.main-center{
 	
    padding: 10px 40px;
	
box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);

}
span.input-group-addon i {
    color: #009edf;
    font-size: 17px;
}

.login-button{
	margin-top: 5px;
}

.login-register{
	font-size: 11px;
	text-align: center;
}
fieldset {
   
    border: 2px solid #146eb4;
    border-radius: 5px;
    padding: 15px;
}

fieldset legend {
    background: #146eb4;
    color: #fff;
    padding: 5px 10px ;
    font-size: 18px;
    border-radius: 5px;
}

 legend {
 	width:auto; 
 	}

    .radio-inline + .radio-inline
    {
        margin-left: 0px;
    }

</style>
<body>
<nav class="navbar navbar-default" role="navigation">
      <div class="container ">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand logo" href="#" >AMC</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
      
      <ul class="nav navbar-nav navbar-right">
          <li class=""><a href="#">Home</a></li>
        <li><a href="#">About Us</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services</a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
        <li><a href="#">Portfolio</a></li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Login</a>
   
        </li>
        
        
      </ul>
    </div><!-- /.navbar-collapse -->
      </div><!-- /.container-collapse -->
  </nav>

<div class = "container">
   <div class="well row">
        <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default"><i class="glyphicon glyphicon-home"></i></a>
            <a href="#" class="btn btn-default">HouseKeeping</a>
            <a href="#" class="btn btn-default">AMC / NewWork</a>
            <a href="#" class="btn btn-default">Commercial / Individual</a>

        </div>
    </div>
</div>

  
<div class="container height">
	<div class = "col-md-7">
		  <h2>Book a <span style="color:#146eb4;">PestControl</span> Service Today</h2>
		  <p class = "quoat">Book highly experienced in-house professionals & get your cleaning service done, Instantly.</p>


	  <h2>Why Choose <span style="color:#146eb4;">AMC ?</span></h2>
	
		    <div class="row cont">
		        <div class="col-xs-12 col-md-6">
		            <i class="fa fa-check-square-o" aria-hidden="true" style="color: #146eb4"></i> &nbsp;High end cleaning machinery
		        </div>
		        <div class="col-xs-12 col-md-6">
		             <i class="fa fa-check-square-o" aria-hidden="true" style="color: #146eb4"></i> &nbsp;High end cleaning machinery
		        </div>
		    </div>

		        <div class="row cont">
		        <div class="col-xs-12 col-md-6">
		            <i class="fa fa-check-square-o" aria-hidden="true" style="color: #146eb4"></i> &nbsp;High end cleaning machinery
		        </div>
		        <div class="col-xs-12 col-md-6">
		             <i class="fa fa-check-square-o" aria-hidden="true" style="color: #146eb4"></i> &nbsp;High end cleaning machinery
		        </div>
		    </div>
		
	  </div> 

	

 	<div class = "col-md-5">
 	<fieldset >
		<!-- <h2 style="text-align: center;">Book a <span style="color:#146eb4;">Electrician</span> Now !</h2> -->
<legend><b>HouseKeeping Form </b> </legend>
		<section>




		
				<div class="main-login main-center">
				<h5>Sign up once and watch any of our free demos.</h5>
					<form class="" method="post" action="#">
						
			
        <div class="form-group">
              <label for="email" class="cols-sm-2 control-label">Type Of House Keeping</label>
              <div class="cols-sm-6">
                   <form>
                        <div class="checkbox">
                          <label><input type="checkbox" value="">General Pest - Rs.1.50paise/sq.ft/month</label>
                        </div>
                      </form>
              </div>
            </div>

						<!-- <div class="form-group">
							<label for="username" class="cols-sm-2 control-label">No.of Flats</label>
							<div class="cols-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="username" id="username"  placeholder="Enter your No of flats"/>
								</div>
							</div>
						</div>


						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">No.of Lifts</label>
							<div class="cols-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="username" id="username"  placeholder="Enter your No of Lifts"/>
								</div>
							</div>
						</div> -->


						

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Contract Type (Discount + Gst)</label>
							<div class="cols-sm-6">
								<form>
                                    <label class="radio-inline">
                                      <input type="radio" name="optradio">3 Years
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="optradio">1 Year
                                    </label>
                                   
                                  </form>
							</div>
						</div>

							<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Schedule of Payment (Inclusion GST)</label>
							<div class="cols-sm-6">
							 <form>
                                    <label class="radio-inline">
                                      <input type="radio" name="optradio">3 Years
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="optradio">1 Year
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="optradio">Half Years
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="optradio">Quarterly
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="optradio">Monthly
                                    </label>

                                   
                                  </form>
							</div>
						</div>

							<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Generate Coupon for Discount</label>
							<div class="cols-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-mobile" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="password" id="password"  placeholder="Enter your Coupon"/>
								</div>
							</div>
						</div>

                            <div class="form-group">
                            <label for="password" class="cols-sm-2 control-label">Total Amount to be Paid </label>
                            <div class="cols-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class=" " aria-hidden="true">INR</i></span>
                                    <input type="text" class="form-control" name="password" id="password"  placeholder="Enter your Amount"/>
                                </div>
                            </div>
                        </div>
				
				

						<div class="form-group ">
							<a href="" target="_blank" type="button" id="button" class="btn btn-primary btn-lg btn-block login-button">Submit</a>
						</div>
						
					</form>
				</div>
	
		</fieldset>
	</div> 
</div> 
	

 <footer id="myFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h5>Get started</h5>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Sign up</a></li>
                        <li><a href="#">Downloads</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>About us</h5>
                    <ul>
                        <li><a href="#">Company Information</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Reviews</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>Support</h5>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Help desk</a></li>
                        <li><a href="#">Forums</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>Legal</h5>
                    <ul>
                        <li><a href="#">Terms of Service</a></li>
                        <li><a href="#">Terms of Use</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <!-- Here we use the Google Embed API to show Google Maps. -->
            <!-- In order for this to work in your project you will need to generate a unique API key.  -->
            <iframe id="map-container" frameborder="0" style="border:0"
                src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJOwg_06VPwokRYv534QaPC8g&key=AIzaSyBdJm9Amm4KALkKlZObWn40dcpRyH119zg" >
            </iframe>
        </div>
        <div class="social-networks">
            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
            <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
        </div>
        <div class="footer-copyright">
            <p>© 2016 Copyright Text </p>
        </div>
    </footer>

</body>
</head>
</html>