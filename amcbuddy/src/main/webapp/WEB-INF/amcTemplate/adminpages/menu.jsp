<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix ="c" %>
   <%@ page isELIgnored="false" %>
 


<div id="sidebar-collapse" class="col-sm-3 col-lg-3 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">Username</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		
		<ul class="nav menu">
			<li class="active"><a href="index.do"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
		     
                <c:forEach items="${modulelist}" var="mname" varStatus="counter" >
                   
                
               <li class="parent" ><a data-toggle="collapse" data-target="#${counter.count}" >
					<em class="fa fa-navicon">&nbsp;</em>${mname.key}<span data-toggle="collapse" href="#${counter.count}" class="icon pull-right"><em class="fa fa-plus"></em></span>
					</a> 
					
					<ul  class="children collapse" id="${counter.count}">
                       <c:forEach items="${mname.value}" var="smodules" >
                             
                       
					     <li>
                             <a href=${smodules.url}><span class="fa fa-arrow-right">&nbsp;</span>${smodules.sectionname}</a>
                           </li>
                            </c:forEach>
                       </ul>
                  
                </li>
              </c:forEach> 
              
              
              
              
            
             
          
            <li><a href="adminlogin.do"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
         </ul>
         
        </div>
         

