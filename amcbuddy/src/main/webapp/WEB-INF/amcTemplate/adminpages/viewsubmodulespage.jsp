<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.amc">
    <tiles:putAttribute name="body">

<div class = "col-lg-4"></div>


<div class="container">
<div class = "col-lg-6">
  <h2>SubModules</h2>
<div class="table-responsive">          
  <table class="table">
<thead>
<tr>
<th>Sub Module Id</th>
<th>Module Name</th>
<th>Sub Module Name</th>
<th>url</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<c:forEach  var="submodules" items="${submodulelist}">
<tr>
<td>${submodules.section_id}</td>
<td>${submodules.menuo.modulename}</td>
<td>${submodules.sectionname}</td>
<td>${submodules.url}</td>
<td><a href="editsubmodule.do?subid=${submodules.section_id}"><button type="button">Edit</button></a>            <a href="deletesubmodule.do?subid=${submodules.section_id}"><button type="button">Delete</button></a></td>
</tr>
</c:forEach>
</tbody>
</table>
</div>
  <a href="addsubmodule.do"><button type="button" class="btn btn-save">add submodule</button></a>
 </div>

</div>

</tiles:putAttribute>
</tiles:insertDefinition>
