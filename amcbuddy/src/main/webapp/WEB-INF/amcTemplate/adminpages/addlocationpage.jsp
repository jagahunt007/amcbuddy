<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.amc">
    <tiles:putAttribute name="body">

<div class="container">
  
  	<h2 style="text-align:center">Locations adding page</h2>
  	<form  action="savelocations.do" method="get">
        <div class = "col-md-3"></div>
        <div class = "col-md-6">
            <div class="well">
   
  <div class="form-group">
    <label for="locationname">Enter Location Name:</label>
    <input type="text" class="form-control" name="locationname"/>
   </div>
   <div class="form-group">
    <label for="pincode">Enter Pincode:</label>
    <input type="text" class="form-control" name="pincode"/>
   </div>
   <div class="form-group">
    <label for="status">Enter status:</label>
    <select name="status" class="form-control">
      <option value="select">select</option>
      <option value="yes">Active</option>
      <option value="no">Inactive</option>
    </select>
   </div>
  
  <button type="submit" class="btn btn-default">Save</button>
        </div>
            </form>
        
  </div>
</div>

</tiles:putAttribute>
</tiles:insertDefinition>
