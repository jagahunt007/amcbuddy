<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.amc">
    <tiles:putAttribute name="body">
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
                <ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active" style="text-align:center">Dashboard</li>
			</ol>
        </div>    
		
		<div class="panel panel-container">
			<div class="row">
    
                <div class = "col-lg-3"></div>
            
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-teal panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-shopping-cart color-blue"></em>
							<div class="large">120</div>
							<div class="text-muted">ELectrician + Plumbing</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-blue panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-comments color-orange"></em>
							<div class="large">52</div>
							<div class="text-muted">Lift Repair</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-orange panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-users color-teal"></em>
							<div class="large">24</div>
							<div class="text-muted">House Keeping</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-red panel-widget ">
						<div class="row no-padding"><em class="fa fa-xl fa-search color-red"></em>
							<div class="large">25.2k</div>
							<div class="text-muted">Property Managment</div>
						</div>
					</div>
				</div>

						
                

			</div><!--/.row-->
            
            	<div class="row">
    
                <div class = "col-lg-3"></div>
            
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-teal panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-shopping-cart color-blue"></em>
							<div class="large">120</div>
							<div class="text-muted">ELectrician + Plumbing</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-blue panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-comments color-orange"></em>
							<div class="large">52</div>
							<div class="text-muted">Lift Repair</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-orange panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-users color-teal"></em>
							<div class="large">24</div>
							<div class="text-muted">House Keeping</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-red panel-widget ">
						<div class="row no-padding"><em class="fa fa-xl fa-search color-red"></em>
							<div class="large">25.2k</div>
							<div class="text-muted">Property Managment</div>
						</div>
					</div>
				</div>

						
                

			</div><!--/.row-->
            
            	<div class="row">
    
                <div class = "col-lg-3"></div>
            
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-teal panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-shopping-cart color-blue"></em>
							<div class="large">120</div>
							<div class="text-muted">ELectrician + Plumbing</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-blue panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-comments color-orange"></em>
							<div class="large">52</div>
							<div class="text-muted">Lift Repair</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-orange panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-users color-teal"></em>
							<div class="large">24</div>
							<div class="text-muted">House Keeping</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-red panel-widget ">
						<div class="row no-padding"><em class="fa fa-xl fa-search color-red"></em>
							<div class="large">25.2k</div>
							<div class="text-muted">Property Managment</div>
						</div>
					</div>
				</div>

						
                

			</div><!--/.row-->
            
            	<div class="row">
    
                <div class = "col-lg-3"></div>
            
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-teal panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-shopping-cart color-blue"></em>
							<div class="large">120</div>
							<div class="text-muted">ELectrician + Plumbing</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-blue panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-comments color-orange"></em>
							<div class="large">52</div>
							<div class="text-muted">Lift Repair</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-orange panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-users color-teal"></em>
							<div class="large">24</div>
							<div class="text-muted">House Keeping</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-red panel-widget ">
						<div class="row no-padding"><em class="fa fa-xl fa-search color-red"></em>
							<div class="large">25.2k</div>
							<div class="text-muted">Property Managment</div>
						</div>
					</div>
				</div>

						
                

			</div><!--/.row-->

	<!--/.row-->



            </div>
			
</tiles:putAttribute>
</tiles:insertDefinition>