<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.amc">
    <tiles:putAttribute name="body">

<div class = "col-lg-4"></div>

<div class="container">
<div class = "col-lg-6">
  <h2>Services</h2>
<div class="table-responsive">          
  <table class="table">
  <thead>
<tr>
<th>Service Id</th>
<th>Service Name</th>

</tr>
</thead>
<tbody>
<c:forEach  var="services" items="${servicelist}">
<tr>
<td>${services.serviceid}</td>
<td>${services.servicename}</td>

</tr>
</c:forEach>
</tbody>
</table>
</div>

<a href="addservice.do"><button type="button" class="btn btn-save">addservice</button></a>
</div>
</div>



</tiles:putAttribute>
</tiles:insertDefinition>