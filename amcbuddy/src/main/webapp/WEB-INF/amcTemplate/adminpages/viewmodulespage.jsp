<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.amc">
    <tiles:putAttribute name="body">

<div class = "col-lg-4"></div>

<div class="container">
<div class = "col-lg-6">
<h2>Modules</h2>
<div class="table-responsive">          
  <table class="table">
<thead>
<tr>
<th>Module Id</th>
<th>Module Name</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<c:forEach  var="modules" items="${modlist}">
<tr>
<td>${modules.module_id}</td>
<td>${modules.modulename}</a></td>
<td><a href="editmodule.do?modid=${modules.module_id}"><button type="button">Edit</button></a>            <a href="deletemodule.do?modid=${modules.module_id}"><button type="button">Delete</button></a></td>
</tr>
</c:forEach>
</tbody>
</table>
</div>


<a href="addmodule.do"><button type="button" class="btn btn-save">add module</button></a>
</div>
</div>
</tiles:putAttribute>
</tiles:insertDefinition>