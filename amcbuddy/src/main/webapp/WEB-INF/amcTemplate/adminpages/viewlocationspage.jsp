<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix ="c" %>
   <%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<table>
<tr>
<th>Location Id</th>
<th>Location Name</th>
<th>Pincode</th>
</tr>
<c:forEach  var="locations" items="${locationlist}">
<tr>
<td>${locations.locationid}</td>
<td>${locations.locationname}</td>
<td>${locations.pincode}</td>
</tr>
</c:forEach>
</table>

</br>

<a href="addmodule.do">addmodule</a>
</body>
</html>