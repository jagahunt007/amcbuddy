<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.amc">
    <tiles:putAttribute name="body">


<div class = "col-lg-4"></div>

<div class="container">
<div class = "col-lg-6">
  <h2>Locations</h2>
<div class="table-responsive">          
  <table class="table">
<thead>
<tr>
<th>Location Id</th>
<th>Location Name</th>
<th>Pincode</th>
</tr>
</thead>
<tbody>
<c:forEach  var="locations" items="${locationlist}">
<tr>
<td>${locations.locationid}</td>
<td>${locations.locationname}</td>
<td>${locations.pincode}</td>
</tr>
</c:forEach>
</tbody>
</table>
</div>
  <a href="addlocations.do"><button type="button" class="btn btn-save">addlocation</button></a>
 </div>
 </div>



</tiles:putAttribute>
</tiles:insertDefinition>
