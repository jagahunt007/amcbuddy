<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AMC -Admin</title>
	<link href="amcDesign/adminweb/css/bootstrap.min.css" rel="stylesheet">
	<link href="amcDesign/adminweb/css/font-awesome.min.css" rel="stylesheet">
	<link href="amcDesign/adminweb/css/datepicker3.css" rel="stylesheet">
	<link href="amcDesign/adminweb/css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	
</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="#"><span>AMC</span>Admin</a>
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-envelope"></em><span class="label label-danger">15</span>
					</a>
						<ul class="dropdown-menu dropdown-messages">
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
									</a>
									<div class="message-body"><small class="pull-right">3 mins ago</small>
										<a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
									<br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
									</a>
									<div class="message-body"><small class="pull-right">1 hour ago</small>
										<a href="#">New message from <strong>Jane Doe</strong>.</a>
									<br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="all-button"><a href="#">
									<em class="fa fa-inbox"></em> <strong>All Messages</strong>
								</a></div>
							</li>
						</ul>
					</li>
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-bell"></em><span class="label label-info">5</span>
					</a>
						<ul class="dropdown-menu dropdown-alerts">
							<li><a href="#">
								<div><em class="fa fa-envelope"></em> 1 New Message
									<span class="pull-right text-muted small">3 mins ago</span></div>
							</a></li>
							<li class="divider"></li>
							<li><a href="#">
								<div><em class="fa fa-heart"></em> 12 New Likes
									<span class="pull-right text-muted small">4 mins ago</span></div>
							</a></li>
							<li class="divider"></li>
							<li><a href="#">
								<div><em class="fa fa-user"></em> 5 New Followers
									<span class="pull-right text-muted small">4 mins ago</span></div>
							</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">Username</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="active"><a href="index.do"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>


			<li class="parent "><a data-toggle="collapse" href="#sub-item-5">
				<em class="fa fa-navicon">&nbsp;</em> CRM <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-5">
					<li><a class="" href="PlumbandElec.html">
						<span class="fa fa-arrow-right">&nbsp;</span> Quaries
					</a></li>
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Leads
					</a>
					</li>
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Deals
					</a></li>
                    	<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Registerd Clients
					</a></li>
                    	<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Payments
					</a></li>
                    	<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Renewals
					</a></li>
					 	<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Invistigation Reports
					</a></li>
				</ul>
			</li>
			
					<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-navicon">&nbsp;</em> Enquiries <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="addEnquiry.do">
						<span class="fa fa-arrow-right">&nbsp;</span> Add Enquiry
					</a></li>
					<li><a class="" href="listofenquiry.do">
						<span class="fa fa-arrow-right">&nbsp;</span> List Enquiry
					</a></li>
					
				</ul>
			</li>

		
<<<<<<< HEAD
<!-- 
				<li class="parent "><a data-toggle="collapse" href="#sub-item-3">
				<em class="fa fa-navicon">&nbsp;</em> Leads <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
			
			</li> -->
=======

>>>>>>> refs/heads/SWATHI
						<li class="parent "><a data-toggle="collapse" href="#sub-item-4">
				<em class="fa fa-navicon">&nbsp;</em> Leads <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-4">
					
<<<<<<< HEAD
				</ul>
			</li>

						<li class="parent "><a data-toggle="collapse" href="#sub-item-7">
				<em class="fa fa-navicon">&nbsp;</em>Deals <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-7">
					<li><a class="" href="deals.do">
						<span class="fa fa-arrow-right">&nbsp;</span> Add Enquiry
					</a></li>
					<li><a class="" href="listofdeals.do">
						<span class="fa fa-arrow-right">&nbsp;</span> List Enquiry
=======
					<li><a class="" href="listofleads.do">
						<span class="fa fa-arrow-right">&nbsp;</span> ListOfLeads
>>>>>>> refs/heads/SWATHI
					</a></li>
					
				</ul>
			</li>

						<li class="parent "><a data-toggle="collapse" href="#sub-item-7">
				<em class="fa fa-navicon">&nbsp;</em>Deals <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-7">
					
					<li><a class="" href="listofdeals.do">
						<span class="fa fa-arrow-right">&nbsp;</span>ListOfDeals
					</a></li>
					
				</ul>
			</li>
				<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
					<em class="fa fa-navicon">&nbsp;</em> Users <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
					</a>
					<ul class="children collapse" id="sub-item-1">
						<li><a class="" href="PlumbandElec.html">
							<span class="fa fa-arrow-right">&nbsp;</span> Plumber + Electrician
						</a></li>
						<li><a class="" href="#">
							<span class="fa fa-arrow-right">&nbsp;</span> Lift Repair
						</a></li>
						<li><a class="" href="#">
							<span class="fa fa-arrow-right">&nbsp;</span> House Keeping
						</a></li>
	                    	<li><a class="" href="#">
							<span class="fa fa-arrow-right">&nbsp;</span> Pest Control
						</a></li>
	                    	<li><a class="" href="#">
							<span class="fa fa-arrow-right">&nbsp;</span> Propert Management
						</a></li>
	                    	<li><a class="" href="#">
							<span class="fa fa-arrow-right">&nbsp;</span> Security
						</a></li>
					</ul>
				</li>


						<li class="parent "><a data-toggle="collapse" href="#sub-item-6">
				<em class="fa fa-navicon">&nbsp;</em> User Management <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-6">
					<li><a class="" href="usermanagement.do">
						<span class="fa fa-arrow-right">&nbsp;</span> Users
					</a></li>
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Roles
					</a></li>
					
				</ul>
			</li>
            
            <li><a href="charts.do"><em class="fa fa-bar-chart">&nbsp;</em>Payment</a></li>
			<li><a href="elements.do"><em class="fa fa-toggle-off">&nbsp;</em> Manage Permission</a></li>
            <li><a href="panels.do"><em class="fa fa-clone">&nbsp;</em>Approval</a></li>
            <li><a href="panels.do"><em class="fa fa-clone">&nbsp;</em>Report Employee Performance</a></li>
			<li><a href="adminlogin.do"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->

			<div class="col-sm-9  col-lg-10 col-lg-offset-2 main">

						

							   <form class="form-horizontal" action="updateenquiry.do" method="GET" modelAttribute="enquiry" >

							    <h2>Enquiry<span style="color:#146eb4;">AMC</span> Service </h2>
							     <div class="form-group">
					      <label class="control-label col-sm-2" for="pwd">Name:</label>
					      <div class="col-sm-6">          
					        <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" value="${enquiry.enquiry_name}">
					      </div>
					    </div>

					       <div class="form-group">
					      <label class="control-label col-sm-2" for="pwd">Contact:</label>
					      <div class="col-sm-6">          
					        <input type="text" class="form-control" id="phonenumber" placeholder="Enter Mobile Number" name="phonenumber" value="${enquiry.enq_mobilenumber }">
					      </div>
					    </div>

					    <div class="form-group">
					      <label class="control-label col-sm-2" for="email">Email:</label>
					      <div class="col-sm-6">
					        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="${enquiry.enq_email }">
					      </div>
					    </div>
					    <div class="form-group">
					      <label class="control-label col-sm-2" for="pwd">Address:</label>
					      <div class="col-sm-6">          
					         <input type="text" class="form-control" rows="5" id="address" name="address" value="${enquiry.enq_address }" >
					      </div>
					    </div>

					        <div class="form-group">
					      <label class="control-label col-sm-2" for="pwd">AssignTo:</label>
					      <div class="col-sm-6">          
					      <select class="form-control" id="sel1">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                      </select>
					      </div>
					    </div>

						  <div class="form-group">
					      <label class="control-label col-sm-2" for="pwd">Service Looking for:</label>
					      <div class="col-sm-6">          
					      <select class="form-control" id="sel1">
                        <option>Electrical & Plumbing</option>
                        <option>Lift Maintanance</option>
                        <option>Pest Control</option>
                        <option>House Keeping</option>
                      </select>
					      </div>
					    </div>



					    <div class="form-group">
					      <label class="control-label col-sm-2" for="email">Note:</label>
					      <div class="col-sm-6">
					      <input type="text" class="form-control" rows="2" id="description" name="description" value="${enquiry.enq_description }">
					      </div>
					    </div> 

					     <div class="form-group">
					      <label class="control-label col-sm-2" for="email">Next Followup Date:</label>
					      <div class="col-sm-6">
					   <input type="date" class="form-control" name="bday" id="followup_date" name="followup_date" value="${enquiry.enq_followupdate }">
					      </div>
					    </div>

 
					   
					    <div class="form-group">        
					      <div class="col-sm-offset-2 col-sm-10">
					        <button type="submit" class="btn btn-primary">Update</button>
					         <button type="submit" class="btn btn-danger">Cancel</button>
					      </div>
					    </div>
					  </form>
	
	</div>	<!--/.main-->
	  

	<script src="amcDesign/adminweb/js/jquery-1.11.1.min.js"></script>
	<script src="amcDesign/adminweb/js/bootstrap.min.js"></script>
	<script src="amcDesign/adminweb/js/chart.min.js"></script>
	<script src="amcDesign/adminweb/js/chart-data.js"></script>
	<script src="amcDesign/adminweb/js/easypiechart.js"></script>
	<script src="amcDesign/adminweb/js/easypiechart-data.js"></script>
	<script src="amcDesign/adminweb/js/bootstrap-datepicker.js"></script>
	<script src="amcDesign/adminweb/js/custom.js"></script>
	<script>
	window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
	var chart2 = document.getElementById("bar-chart").getContext("2d");
	window.myBar = new Chart(chart2).Bar(barChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
	var chart3 = document.getElementById("doughnut-chart").getContext("2d");
	window.myDoughnut = new Chart(chart3).Doughnut(doughnutData, {
	responsive: true,
	segmentShowStroke: false
	});
	var chart4 = document.getElementById("pie-chart").getContext("2d");
	window.myPie = new Chart(chart4).Pie(pieData, {
	responsive: true,
	segmentShowStroke: false
	});
	var chart5 = document.getElementById("radar-chart").getContext("2d");
	window.myRadarChart = new Chart(chart5).Radar(radarData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.05)",
	angleLineColor: "rgba(0,0,0,.2)"
	});
	var chart6 = document.getElementById("polar-area-chart").getContext("2d");
	window.myPolarAreaChart = new Chart(chart6).PolarArea(polarData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	segmentShowStroke: false
	});
};
	</script>	
</body>
</html>
