<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.amc">
    <tiles:putAttribute name="body">
<div class = "col-lg-4"></div>


<div class="container">
<div class = "col-lg-6">
  <h2>Backend users</h2>
<div class="table-responsive">          
  <table class="table">
<thead>
<tr>
<th>Backend User Id</th>
<th>Name</th>
<th>UserName</th>
<th>Password</th>
<th>Role</th>
<th>Mobile Number</th>
<th>Email</th>
<th>Created Date</th>
</tr>
</thead>
<tbody>
<c:forEach  var="busers" items="${buserlist}">
<tr>
<td>${busers.backend_user_id}</td>
<td>${busers.name}</td>
<td>${busers.username}</td>
<td>${busers.password}</td>
<td>${busers.work}</td>
<td>${busers.mobileno}</td>
<td>${busers.email}</td>
<td>${busers.createdOn}</td>
</tr>
</c:forEach>
</tbody>
</table>
</div>
  <a href="addbackenduser.do"><button type="button" class="btn btn-save">addbackenduser</button></a>
</div>
</div>




</tiles:putAttribute>
</tiles:insertDefinition>
