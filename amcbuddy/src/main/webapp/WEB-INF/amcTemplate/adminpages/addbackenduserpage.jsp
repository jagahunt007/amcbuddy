<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.amc">
    <tiles:putAttribute name="body">

<div class="container">
  
  	<h2 style="text-align:center">Backendusers adding page</h2>
  	<form  action="savebackenduser.do" method="get">
        <div class = "col-md-3"></div>
        <div class = "col-md-6">
            <div class="well">
   <div class="form-group">
    <label for="role">Select Role:</label>
    <select name="work" class="form-control">
      <option value="select">select</option>
      <c:forEach  var="roles" items="${rlist}">
      <option value="${roles.role_name}">${roles.role_name}</option>
      </c:forEach> 
    </select>
   </div>
  <div class="form-group">
    <label for="name">Enter Name:</label>
    <input type="text" class="form-control" name="name"/>
   </div>
   <div class="form-group">
    <label for="username">Enter Username:</label>
    <input type="text" class="form-control" name="username"/>
   </div>
   <div class="form-group">
    <label for="password">Enter Password:</label>
    <input type="password" class="form-control" name="password"/>
   </div>
   <div class="form-group">
    <label for="mobileno">Enter Mobilenumber:</label>
    <input type="tel" class="form-control" name="mobileno"/>
   </div>
   <div class="form-group">
    <label for="email">Enter EmailId:</label>
    <input type="email" class="form-control" name="email"/>
   </div>
  
  <button type="submit" class="btn btn-default">Save</button>
        </div>
            </form>
        
  </div>
</div>

</tiles:putAttribute>
</tiles:insertDefinition>
