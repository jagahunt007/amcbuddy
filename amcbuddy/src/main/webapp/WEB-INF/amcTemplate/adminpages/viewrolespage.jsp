<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.amc">
    <tiles:putAttribute name="body">

<div class = "col-lg-4"></div>
<div class="container">
<div class = "col-lg-6">
  <h2>Roles</h2>
<div class="table-responsive">          
  <table class="table">
<thead>
<tr>
<th>Role Id</th>
<th>Role Name</th>
<th>Role Type</th>
<th>Description</th>
<th>Status</th>
</tr>
</thead>
<tbody>
<c:forEach  var="roles" items="${rolelist}">
<tr>
<td>${roles.role_id}</td>
<td>${roles.role_name}</td>
<td>${roles.role_type}</td>
<td>${roles.description}</td>
<td>${roles.status}</td>
</tr>
</c:forEach>
</tbody>
</table>
</div>
  <a href = "addrole.do"><button type="button" class="btn btn-save">AddRole</button></a>
  </div>




</div>



</tiles:putAttribute>
</tiles:insertDefinition>