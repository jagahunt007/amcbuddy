<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.amc">
    <tiles:putAttribute name="body">



<div class="container">
  
  	<h2 style="text-align:center">Modules adding page</h2>
  	<form  action="savemodule.do" method="get" modelattribute="mli">
        <div class = "col-md-3"></div>
        <div class = "col-md-6">
            <div class="well">
            <input type="hidden" name="module_id" value="${mli.module_id}"/>
  <div class="form-group">
    <label for="modulename">Enter Modulename:</label>
    <input type="text" class="form-control" name="modulename" value="${mli.modulename}"/>
  </div>
  
  <div class="form-group">
    <label for="status">Enter status:</label>
    <select name="module_status" class="form-control">
      <option value="select">select</option>
      <option value="yes">Active</option>
      <option value="no">Inactive</option>
    </select>
   </div>
  <button type="submit" class="btn btn-default">Save</button>
        </div>
            </form>
        
  </div>
</div>

</tiles:putAttribute>
</tiles:insertDefinition>

