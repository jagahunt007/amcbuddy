<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AMC - Admin</title>
	<link href="amcDesign/adminweb/css/bootstrap.min.css" rel="stylesheet">
	<link href="amcDesign/adminweb/css/font-awesome.min.css" rel="stylesheet">
	<link href="amcDesign/adminweb/css/datepicker3.css" rel="stylesheet">
	<link href="amcDesign/adminweb/css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="amcDesign/adminweb/js/html5shiv.js"></script>
	<script src="amcDesign/adminweb/js/respond.min.js"></script>	
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="#"><span>AMC</span>Admin</a>
				<ul class="nav navbar-top-links navbar-right">

						<li><a href = "orders.html"><button type = "button" class="btn btn-primary">Orders</button></a></li>
				
					<!-- <li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-bell"></em><span class="label label-info">5</span>
					</a>
						<ul class="dropdown-menu dropdown-alerts">
							<li><a href="#">
								<div><em class="fa fa-envelope"></em> 1 New Message
									<span class="pull-right text-muted small">3 mins ago</span></div>
							</a></li>
							<li class="divider"></li>
							<li><a href="#">
								<div><em class="fa fa-heart"></em> 12 New Likes
									<span class="pull-right text-muted small">4 mins ago</span></div>
							</a></li>
							<li class="divider"></li>
							<li><a href="#">
								<div><em class="fa fa-user"></em> 5 New Followers
									<span class="pull-right text-muted small">4 mins ago</span></div>
							</a></li>
						</ul>
					</li> -->
				</ul>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">Username</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="active"><a href="index.do"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>


			<li class="parent "><a data-toggle="collapse" href="#sub-item-5">
				<em class="fa fa-navicon">&nbsp;</em> CRM <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-5">
					<li><a class="" href="PlumbandElec.html">
						<span class="fa fa-arrow-right">&nbsp;</span> Quaries
					</a></li>
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Leads
					</a>
					</li>
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Deals
					</a></li>
                    	<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Registerd Clients
					</a></li>
                    	<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Payments
					</a></li>
                    	<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Renewals
					</a></li>
					 	<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Invistigation Reports
					</a></li>
				</ul>
			</li>
			
			<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-navicon">&nbsp;</em> Enquiries <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="addEnquiry.do">
						<span class="fa fa-arrow-right">&nbsp;</span> Add Enquiry
					</a></li>
					<li><a class="" href="ListaddEnquiry.do">
						<span class="fa fa-arrow-right">&nbsp;</span> List Enquiry
					</a></li>
					
				</ul>
			</li>

		
<!-- 
				<li class="parent "><a data-toggle="collapse" href="#sub-item-3">
				<em class="fa fa-navicon">&nbsp;</em> Leads <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
			
			</li> -->
			<li class="parent "><a data-toggle="collapse" href="#sub-item-4">
				<em class="fa fa-navicon">&nbsp;</em> Leads <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-4">
					<li><a class="" href="leads.do">
						<span class="fa fa-arrow-right">&nbsp;</span> Add Enquiry
					</a></li>
					<li><a class="" href="listofleads.do">
						<span class="fa fa-arrow-right">&nbsp;</span> List Enquiry
					</a></li>
					
				</ul>
			</li>

			<li class="parent "><a data-toggle="collapse" href="#sub-item-7">
				<em class="fa fa-navicon">&nbsp;</em> Deals <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-7">
					<li><a class="" href="deals.do">
						<span class="fa fa-arrow-right">&nbsp;</span> Add deals
					</a></li>
					<li><a class="" href="listofdeals.do">
						<span class="fa fa-arrow-right">&nbsp;</span> List of deals
					</a></li>
					
				</ul>
			</li>

				<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
					<em class="fa fa-navicon">&nbsp;</em> Users <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
					</a>
					<ul class="children collapse" id="sub-item-1">
						<li><a class="" href="PlumbandElec.html">
							<span class="fa fa-arrow-right">&nbsp;</span> Plumber + Electrician
						</a></li>
						<li><a class="" href="#">
							<span class="fa fa-arrow-right">&nbsp;</span> Lift Repair
						</a></li>
						<li><a class="" href="#">
							<span class="fa fa-arrow-right">&nbsp;</span> House Keeping
						</a></li>
	                    	<li><a class="" href="#">
							<span class="fa fa-arrow-right">&nbsp;</span> Pest Control
						</a></li>
	                    	<li><a class="" href="#">
							<span class="fa fa-arrow-right">&nbsp;</span> Propert Management
						</a></li>
	                    	<li><a class="" href="#">
							<span class="fa fa-arrow-right">&nbsp;</span> Security
						</a></li>
					</ul>
				</li>


						<li class="parent "><a data-toggle="collapse" href="#sub-item-6">
				<em class="fa fa-navicon">&nbsp;</em> User Management <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-6">
					<li><a class="" href="viewbackendusers.do">
						<span class="fa fa-arrow-right">&nbsp;</span> Users
					</a></li>
					<li><a class="" href="viewroles.do">
						<span class="fa fa-arrow-right">&nbsp;</span> Roles
					</a></li>
					
				</ul>
			</li>
			
            <li><a href="charts.do"><em class="fa fa-bar-chart">&nbsp;</em>Payment</a></li>
			<li><a href="elements.do"><em class="fa fa-toggle-off">&nbsp;</em> Manage Permission</a></li>
            <li><a href="panels.do"><em class="fa fa-clone">&nbsp;</em>Approval</a></li>
            <li><a href="panels.do"><em class="fa fa-clone">&nbsp;</em>Employee Performance</a></li>
            <li class="parent "><a data-toggle="collapse" href="#sub-item-60">
				<em class="fa fa-navicon">&nbsp;</em> Module Management <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-60">
					<li><a class="" href="viewmodules.do">
						<span class="fa fa-arrow-right">&nbsp;</span> Modules
					</a></li>
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Submodules
					</a></li>
					
				</ul>
			</li>
			<li><a href="adminlogin.do"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->


		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->

		
		<div class="panel panel-container">
			<div class="row">
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-teal panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-shopping-cart color-blue"></em>
							<div class="large">120</div>
							<div class="text-muted">ELectrician + Plumbing</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-blue panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-comments color-orange"></em>
							<div class="large">52</div>
							<div class="text-muted">Lift Repair</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-orange panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-users color-teal"></em>
							<div class="large">24</div>
							<div class="text-muted">House Keeping</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-red panel-widget ">
						<div class="row no-padding"><em class="fa fa-xl fa-search color-red"></em>
							<div class="large">25.2k</div>
							<div class="text-muted">Property Managment</div>
						</div>
					</div>
				</div>

						<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-red panel-widget ">
						<div class="row no-padding"><em class="fa fa-xl fa-search color-red"></em>
							<div class="large">25.2k</div>
							<div class="text-muted">Pest Control</div>
						</div>
					</div>
				</div>

						<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-red panel-widget ">
						<div class="row no-padding"><em class="fa fa-xl fa-search color-red"></em>
							<div class="large">25.2k</div>
							<div class="text-muted">Security</div>
						</div>
					</div>
				</div>
                

			</div><!--/.row-->

			<div class = "row">
							<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-red panel-widget ">
						<div class="row no-padding"><em class="fa fa-xl fa-search color-red"></em>
							<div class="large">25.2k</div>
							<div class="text-muted">Enquiries</div>
						</div>
					</div>
				</div>


						<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-red panel-widget ">
						<div class="row no-padding"><em class="fa fa-xl fa-search color-red"></em>
							<div class="large">25.2k</div>
							<div class="text-muted">Leads</div>
						</div>
					</div>
				</div>

						<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-red panel-widget ">
						<div class="row no-padding"><em class="fa fa-xl fa-search color-red"></em>
							<div class="large">25.2k</div>
							<div class="text-muted">Deals</div>
						</div>
					</div>
				</div>

						<div class="col-xs-6 col-md-3 col-lg-2 no-padding">
					<div class="panel panel-red panel-widget ">
						<div class="row no-padding"><em class="fa fa-xl fa-search color-red"></em>
							<div class="large">25.2k</div>
							<div class="text-muted">Register Calls</div>
						</div>
					</div>
				</div>

			</div>
			<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Site Traffic Overview
						<ul class="pull-right panel-settings panel-button-tab-right">
							<li class="dropdown"><a class="pull-right dropdown-toggle" data-toggle="dropdown" href="#">
								<em class="fa fa-cogs"></em>
							</a>
								<ul class="dropdown-menu dropdown-menu-right">
									<li>
										<ul class="dropdown-settings">
											<li><a href="#">
												<em class="fa fa-cog"></em> Settings 1
											</a></li>
											<li class="divider"></li>
											<li><a href="#">
												<em class="fa fa-cog"></em> Settings 2
											</a></li>
											<li class="divider"></li>
											<li><a href="#">
												<em class="fa fa-cog"></em> Settings 3
											</a></li>
										</ul>
									</li>
								</ul>
							</li>
						</ul>
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<div class="canvas-wrapper">
							<canvas class="main-chart" id="line-chart" height="200" width="600"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div><!--/.row-->



            </div>
			

		
	
	</div>	<!--/.main-->

	
	<script src="amcDesign/adminweb/js/jquery-1.11.1.min.js"></script>
	<script src="amcDesign/adminweb/js/bootstrap.min.js"></script>
	<script src="amcDesign/adminweb/js/chart.min.js"></script>
	<script src="amcDesign/adminweb/js/chart-data.js"></script>
	<script src="amcDesign/adminweb/js/easypiechart.js"></script>
	<script src="amcDesign/adminweb/js/easypiechart-data.js"></script>
	<script src="amcDesign/adminweb/js/bootstrap-datepicker.js"></script>
	<script src="amcDesign/adminweb/js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>
		
</body>
</html>
