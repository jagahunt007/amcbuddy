<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.amc">
    <tiles:putAttribute name="body">


<div class="container">
  
  	<h2 style="text-align:center">Roles adding page</h2>
  	<form  action="savesubmodule.do" method="get" modelattribute="sli">
        <div class = "col-md-3"></div>
        <div class = "col-md-6">
            <div class="well">
            <input type="hidden" name="module_id" value="${sli.section_id}"/>
  <div class="form-group">
    <label for="module_id">Select Module Name:</label>
    <select name="module_id" class="form-control">
      <option value="select">select</option>
      <c:forEach  var="modules" items="${mod1}">
      <option value="${modules.module_id}">${modules.modulename}</option>
      </c:forEach>
    </select>
   </div>
   <div class="form-group">
    <label for="sectionname">Enter SubModuleName:</label>
    <input type="text" class="form-control" name="sectionname"  value="${sli.sectionname}"/>
  </div>
  <div class="form-group">
    <label for="url">Enter url:</label>
    <input type="text" class="form-control" name="url"  value="${sli.url}"/>
   </div>
   
   
  <button type="submit" class="btn btn-default">Save</button>
        </div>
            </form>
        
  </div>
</div>

</tiles:putAttribute>
</tiles:insertDefinition>





