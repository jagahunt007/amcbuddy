<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.amc">
    <tiles:putAttribute name="body">

<div class = "col-lg-4"></div>

<div class="container">
<div class = "col-lg-6">
  <h2>Categories</h2>
<div class="table-responsive">          
  <table class="table">
<thead>
<tr>
<th>Category Id</th>
<th>Category Name</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<c:forEach  var="categories" items="${catlist}">
<tr>
<td>${categories.categoryid}</td>
<td>${categories.categoryname}</td>
<td><a href="editcategory.do?catid=${categories.categoryid}"><button type="button">Edit</button></a>            <a href="deletecategory.do?catid=${categories.categoryid}"><button type="button">Delete</button></a></td>
</tr>
</c:forEach>
</tbody>
</table>
</div>
  <a href = "addcategory.do"><button type="button" class="btn btn-save">addcategory</button></a>
  </div>
</div>



</tiles:putAttribute>
</tiles:insertDefinition>