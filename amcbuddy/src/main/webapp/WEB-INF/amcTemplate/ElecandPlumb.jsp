<!DOCTYPE html>
<html lang="en">
<head>
<title>AMC : Annual Maintance Contract</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Innovate Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="amcDesign/styles/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="amcDesign/styles/font-awesome.css" rel="stylesheet"> 
<link rel="stylesheet" href="amcDesign/styles/Footer-with-map.css">
<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
<script type='text/javascript' src='amcDesign/script/jquery-2.2.3.min.js'></script>
<script type='text/javascript' src='amcDesign/script/bootstrap.js'></script>
<style>
body, html{
   /*  height: 100%;
 	background-repeat: no-repeat;
 	background-color: #d3d3d3;*/
 	font-family: 'Oxygen', sans-serif;
}/**/
.navbar-default {
    background-color: #146eb4;
    border-color: #146eb4;
}
.logo
{
	color:white !important;
	font-size: 36px;
}
.navbar-default .navbar-nav > li > a
{
	color: white;
	line-height: 25px;
    font-size: 18px;
    letter-spacing: 0.5px;
    padding: 0 15px;
    margin: 10px;
}
.quoat
{
	font-size: 18px;
    color: #787878;
    line-height: 30px;
    padding: 10px 70px 0px 0;
}
.cont
{
	    padding: 12px 0px;
    text-align: left;
    font-size: 20px;
    color: #787878;
}

	   

.main{
 	margin:15px 15px;
}

label{
	margin-bottom: 15px;
}

/*input,
input::-webkit-input-placeholder {
    font-size: 11px;
    padding-top: 3px;
}*/

.main-login{
 	background-color: #fff;
    /* shadows and rounded borders */
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);

}
.form-control {
    height: auto!important;
padding: 8px 12px !important;
}


.main-center{
 	
    padding: 10px 40px;
	
box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);

}
span.input-group-addon i {
    color: #009edf;
    font-size: 17px;
}

.login-button{
	margin-top: 5px;
}

.login-register{
	font-size: 11px;
	text-align: center;
}
.nav-tabs { border-bottom: 2px solid #DDD; }
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
    .nav-tabs > li > a { border: none; color: #666; }
        .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #4285F4 !important; background: transparent; }
        .nav-tabs > li > a::after { content: ""; background: #4285F4; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
    .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
.tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
.tab-pane { padding: 15px 0; }
.tab-content{padding:20px}

.panel
{
    text-align: center;
}
.panel:hover { box-shadow: 0 1px 5px rgba(0, 0, 0, 0.4), 0 1px 5px rgba(130, 130, 130, 0.35); }
.panel-body
{
    padding: 0px;
    text-align: center;
}

.the-price
{
    background-color: rgba(220,220,220,.17);
    box-shadow: 0 1px 0 #dcdcdc, inset 0 1px 0 #fff;
    padding: 20px;
    margin: 0;
}

.the-price h1
{
    line-height: 1em;
    padding: 0;
    margin: 0;
}

.subscript
{
    font-size: 25px;
}

/* CSS-only ribbon styles    */
.cnrflash
{
    /*Position correctly within container*/
    position: absolute;
    top: -9px;
    right: 4px;
    z-index: 1; /*Set overflow to hidden, to mask inner square*/
    overflow: hidden; /*Set size and add subtle rounding  		to soften edges*/
    width: 100px;
    height: 100px;
    border-radius: 3px 5px 3px 0;
}
.cnrflash-inner
{
    /*Set position, make larger then 			container and rotate 45 degrees*/
    position: absolute;
    bottom: 0;
    right: 0;
    width: 145px;
    height: 145px;
    -ms-transform: rotate(45deg); /* IE 9 */
    -o-transform: rotate(45deg); /* Opera */
    -moz-transform: rotate(45deg); /* Firefox */
    -webkit-transform: rotate(45deg); /* Safari and Chrome */
    -webkit-transform-origin: 100% 100%; /*Purely decorative effects to add texture and stuff*/ /* Safari and Chrome */
    -ms-transform-origin: 100% 100%;  /* IE 9 */
    -o-transform-origin: 100% 100%; /* Opera */
    -moz-transform-origin: 100% 100%; /* Firefox */
    background-image: linear-gradient(90deg, transparent 50%, rgba(255,255,255,.1) 50%), linear-gradient(0deg, transparent 0%, rgba(1,1,1,.2) 50%);
    background-size: 4px,auto, auto,auto;
    background-color: #aa0101;
    box-shadow: 0 3px 3px 0 rgba(1,1,1,.5), 0 1px 0 0 rgba(1,1,1,.5), inset 0 -1px 8px 0 rgba(255,255,255,.3), inset 0 -1px 0 0 rgba(255,255,255,.2);
}
.cnrflash-inner:before, .cnrflash-inner:after
{
    /*Use the border triangle trick to make  				it look like the ribbon wraps round it's 				container*/
    content: " ";
    display: block;
    position: absolute;
    bottom: -16px;
    width: 0;
    height: 0;
    border: 8px solid #800000;
}
.cnrflash-inner:before
{
    left: 1px;
    border-bottom-color: transparent;
    border-right-color: transparent;
}
.cnrflash-inner:after
{
    right: 0;
    border-bottom-color: transparent;
    border-left-color: transparent;
}
.cnrflash-label
{
    /*Make the label look nice*/
    position: absolute;
    bottom: 0;
    left: 0;
    display: block;
    width: 100%;
    padding-bottom: 5px;
    color: #fff;
    text-shadow: 0 1px 1px rgba(1,1,1,.8);
    font-size: 0.95em;
    font-weight: bold;
    text-align: center;
}

</style>
<body>


   <script type="text/javascript"> 
$( document ).ready(function() {
		
	/*  alert("hi naag"); */
	 
	 var ageaddon= 0;
	 var flataddon = 0;
	 var contractaddon = 0;
	 var totalamnt = 0;
	 var discount = 0;
	 var contractType = 0;
	 var flatno=0;
	 
	 var buffer=0;
	
	
	//this is property age  validation 
	 
	 
	 $( "#propAge" ).change(function() {
			
		                      
		   var  proage =$("#propAge").val(); 
		    	
	
/* 		 alert("property age.............."+proage); */
		 
		 if(proage > 5){
			 ageaddon =( proage-5)*1;
			  ageaddon = (100+ageaddon)*flatno; 
			  
			
			 
			 $('#displayValue').html(ageaddon+"/Month");
			 
			
		 }
		 else{
			
			  ageaddon = (100+ageaddon)*flatno; 
			  $('#displayValue').html(ageaddon+"/Month");
			  
		 }
		 
		});
	
	//this is no of flats validation 
	 $( "#nooflats" ).change(function() {
		
		 $('#displayValue').val(); 
		    var noflats =$("#nooflats").val(); 
		    flatno =noflats;
		    buffer = noflats*100;
		    $('#displayValue').html(buffer+"/Month");
		    
		    
		  
		
		 
		});
	
	 
	 
	 
	 $( "#contractType" ).change(function() {
			
		
		    var cType =$("#contractType").val(); 
		    contractType = cType;
		    
		    
		    if(cType==3)
		    	{
		    /* 	contractaddon= (3*12)*flataddon; */
		    	contractaddon= (3*12)*ageaddon;
		    	
		    	
		    	
		    	 $('#displayValue').html(contractaddon+"/3years");
		    	
		    	/*  alert("contract type.........."+contractaddon); */
		    	 
		    	
		    	} 
		    else{
		    	contractaddon= (1*12)*ageaddon;
		    	
		    	 $('#displayValue').html(contractaddon+"/1years");
		    /* 	alert("contract type.........."+contractaddon); */
		    	
		    }
		    
		    
		   
		 
		});
	
	 
	 $( "#schedultyp" ).change(function() {
			
			
		    var sType =$("#schedultyp").val(); 
		    
		    
		  if(contractType == 3){
			  
			/*   alert("3years......."); */
			  
			  
			    if(sType==3)
			    	{
			    	
			    	discount=contractaddon*(10/100);
			    	totalamnt = contractaddon -discount;
			    	
			    	 $('#displayValue').html(totalamnt+"/3years");
			    	/*  alert("schedual type.........."+totalamnt); */
			    	 
			    	
			    	} 
			 
			    if(sType==1)
		    	{
		    	
		    	discount=contractaddon/3;
		    	totalamnt = discount;
		    	 $('#displayValue').html(totalamnt+"/1years");
		    	
		    /* 	 alert("2nd condition schedual type.........."+totalamnt); */
		    	
		    	
		    	} 
		  
			    if(sType==0.6)
		    	{
		    	
		    	discount=(contractaddon/6);
		    	var gstAmnt = (18/100)*discount;
		    	 $('#displayValue').html(discount);
		    	
		    	/* alert("half year..............."+discount); */
		    	 $('#displayValue').html(gstAmnt);
		    	
		    	/*  alert("GST Amount.........."+gstAmnt); */
		    	 
		    	 totalamnt = discount+gstAmnt;
		    	 $('#displayValue').html(totalamnt+"/half yearly"); 
		    	/*  alert("total Amount.........."+totalamnt); */
		    	
		    	} 
			  
		  } 
		  else{
			  if(sType==1)
		    	{
		    	
		    	
		    	totalamnt = contractaddon;
		    	
		    	 $('#displayValue').html(totalamnt+"/1years"); 
		    	/*  alert("one year contract schedual type.........."+totalamnt); */
		    	
		    	
		    	} 
		  
			    if(sType==0.6)
		    	{
		    	
		    	discount=(contractaddon/2);
		    	var gstAmnt = (18/100)*discount;
		    	
		    	$('#displayValue').html(discount); 
		    	/* alert(" one year contract schedual....................half year..............."+discount); */
		    	$('#displayValue').html(gstAmnt); 
		    	
		    	/*  alert("GST Amount.........."+gstAmnt); */
		    	 
		    	 totalamnt = discount+gstAmnt;
		    	 $('#displayValue').html(totalamnt+"/half yearly");
		    	 
		    	/*  alert("total Amount.........."+totalamnt); */
		    	
		    	}
		  }
		  
		  
		  
		  
		    
		    
		   
		 
		});
	 
	 
	 
	
	
});
</script> 
    
 

<script>
$(document).ready(function(){
 var  flatno=0;
 var age =0;
 var flatage=0
var contactAmt =0;
var ctype =0;
var noyrs =0;

$("#flatno_external").change(function(){
        flatno = parseInt($(this).val());
        $("#Ex_Value").html(flatno+"/monthly");
      
    });
    
    $("#Ex_propAge").change(function(){
        
        if($(this).val()>5){
        age = $(this).val()-5;
         
       flatage=  flatno+age;
       $("#Ex_Value").html(flatage+"/monthly");
       
        }
        else{
        	 flatage=  flatno+age;
             $("#Ex_Value").html(flatage+"/monthly");
        }
     
    });
    
     $("#contractype_external").change(function(){
    	 noyrs = $(this).val();
        
        if($(this).val() == 3){
       
        
      ctype=  flatage*3*12;
      /*  alert("success"+ctype); */
       $("#Ex_Value").html(ctype+"/years");
        }else{
        ctype = flatage*12;
       /*  alert("success"+ctype); */
        $("#Ex_Value").html(ctype+"/year");
        
        }
        
     
    });
    
    
    $("#paymentPlan").change(function(){
       
       if(noyrs ==3){
    	   
    	   
    	   if($(this).val() == 3){
    	       
    	        
    		      ctype=  flatage*3*12;
    		      /*  alert("success"+ctype); */
    		       $("#Ex_Value").html(ctype+"/years");
    		        }
    		        if($(this).val() == 1){
    		        ctype = flatage*12;
    		       /*  alert("success"+ctype); */
    		        $("#Ex_Value").html(ctype+"/year");
    		        
    		        }
    		        if($(this).val() == 0.6){
    		        ctype = (flatage*6)+600;
    		       /*  alert("success"+ctype); */
    		        $("#Ex_Value").html(ctype+"/year");
    		        
    		        }
  	   
       } 
if(noyrs ==1){
    	   
	
		   $('#paymentPlan option[value="3"]').hidden = true 
		   
		   
    		        if($(this).val() == 1){
    		        ctype = flatage*12;
    		       /*  alert("success"+ctype); */
    		        $("#Ex_Value").html(ctype+"/year");
    		        
    		        }
    		        if($(this).val() == 0.6){
    		        ctype = (flatage*6)+600;
    		        /* alert("success"+ctype); */
    		        $("#Ex_Value").html(ctype+"/year");
    		        
    		        }
  	   
       } 
        
     
    });
    
    


   
});
</script>

  
		<script>
$(document).ready(function(){
 var  flatnoo=0;
 var agee =0;
 var flatagee=0
var contactAmt =0;
var ctypee =0;
var nyrs1 =0;

$("#flattyp_internal").change(function(){
        flatnoo = parseInt($(this).val());
        $("#In_Value").html(flatnoo+"/month");
      
    });
    
    
    
    
    
    
    $("#Ix_propAge").change(function(){
        
        if($(this).val()>5){
        agee = $(this).val()-5;
         
       flatagee=  flatnoo+agee;
       $("#In_Value").html(flatagee+"/month");
      
        
        }
        else{
        	flatagee=  flatnoo+agee;
            $("#In_Value").html(flatagee+"/month");
        }
     
    });
    
     $("#contracttype_internal").change(function(){
    	 nyrs1 =$(this).val();
        
        if($(this).val() == 3){
       
        	
      ctypee=  flatagee*3*12;
       
       $("#In_Value").html(ctypee+"/year");
        }else{
        	
        ctypee = flatagee*12;
       
        $("#In_Value").html(ctypee+"/year");
        
        }
        
     
    });
    
    

     $("#ipaymentPlan").change(function(){
        
    	 if(nyrs1 ==3){     
         if($(this).val() == 3){
        
         
       ctypee=  flatagee*3*12;
        
        $("#In_Value").html(ctypee+"/3years");
         }
         if($(this).val() == 1){
         ctypee = flatagee*12;
       
         $("#In_Value").html(ctypee+"/1year");
         
         }
         if($(this).val() == 0.6){
         ctypee = (flatagee*6)+60;
         
         $("#In_Value").html(ctypee+"/half year");
         
         }
    	 } 
    	 if(nyrs1 ==1){  
    		 
    		 
    		 
    		 if($(this).val() == 1){
    	         ctypee = flatagee*12;
    	       
    	         $("#In_Value").html(ctypee+"/year");
    	         
    	         }
    	         if($(this).val() == 0.6){
    	         ctypee = (flatagee*6)+60;
    	         
    	         $("#In_Value").html(ctypee+"/half year");
    	         
    	         }	 
    		 
    		 
    		 
    	 }
    	 
    	 
    	 
    	 
    	 
    	 
      
     });
     
     


   
      
});
</script> 
    
    
    <script>
$(document).ready(function(){
	
	
	
 var  flatnooo=0;
 var ageee =0;
 var flatageee=0
var contactAmt =0;
var ctypeee =0;
var nyrs2 =0;

$("#sfttpy").change(function(){
	flatnooo = parseInt($(this).val());
        $("#ivalue").html(flatnooo+"/month");
      
    });
    
    
    
$("#inproperty").change(function(){
    
    if($(this).val()>5){
    	ageee = $(this).val()-5;
     
    	flatageee=  flatnooo+ageee;
   $("#ivalue").html(flatageee+"/month");
  
    
    }
    else{
    	flatageee=  flatnooo+ageee;
    	   $("#ivalue").html(flatageee+"/month");
    }
 
});


$("#contype").change(function(){
	 nyrs2 =$(this).val();
   
   if($(this).val() == 3){
  
   	
	   ctypeee=  flatageee*3*12;
  
  $("#ivalue").html(ctypeee+"/year");
   }else{
   	
	   ctypeee = flatageee*12;
  
   $("#ivalue").html(ctypeee+"/year");
   
   }
   

});

$("#spayment").change(function(){
    
	 if(nyrs2 ==3){     
    if($(this).val() == 3){
   
    
    	ctypeee=  flatageee*3*12;
   
   $("#ivalue").html(ctypeee+"/3years");
    }
    if($(this).val() == 1){
    	ctypeee = flatageee*12;
  
    $("#ivalue").html(ctypeee+"/1year");
    
    }
    if($(this).val() == 0.6){
    	ctypeee = (flatageee*6)+60;
    
    $("#ivalue").html(ctypeee+"/half year");
    
    }
	 } 
	 if(nyrs2 ==1){  
		 
		 
		 
		 if($(this).val() == 1){
			 ctypeee = flatageee*12;
	       
	         $("#ivalue").html(ctypee+"/year");
	         
	         }
	         if($(this).val() == 0.6){
	        	 ctypeee = (flatageee*6)+60;
	         
	         $("#ivalue").html(ctypeee+"/half year");
	         
	         }	 
		 
		 
		 
	 }
	
});

    
});
    </script> 
    
    
    <script>
    $(document).ready(function(){
    	
    	 var gageaddon=0;
    	 var gflatno=0;
    	 var buffer=0;
    	 var gcontractType=0;
    	var gcontractaddon=0;
    	var discount=0;
    	var totalamnt=0;
    	
    	$( "#gpropAge" ).change(function() {
			
            
 		   var  proage =$("#gpropAge").val(); 
 		    	
 	
 /* 		 alert("property age.............."+proage); */
 		 
 		 if(proage > 5){
 			 gageaddon =( proage-5)*1;
 			gageaddon = (100+gageaddon)*gflatno; 
 			  
 			
 			 
 			 $('#gvalue').html(gageaddon+"/Month");
 			 
 			
 		 }
 		 else{
 			gageaddon = (100+gageaddon)*gflatno; 
			  
 			
			 
			 $('#gvalue').html(gageaddon+"/Month");
			 
 		 }
 		
 		 
 		});
 	
    	
    	
    	$("#gnooflats").change(function(){
    		/* alert(" gnooflats..................."); */
    		 $('#gvalue').val(); 
 		    var noflats =$("#gnooflats").val(); 
 		   gflatno =noflats;
 		    buffer = noflats*100;
 		    $('#gvalue').html(buffer+"/Month");
    		
    		
    	});
    	
    	 $( "#gcontractType" ).change(function() {
 			
    			
 		    var cType =$("#gcontractType").val(); 
 		    gcontractType = cType;
 		    
 		    
 		    if(cType==3)
 		    	{
 		    /* 	contractaddon= (3*12)*flataddon; */
 		    	gcontractaddon= (3*12)*gageaddon;
 		    	
 		    	
 		    	
 		    	 $('#gvalue').html(gcontractaddon+"/3years");
 		    	
 		    	/*  alert("contract type.........."+contractaddon); */
 		    	 
 		    	
 		    	} 
 		    else{
 		    	gcontractaddon= (1*12)*gageaddon;
 		    	
 		    	 $('#gvalue').html(gcontractaddon+"/1years");
 		    /* 	alert("contract type.........."+contractaddon); */
 		    	
 		    }
 		    
 		    
 		   
 		 
 		});
    	 
    	 $( "#gschedultyp" ).change(function() {
 			
 			
 		    var sType =$("#gschedultyp").val(); 
 		    
 		    
 		  if(gcontractType == 3){
 			  
 			/*   alert("3years......."); */
 			  
 			  
 			    if(sType==3)
 			    	{
 			    	
 			    	discount=gcontractaddon*(10/100);
 			    	totalamnt = gcontractaddon -discount;
 			    	
 			    	 $('#gvalue').html(totalamnt+"/3years");
 			    	/*  alert("schedual type.........."+totalamnt); */
 			    	 
 			    	
 			    	} 
 			 
 			    if(sType==1)
 		    	{
 		    	
 		    	discount=gcontractaddon/3;
 		    	totalamnt = discount;
 		    	 $('#gvalue').html(totalamnt+"/1years");
 		    	
 		    /* 	 alert("2nd condition schedual type.........."+totalamnt); */
 		    	
 		    	
 		    	} 
 		  
 			    if(sType==0.6)
 		    	{
 		    	
 		    	discount=(gcontractaddon/6);
 		    	var gstAmnt = (18/100)*discount;
 		    	 $('#gvalue').html(discount);
 		    	
 		    	/* alert("half year..............."+discount); */
 		    	 $('#gvalue').html(gstAmnt);
 		    	
 		    	/*  alert("GST Amount.........."+gstAmnt); */
 		    	 
 		    	 totalamnt = discount+gstAmnt;
 		    	 $('#gvalue').html(totalamnt+"/half yearly"); 
 		    	/*  alert("total Amount.........."+totalamnt); */
 		    	
 		    	} 
 			  
 		  } 
 		  else{
 			  if(sType==1)
 		    	{
 		    	
 		    	
 		    	totalamnt = gcontractaddon;
 		    	
 		    	 $('#gvalue').html(totalamnt+"/1years"); 
 		    	/*  alert("one year contract schedual type.........."+totalamnt); */
 		    	
 		    	
 		    	} 
 		  
 			    if(sType==0.6)
 		    	{
 		    	
 		    	discount=(gcontractaddon/2);
 		    	var gstAmnt = (18/100)*discount;
 		    	
 		    	$('#gvalue').html(discount); 
 		    	/* alert(" one year contract schedual....................half year..............."+discount); */
 		    	$('#gvalue').html(gstAmnt); 
 		    	
 		    	/*  alert("GST Amount.........."+gstAmnt); */
 		    	 
 		    	 totalamnt = discount+gstAmnt;
 		    	 $('#gvalue').html(totalamnt+"/half yearly");
 		    	 
 		    	/*  alert("total Amount.........."+totalamnt); */
 		    	
 		    	}
 		  }
 		  
 		  
 		  
 		  
 		    
 		    
 		   
 		 
 		});
 	 
    	
    	
    	
    });
   
    </script> 
    
  <script>
$(document).ready(function(){
 var  gflatno=0;
 var gage =0;
 var gflatage=0
var contactAmt =0;
var gctype =0;
var gnoyrs =0;

$("#gflatno_external").change(function(){
	gflatno = parseInt($(this).val());
        $("#Egvalue").html(gflatno+"/monthly");
      
    });
    
    $("#gEx_propAge").change(function(){
        
        if($(this).val()>5){
        	gage = $(this).val()-5;
         
        gflatage=  gflatno+gage;
       $("#Egvalue").html(gflatage+"/monthly");
       
        }
        else{
        	 gflatage=  gflatno+gage;
             $("#Egvalue").html(gflatage+"/monthly");
        }
     
    });
    
     $("#gcontractype_external").change(function(){
    	 gnoyrs = $(this).val();
        
        if($(this).val() == 3){
       
        
      gctype=  gflatage*3*12;
      /*  alert("success"+ctype); */
       $("#Egvalue").html(gctype+"/years");
        }else{
        	gctype = gflatage*12;
       /*  alert("success"+ctype); */
        $("#Egvalue").html(gctype+"/year");
        
        }
        
     
    });
    
    
    $("#gpaymentPlan").change(function(){
       
       if(gnoyrs ==3){
    	   
    	   
    	   if($(this).val() == 3){
    	       
    	        
    		   gctype=  gflatage*3*12;
    		      /*  alert("success"+ctype); */
    		       $("#Egvalue").html(gctype+"/years");
    		        }
    		        if($(this).val() == 1){
    		        	gctype = gflatage*12;
    		       /*  alert("success"+ctype); */
    		        $("#Egvalue").html(gctype+"/year");
    		        
    		        }
    		        if($(this).val() == 0.6){
    		        	gctype = (gflatage*6)+600;
    		       /*  alert("success"+ctype); */
    		        $("#Egvalue").html(gctype+"/year");
    		        
    		        }
  	   
       } 
if(gnoyrs ==1){
    	   
	
		   $('#gpaymentPlan option[value="3"]').hidden = true 
		   
		   
    		        if($(this).val() == 1){
    		        	gctype = gflatage*12;
    		       /*  alert("success"+ctype); */
    		        $("#Egvalue").html(gctype+"/year");
    		        
    		        }
    		        if($(this).val() == 0.6){
    		        	gctype = (gflatage*6)+600;
    		        /* alert("success"+ctype); */
    		        $("#Egvalue").html(gctype+"/year");
    		        
    		        }
  	   
       } 
        
     
    });
    
    


   
});
</script>

<script>
$(document).ready(function(){
 var  gflatnoo=0;
 var gagee =0;
 var gflatagee=0
var contactAmt =0;
var gctypee =0;
var nyrs2 =0;

$("#gflattyp_internal").change(function(){
	gflatnoo = parseInt($(this).val());
        $("#igvalue").html(gflatnoo+"/month");
      
    });
    
    
    
    
    
    
    $("#gIx_propAge").change(function(){
        
        if($(this).val()>5){
        	gagee = $(this).val()-5;
         
        	gflatagee=  gflatnoo+gagee;
       $("#igvalue").html(gflatagee+"/month");
      
        
        }
        else{
        	gflatagee=  gflatnoo+gagee;
            $("#igvalue").html(gflatagee+"/month");
        }
     
    });
    
     $("#gcontracttype_internal").change(function(){
    	 nyrs2 =$(this).val();
        
        if($(this).val() == 3){
       
        	
        	gctypee=  gflatagee*3*12;
       
       $("#igvalue").html(gctypee+"/year");
        }else{
        	
        	gctypee = gflatagee*12;
       
        $("#igvalue").html(gctypee+"/year");
        
        }
        
     
    });
    
    

     $("#gipaymentPlan").change(function(){
        
    	 if(nyrs2 ==3){     
         if($(this).val() == 3){
        
         
        	 gctypee=  gflatagee*3*12;
        
        $("#igvalue").html(gctypee+"/3years");
         }
         if($(this).val() == 1){
        	 gctypee = gflatagee*12;
       
         $("#igvalue").html(gctypee+"/1year");
         
         }
         if($(this).val() == 0.6){
        	 gctypee = (gflatagee*6)+60;
         
         $("#igvalue").html(gctypee+"/half year");
         
         }
    	 } 
    	 if(nyrs2 ==1){  
    		 
    		 
    		 
    		 if($(this).val() == 1){
    			 gctypee = gflatagee*12;
    	       
    	         $("#igvalue").html(gctypee+"/year");
    	         
    	         }
    	         if($(this).val() == 0.6){
    	        	 gctypee = (gflatagee*6)+60;
    	         
    	         $("#igvalue").html(gctypee+"/half year");
    	         
    	         }	 
    		 
    		 
    		 
    	 }
    	 
    	 
    	 
    	 
    	 
    	 
      
     });
     
     


   
      
});
</script> 
 
<nav class="navbar navbar-default" role="navigation">
      <div class="container ">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand logo" href="#" >AMC</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
      
      <ul class="nav navbar-nav navbar-right">
          <li class=""><a href="#">Home</a></li>
        <li><a href="#">About Us</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services</a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
        <li><a href="#">Portfolio</a></li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Login</a>
   
        </li>
        
        
      </ul>
    </div><!-- /.navbar-collapse -->
      </div><!-- /.container-collapse -->
  </nav>

<div class="container">

 

	<div class="row">
		                                <div class="col-md-9">
                                    <!-- Nav tabs --><div class="card">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#Apartment" aria-controls="home" role="tab" data-toggle="tab" style="font-size: 20px;padding: 20px;">Apartment</a></li>
                                        <li role="presentation"><a href="#Individual" aria-controls="profile" role="tab" data-toggle="tab" style="font-size: 20px;padding: 20px;">Individual</a></li>
                                        <li role="presentation"><a href="#gated" aria-controls="profile" role="tab" data-toggle="tab" style="font-size: 20px;padding: 20px;">Gated Community</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="Apartment">
                                        					<div class="container">
    <div class="row">

         <div class="col-xs-12 col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Industrial+Community</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1 id="displayValue">
                            Rs0.00<span class="subscript">/month</span></h1>
                   
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                              Number of flats :<input type = "text" name = "nooflats" id="nooflats" size="7">
                            </td>
                        </tr>
                        <tr>
                            <td>
                               Age of Property :<input type = "text" name = "propAge" id = "propAge"  size="7">
                            </td>
                        </tr>
                        <tr>
                            <td>
                              Contract Type: <select id="contractType">
                                                     <option value="0">Choose</option>
                              
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  
											</select>
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                               Schedule of Payment: <select id="schedultyp">
                                 <option value="0">choose</option>
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  <option value="0.6">1/2 yearly</option>
												 
											</select>
                            </td>
                        </tr>
                <!--         <tr class="active">
                            <td>
                                100MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr> -->
                    </table>
                </div>
                <div class="panel-footer">
                     <a href="" class="btn btn-success" role="button" data-toggle="modal" data-target="#myModal">Proceed</a>
                   </div>
            </div>
        </div>
         <div class="col-xs-12 col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Industrial+Community</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1 id="Ex_Value">
                            Rs0.00<span class="subscript">/month</span></h1>
                    
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                              Number of flats: <select  id="flatno_external">
                                                  <option value="0">choose</option>
												  <option value="500">1-10</option>
												  <option value="750">10-20</option>
												  <option value="1000">20-30</option>
												  <option value="1250">30-40</option>
												  <option value="1500">40-50</option>
												
												  
											</select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               Age of Property :<input type = "text" name = "Ex_propAge" id = "Ex_propAge"  size="7">
                            </td>
                        </tr>
                        <tr>
                            <td>
                              Contract Type: <select id="contractype_external">
                                                     <option value="0">Choose</option>
                              
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  
											</select>
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                               Schedule of Payment: <select id="paymentPlan">
                                 <option value="0">choose</option>
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  <option value="0.6">1/2 yearly</option>
												 
											</select>
                            </td>
                        </tr>
                <!--         <tr class="active">
                            <td>
                                100MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr> -->
                    </table>
                </div>
                <div class="panel-footer">
                      <a href="" class="btn btn-success" role="button" data-toggle="modal" data-target="#myModal">Proceed</a>
                   </div>
            </div>
        </div>
        
         <div class="col-xs-12 col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Industrial+Community</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1 id="In_Value">
                            Rs0.00<span class="subscript">/month</span></h1>
                    
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                             Type of flat: <select id="flattyp_internal">
												 
                                                  <option value="0">choose</option>
												  <option value="50">1BHK</option>
												  <option value="75">2BHK</option>
												  <option value="100">3BHK</option>
												  <option value="125">4BHK</option>
											
												  
											</select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               Age of Property :<input type = "text" name = "Ix_propAge" id = "Ix_propAge"  size="7">
                            </td>
                        </tr>
                        <tr>
                            <td>
                              Contract Type: <select id="contracttype_internal">
                                                     <option value="0">Choose</option>
                              
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  
											</select>
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                               Schedule of Payment: <select id="ipaymentPlan">
                                 <option value="0">choose</option>
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  <option value="0.6">1/2 yearly</option>
												 
											</select>
                            </td>
                        </tr>
                <!--         <tr class="active">
                            <td>
                                100MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr> -->
                    </table>
                </div>
                <div class="panel-footer">
                       <a href="" class="btn btn-success" role="button" data-toggle="modal" data-target="#myModal">Proceed</a>
                   </div>
            </div>
        </div>



    </div>
</div>



                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="Individual">
                                        		<div class="container">
    <div class="row">

         <div class="col-xs-12 col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Bronze</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1 id="ivalue">
                            Rs0.00<span class="subscript">/month</span></h1>
                        <small></small>
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                              Total sft :<select id="sfttpy">
												 
                                                  <option value="0">choose</option>
												  <option value="100">1000sft</option>
												  <option value="125">1000-1500</option>
												  <option value="150">1500-2000</option>
												  <option value="200">2000-3000</option>
											
												  
											</select>
                            </td>
                        </tr>
                         <tr>
                            <td>
                               Age of Property :<input type = "text" name = "inproperty" id = "inproperty"  size="7">
                            </td>
                        </tr>
                       
                       
                        <tr>
                            <td>
                              Contract Type: <select id="contype">
                               <option value="0">choose</option>
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  
											</select>
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                               Schedule of Payment: <select id="spayment">
                                <option value="0">choose</option>
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  <option value="0.6">1/2 yearly</option>
												 
											</select>
                            </td>
                        </tr>
                <!--         <tr class="active">
                            <td>
                                100MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr> -->
                    </table>
                </div>
                <div class="panel-footer">
                      <a href="" class="btn btn-success" role="button" data-toggle="modal" data-target="#myModal">Proceed</a>
                   </div>
            </div>
        </div>
       <!--  <div class="col-xs-12 col-md-3">
            <div class="panel panel-success">
           
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Silver</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1>
                            Rs.20<span class="subscript">/mo</span></h1>
                        <small>1 month FREE trial</small>
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                                2 Account
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                5 Project
                            </td>
                        </tr>
                        <tr>
                            <td>
                                100K API Access
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                200MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-footer">
                    <a href="" class="btn btn-success" role="button">Proceed</a>
                   </div>
            </div>
        </div> -->
     <!--    <div class="col-xs-12 col-md-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Gold</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1>
                            Rs.35<span class="subscript">/mo</span></h1>
                        <small>1 month FREE trial</small>
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                                5 Account
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                20 Project
                            </td>
                        </tr>
                        <tr>
                            <td>
                                300K API Access
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                500MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-footer">
                      <a href="" class="btn btn-success" role="button">Proceed</a>
                      </div>
            </div>
        </div> -->
    </div>
</div>



                                        </div>


            <div role="tabpanel" class="tab-pane" id="gated">
                                        		<div class="container">
     <div class="row">

         <div class="col-xs-12 col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Industrial+Community</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1 id="gvalue">
                            Rs0.00<span class="subscript">/month</span></h1>
                    
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                              Number of flats :<input type = "text"  id="gnooflats" size="7">
                            </td>
                        </tr>
                        <tr>
                            <td>
                               Age of Property :<input type = "text" id = "gpropAge"  size="7">
                            </td>
                        </tr>
                        <tr>
                            <td>
                              Contract Type: <select id="gcontractType">
                                                     <option value="0">Choose</option>
                              
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  
											</select>
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                               Schedule of Payment: <select id="gschedultyp">
                                 <option value="0">choose</option>
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  <option value="0.6">1/2 yearly</option>
												 
											</select>
                            </td>
                        </tr>
                <!--         <tr class="active">
                            <td>
                                100MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr> -->
                    </table>
                </div>
                <div class="panel-footer">
                       <a href="" class="btn btn-success" role="button" data-toggle="modal" data-target="#myModal">Proceed</a>
                   </div>
            </div>
        </div>
         <div class="col-xs-12 col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Industrial+Community</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1 id="Egvalue">
                            Rs0.00<span class="subscript">/month</span></h1>
                     
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                              Number of flats: <select  id="gflatno_external">
                                                  <option value="0">choose</option>
												  <option value="500">1-10</option>
												  <option value="750">10-20</option>
												  <option value="1000">20-30</option>
												  <option value="1250">30-40</option>
												  <option value="1500">40-50</option>
												
												  
											</select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               Age of Property :<input type = "text" name = "gEx_propAge" id = "gEx_propAge"  size="7">
                            </td>
                        </tr>
                        <tr>
                            <td>
                              Contract Type: <select id="gcontractype_external">
                                                     <option value="0">Choose</option>
                              
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  
											</select>
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                               Schedule of Payment: <select id="gpaymentPlan">
                                 <option value="0">choose</option>
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  <option value="0.6">1/2 yearly</option>
												 
											</select>
                            </td>
                        </tr>
                <!--         <tr class="active">
                            <td>
                                100MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr> -->
                    </table>
                </div>
                <div class="panel-footer">
                      <a href="" class="btn btn-success" role="button" data-toggle="modal" data-target="#myModal">Proceed</a>
                   </div>
            </div>
        </div>
        
         <div class="col-xs-12 col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Industrial+Community</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1 id="igvalue">
                            Rs0.00<span class="subscript">/month</span></h1>
                      
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                             Type of flat: <select id="gflattyp_internal">
												 
                                                  <option value="0">choose</option>
												  <option value="50">1BHK</option>
												  <option value="75">2BHK</option>
												  <option value="100">3BHK</option>
												  <option value="125">4BHK</option>
											
												  
											</select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               Age of Property :<input type = "text" name = "gIx_propAge" id = "gIx_propAge"  size="7">
                            </td>
                        </tr>
                        <tr>
                            <td>
                              Contract Type: <select id="gcontracttype_internal">
                                                     <option value="0">Choose</option>
                              
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  
											</select>
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                               Schedule of Payment: <select id="gipaymentPlan">
                                 <option value="0">choose</option>
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  <option value="0.6">1/2 yearly</option>
												 
											</select>
                            </td>
                        </tr>
                <!--         <tr class="active">
                            <td>
                                100MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr> -->
                    </table>
                </div>
                <div class="panel-footer">
                      <a href="" class="btn btn-success" role="button" data-toggle="modal" data-target="#myModal">Proceed</a>
                   </div>
            </div>
        </div>



    </div>
</div>



                                        </div>

                                </div>
	</div>
</div>


  
<!-- <div class="container height">
	<div class = "col-md-7">
		 

	  <h2>Why Choose <span style="color:#146eb4;">AMC ?</span></h2>
	
		    <div class="row cont">
		        <div class="col-xs-12 col-md-6">
		            <i class="fa fa-check-square-o" aria-hidden="true" style="color: #146eb4"></i> &nbsp;High end cleaning machinery
		        </div>
		        <div class="col-xs-12 col-md-6">
		             <i class="fa fa-check-square-o" aria-hidden="true" style="color: #146eb4"></i> &nbsp;High end cleaning machinery
		        </div>
		    </div>

		        <div class="row cont">
		        <div class="col-xs-12 col-md-6">
		            <i class="fa fa-check-square-o" aria-hidden="true" style="color: #146eb4"></i> &nbsp;High end cleaning machinery
		        </div>
		        <div class="col-xs-12 col-md-6">
		             <i class="fa fa-check-square-o" aria-hidden="true" style="color: #146eb4"></i> &nbsp;High end cleaning machinery
		        </div>
		    </div>
		
	  </div> 

	

 	<div class = "col-md-5">
		<h2 style="text-align: center;">Book a <span style="color:#146eb4;">Electrician</span> Now !</h2>

		
		
				<div class="main-login main-center">
				<h5>Sign up once and watch any of our free demos.</h5>
					<form class="" method="post" action="#">
						
			

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Email</label>
							<div class="cols-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="email" id="email"  placeholder="Enter your Email"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Username</label>
							<div class="cols-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="username" id="username"  placeholder="Enter your Username"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-mobile" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
								</div>
							</div>
						</div>

									<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Choose Work</label>
							<div class="cols-sm-6">
								<div class="input-group">
								 <form>
								    <label class="checkbox-inline">
								      <input type="checkbox" value="">AMC
								    </label>
								    <label class="checkbox-inline">
								      <input type="checkbox" value="">Repair
								    </label>
								    <label class="checkbox-inline">
								      <input type="checkbox" value="">New Work
								    </label>
								  </form>
								</div>
							</div>
						</div>

							<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Type of Property</label>
							<div class="cols-sm-6">
								<div class="input-group form-control">
								 <form>
								    <label class="radio">
								      <input type="radio" value="" name="optradio">Apartment / Group House
								    </label>
								    <label class="radio">
								      <input type="radio" value="" name="optradio">Gated Community
								    </label>
								      <label class="radio">
								      <input type="radio" value="" name="optradio">Commercial
								    </label>
								      <label class="radio">
								      <input type="radio" value="" name="optradio">Individual
								    </label>
								   
								  </form>
									<!-- <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="name" id="name"  placeholder="Enter your Name"/> -->
								<!-- </div>
							</div>
						</div>
				

						<div class="form-group ">
							<a href="common.html" target="_blank" type="button" id="button" class="btn btn-primary btn-lg btn-block login-button">Submit</a>
						</div>
						
					</form>
				</div>
	
		
	</div> 
</div> --> 
	

<!--  <footer id="myFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h5>Get started</h5>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Sign up</a></li>
                        <li><a href="#">Downloads</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>About us</h5>
                    <ul>
                        <li><a href="#">Company Information</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Reviews</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>Support</h5>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Help desk</a></li>
                        <li><a href="#">Forums</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>Legal</h5>
                    <ul>
                        <li><a href="#">Terms of Service</a></li>
                        <li><a href="#">Terms of Use</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <!-- Here we use the Google Embed API to show Google Maps. -->
            <!-- In order for this to work in your project you will need to generate a unique API key.  -->
<!--             <iframe id="map-container" frameborder="0" style="border:0"
                src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJOwg_06VPwokRYv534QaPC8g&key=AIzaSyBdJm9Amm4KALkKlZObWn40dcpRyH119zg" >
            </iframe>
        </div>
        <div class="social-networks">
            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
            <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
        </div>
        <div class="footer-copyright">
            <p>� 2016 Copyright Text </p>
        </div>
    </footer>  -->
    
    


</body>
</head>
</html>