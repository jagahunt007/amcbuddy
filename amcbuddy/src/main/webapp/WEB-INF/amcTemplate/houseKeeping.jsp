<!DOCTYPE html>
<html lang="en">
<head>
<title>AMC : Annual Maintance Contract</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Innovate Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="amcDesign/styles/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="amcDesign/styles/font-awesome.css" rel="stylesheet"> 
<link rel="stylesheet" href="amcDesign/styles/Footer-with-map.css">
<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
<script type='text/javascript' src='amcDesign/script/jquery-2.2.3.min.js'></script>
<script type='text/javascript' src='amcDesign/script/bootstrap.js'></script>
<style>
body, html{
   /*  height: 100%;
 	background-repeat: no-repeat;
 	background-color: #d3d3d3;*/
 	font-family: 'Oxygen', sans-serif;
}/**/
.navbar-default {
    background-color: #146eb4;
    border-color: #146eb4;
}
.logo
{
	color:white !important;
	font-size: 36px;
}
.navbar-default .navbar-nav > li > a
{
	color: white;
	line-height: 25px;
    font-size: 18px;
    letter-spacing: 0.5px;
    padding: 0 15px;
    margin: 10px;
}
.quoat
{
	font-size: 18px;
    color: #787878;
    line-height: 30px;
    padding: 10px 70px 0px 0;
}
.cont
{
	    padding: 12px 0px;
    text-align: left;
    font-size: 20px;
    color: #787878;
}

	   

.main{
 	margin:15px 15px;
}

label{
	margin-bottom: 15px;
}

/*input,
input::-webkit-input-placeholder {
    font-size: 11px;
    padding-top: 3px;
}*/

.main-login{
 	background-color: #fff;
    /* shadows and rounded borders */
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);

}
.form-control {
    height: auto!important;
padding: 8px 12px !important;
}


.main-center{
 	
    padding: 10px 40px;
	
box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);

}
span.input-group-addon i {
    color: #009edf;
    font-size: 17px;
}

.login-button{
	margin-top: 5px;
}

.login-register{
	font-size: 11px;
	text-align: center;
}
.nav-tabs { border-bottom: 2px solid #DDD; }
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
    .nav-tabs > li > a { border: none; color: #666; }
        .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #4285F4 !important; background: transparent; }
        .nav-tabs > li > a::after { content: ""; background: #4285F4; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
    .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
.tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
.tab-pane { padding: 15px 0; }
.tab-content{padding:20px}

.panel
{
    text-align: center;
}
.panel:hover { box-shadow: 0 1px 5px rgba(0, 0, 0, 0.4), 0 1px 5px rgba(130, 130, 130, 0.35); }
.panel-body
{
    padding: 0px;
    text-align: center;
}

.the-price
{
    background-color: rgba(220,220,220,.17);
    box-shadow: 0 1px 0 #dcdcdc, inset 0 1px 0 #fff;
    padding: 20px;
    margin: 0;
}

.the-price h1
{
    line-height: 1em;
    padding: 0;
    margin: 0;
}

.subscript
{
    font-size: 25px;
}

/* CSS-only ribbon styles    */
.cnrflash
{
    /*Position correctly within container*/
    position: absolute;
    top: -9px;
    right: 4px;
    z-index: 1; /*Set overflow to hidden, to mask inner square*/
    overflow: hidden; /*Set size and add subtle rounding  		to soften edges*/
    width: 100px;
    height: 100px;
    border-radius: 3px 5px 3px 0;
}
.cnrflash-inner
{
    /*Set position, make larger then 			container and rotate 45 degrees*/
    position: absolute;
    bottom: 0;
    right: 0;
    width: 145px;
    height: 145px;
    -ms-transform: rotate(45deg); /* IE 9 */
    -o-transform: rotate(45deg); /* Opera */
    -moz-transform: rotate(45deg); /* Firefox */
    -webkit-transform: rotate(45deg); /* Safari and Chrome */
    -webkit-transform-origin: 100% 100%; /*Purely decorative effects to add texture and stuff*/ /* Safari and Chrome */
    -ms-transform-origin: 100% 100%;  /* IE 9 */
    -o-transform-origin: 100% 100%; /* Opera */
    -moz-transform-origin: 100% 100%; /* Firefox */
    background-image: linear-gradient(90deg, transparent 50%, rgba(255,255,255,.1) 50%), linear-gradient(0deg, transparent 0%, rgba(1,1,1,.2) 50%);
    background-size: 4px,auto, auto,auto;
    background-color: #aa0101;
    box-shadow: 0 3px 3px 0 rgba(1,1,1,.5), 0 1px 0 0 rgba(1,1,1,.5), inset 0 -1px 8px 0 rgba(255,255,255,.3), inset 0 -1px 0 0 rgba(255,255,255,.2);
}
.cnrflash-inner:before, .cnrflash-inner:after
{
    /*Use the border triangle trick to make  				it look like the ribbon wraps round it's 				container*/
    content: " ";
    display: block;
    position: absolute;
    bottom: -16px;
    width: 0;
    height: 0;
    border: 8px solid #800000;
}
.cnrflash-inner:before
{
    left: 1px;
    border-bottom-color: transparent;
    border-right-color: transparent;
}
.cnrflash-inner:after
{
    right: 0;
    border-bottom-color: transparent;
    border-left-color: transparent;
}
.cnrflash-label
{
    /*Make the label look nice*/
    position: absolute;
    bottom: 0;
    left: 0;
    display: block;
    width: 100%;
    padding-bottom: 5px;
    color: #fff;
    text-shadow: 0 1px 1px rgba(1,1,1,.8);
    font-size: 0.95em;
    font-weight: bold;
    text-align: center;
}

</style>
<body>
   <script>
$(document).ready(function(){
   var wflatnooo=0;
var cnoflat=0;
var wctype=0;
var nyrs=0; 


 
/* $("#pest_sft").change(function(){
	cflatnooo = parseFloat($(this).val());
        $("#sftvalue").html(cflatnooo+"/month");
      
    });  */
    
    
    
     $("#hk_sft").change(function(){
    	 wflatnooo=parseInt($(this).val())*1.50;
    	 $("#displayValue").html(wflatnooo.toFixed(2)+"/month");
      
    }); 
    
    $("#hk_contractType").change(function(){
    	
    	nyrs =	$(this).val();
    	
 if(nyrs==3)
 {
	 wctype=wflatnooo*3*12;
  $("#displayValue").html(wctype.toFixed(2)+"/3years");
 }
 if(nyrs==1)
 {
	 wctype=wflatnooo*1*12;
  $("#displayValue").html(wctype.toFixed(2)+"/1year");
 }
 
 
    });
     
    
    
    
    
     $("#hk_schedultyp").change(function(){
    
        
   	 if(nyrs ==3){     
        if($(this).val() == 3){
       
        
        	wctype=  wflatnooo*3*12;
       
       $("#displayValue").html(wctype.toFixed(2)+"/3years");
        }
        if($(this).val() == 1){
        	wctype = wflatnooo*12;
      
        $("#displayValue").html(wctype.toFixed(2)+"/1year");
        
        }
        if($(this).val() == 0.6){
        	wctype = (wflatnooo*6);
        
        $("#displayValue").html(wctype.toFixed(2)+"/half year");
        
        }
   	 } 
   	 if(nyrs ==1){  
   		 
   		 
   		 
   		 if($(this).val() == 1){
   			wctype = wflatnooo*12;
   	       
   	         $("#displayValue").html(wctype.toFixed(2)+"/year");
   	         
   	         }
   	         if($(this).val() == 0.6){
   	        	wctype = (wflatnooo*6);
   	         
   	         $("#displayValue").html(wctype.toFixed(2)+"/half year");
   	         
   	         }	 
   		 
   		 
   		 
   	 }
   	 
   	 
   	 
   	 
   	 
   	 
     
    });
    
     
    
    
});
</script>
    
    
    
    
    
    
    
    
<nav class="navbar navbar-default" role="navigation">
      <div class="container ">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand logo" href="#" >AMC</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
      
      <ul class="nav navbar-nav navbar-right">
          <li class=""><a href="#">Home</a></li>
        <li><a href="#">About Us</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services</a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
        <li><a href="#">Portfolio</a></li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Login</a>
   
        </li>
        
        
      </ul>
    </div><!-- /.navbar-collapse -->
      </div><!-- /.container-collapse -->
  </nav>

<div class="container">

 

	<div class="row">
		                                <div class="col-md-9">
                                    <!-- Nav tabs --><div class="card">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#Apartment" aria-controls="home" role="tab" data-toggle="tab" style="font-size: 20px;padding: 20px;">Apartment</a></li>
                                       
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="Apartment">
                                        					<div class="container">
    <div class="row">

         <div class="col-xs-12 col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        commercial & individual</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1 id="displayValue">
                            Rs00<span class="subscript">/mo</span></h1>
                        
                    </div>
                    <table class="table">
                    <tr>
                    <h3>Rs 1.50/sft/month</h3>
                    
                         </tr>
                        <tr>
                            <td>
                              total sft :<input type = "text" name = "hk_sft" id="hk_sft" size="7">
                            </td>
                        </tr>
                       
                        <tr>
                            <td>
                              Contract Type: <select id="hk_contractType">
                                                     <option value="0">Choose</option>
                              
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  
											</select>
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                               Schedule of Payment: <select id="hk_schedultyp">
                                 <option value="0">choose</option>
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  
												 
											</select>
                            </td>
                        </tr>
                <!--         <tr class="active">
                            <td>
                                100MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr> -->
                    </table>
                </div>
                <div class="panel-footer">
                     <a href="" class="btn btn-success" role="button">Proceed</a>
                   </div>
            </div>
        </div>



    </div>
</div>



                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="Individual">
                                        		<div class="container">
    <div class="row">

         <div class="col-xs-12 col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Bronze</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1>
                            Rs.10<span class="subscript">/mo</span></h1>
                        <small>1 month FREE trial</small>
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                              Number of flats :<input type = "text" name = "nooflats" id="innooflats" size="7">
                            </td>
                        </tr>
                        <tr>
                            <td>
                               Age of Property :<input type = "text" name = "propAge" id = "inpropAge"  size="7">
                            </td>
                        </tr>
                        <tr>
                            <td>
                              Contract Type: <select id="incontractType">
                                                     <option value="0">Choose</option>
                              
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  
											</select>
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                               Schedule of Payment: <select id="inschedultyp">
                                 <option value="0">choose</option>
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  <option value="0.6">1/2 yearly</option>
												 
											</select>
                            </td>
                        </tr>
                <!--         <tr class="active">
                            <td>
                                100MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr> -->
                    </table>
                </div>
                <div class="panel-footer">
                     <a href="" class="btn btn-success" role="button">Proceed</a>
                   </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-3">
            <div class="panel panel-success">
           
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Silver</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1>
                            Rs.20<span class="subscript">/mo</span></h1>
                        <small>1 month FREE trial</small>
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                                2 Account
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                5 Project
                            </td>
                        </tr>
                        <tr>
                            <td>
                                100K API Access
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                200MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-footer">
                    <a href="" class="btn btn-success" role="button">Proceed</a>
                   </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Gold</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1>
                            Rs.35<span class="subscript">/mo</span></h1>
                        <small>1 month FREE trial</small>
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                                5 Account
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                20 Project
                            </td>
                        </tr>
                        <tr>
                            <td>
                                300K API Access
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                500MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-footer">
                      <a href="" class="btn btn-success" role="button">Proceed</a>
                      </div>
            </div>
        </div>
    </div>
</div>



                                        </div>


            <div role="tabpanel" class="tab-pane" id="gated">
                                        		<div class="container">
    <div class="row">

         <div class="col-xs-12 col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Bronze</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1>
                            Rs.10<span class="subscript">/mo</span></h1>
                        <small>1 month FREE trial</small>
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                              Number of flats :<input type = "text" name = "nooflats" id="innooflats" size="7">
                            </td>
                        </tr>
                        <tr>
                            <td>
                               Age of Property :<input type = "text" name = "propAge" id = "inpropAge"  size="7">
                            </td>
                        </tr>
                        <tr>
                            <td>
                              Contract Type: <select id="incontractType">
                                                     <option value="0">Choose</option>
                              
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  
											</select>
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                               Schedule of Payment: <select id="inschedultyp">
                                 <option value="0">choose</option>
												  <option value="3">3years</option>
												  <option value="1">1year</option>
												  <option value="0.6">1/2 yearly</option>
												 
											</select>
                            </td>
                        </tr>
                <!--         <tr class="active">
                            <td>
                                100MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr> -->
                    </table>
                </div>
                <div class="panel-footer">
                     <a href="" class="btn btn-success" role="button">Proceed</a>
                   </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-3">
            <div class="panel panel-success">
           
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Silver</h3>
                </div>
                <div class="panel-body">
                    <div class="the-price">
                        <h1>
                            Rs.20<span class="subscript">/mo</span></h1>
                        <small>1 month FREE trial</small>
                    </div>
                    <table class="table">
                        <tr>
                            <td>
                                2 Account
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                5 Project
                            </td>
                        </tr>
                        <tr>
                            <td>
                                100K API Access
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                200MB Storage
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Custom Cloud Services
                            </td>
                        </tr>
                        <tr class="active">
                            <td>
                                Weekly Reports
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-footer">
                    <a href="" class="btn btn-success" role="button">Proceed</a>
                   </div>
            </div>
        </div>
     
    </div>
</div>



                                        </div>

                                </div>
	</div>
</div>


  
<!-- <div class="container height">
	<div class = "col-md-7">
		 

	  <h2>Why Choose <span style="color:#146eb4;">AMC ?</span></h2>
	
		    <div class="row cont">
		        <div class="col-xs-12 col-md-6">
		            <i class="fa fa-check-square-o" aria-hidden="true" style="color: #146eb4"></i> &nbsp;High end cleaning machinery
		        </div>
		        <div class="col-xs-12 col-md-6">
		             <i class="fa fa-check-square-o" aria-hidden="true" style="color: #146eb4"></i> &nbsp;High end cleaning machinery
		        </div>
		    </div>

		        <div class="row cont">
		        <div class="col-xs-12 col-md-6">
		            <i class="fa fa-check-square-o" aria-hidden="true" style="color: #146eb4"></i> &nbsp;High end cleaning machinery
		        </div>
		        <div class="col-xs-12 col-md-6">
		             <i class="fa fa-check-square-o" aria-hidden="true" style="color: #146eb4"></i> &nbsp;High end cleaning machinery
		        </div>
		    </div>
		
	  </div> 

	

 	<div class = "col-md-5">
		<h2 style="text-align: center;">Book a <span style="color:#146eb4;">Electrician</span> Now !</h2>

		
		
				<div class="main-login main-center">
				<h5>Sign up once and watch any of our free demos.</h5>
					<form class="" method="post" action="#">
						
			

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Email</label>
							<div class="cols-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="email" id="email"  placeholder="Enter your Email"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Username</label>
							<div class="cols-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="username" id="username"  placeholder="Enter your Username"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-mobile" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
								</div>
							</div>
						</div>

									<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Choose Work</label>
							<div class="cols-sm-6">
								<div class="input-group">
								 <form>
								    <label class="checkbox-inline">
								      <input type="checkbox" value="">AMC
								    </label>
								    <label class="checkbox-inline">
								      <input type="checkbox" value="">Repair
								    </label>
								    <label class="checkbox-inline">
								      <input type="checkbox" value="">New Work
								    </label>
								  </form>
								</div>
							</div>
						</div>

							<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Type of Property</label>
							<div class="cols-sm-6">
								<div class="input-group form-control">
								 <form>
								    <label class="radio">
								      <input type="radio" value="" name="optradio">Apartment / Group House
								    </label>
								    <label class="radio">
								      <input type="radio" value="" name="optradio">Gated Community
								    </label>
								      <label class="radio">
								      <input type="radio" value="" name="optradio">Commercial
								    </label>
								      <label class="radio">
								      <input type="radio" value="" name="optradio">Individual
								    </label>
								   
								  </form>
									<!-- <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="name" id="name"  placeholder="Enter your Name"/> -->
								<!-- </div>
							</div>
						</div>
				

						<div class="form-group ">
							<a href="common.html" target="_blank" type="button" id="button" class="btn btn-primary btn-lg btn-block login-button">Submit</a>
						</div>
						
					</form>
				</div>
	
		
	</div> 
</div> --> 
	

<!--  <footer id="myFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h5>Get started</h5>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Sign up</a></li>
                        <li><a href="#">Downloads</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>About us</h5>
                    <ul>
                        <li><a href="#">Company Information</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Reviews</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>Support</h5>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Help desk</a></li>
                        <li><a href="#">Forums</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>Legal</h5>
                    <ul>
                        <li><a href="#">Terms of Service</a></li>
                        <li><a href="#">Terms of Use</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <!-- Here we use the Google Embed API to show Google Maps. -->
            <!-- In order for this to work in your project you will need to generate a unique API key.  -->
<!--             <iframe id="map-container" frameborder="0" style="border:0"
                src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJOwg_06VPwokRYv534QaPC8g&key=AIzaSyBdJm9Amm4KALkKlZObWn40dcpRyH119zg" >
            </iframe>
        </div>
        <div class="social-networks">
            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
            <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
        </div>
        <div class="footer-copyright">
            <p>� 2016 Copyright Text </p>
        </div>
    </footer>  -->

</body>
</head>
</html>