
<!DOCTYPE HTML>
<html>
<head>
<title>AMC Module Plans</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="amcDesign/styles/Pricingstyle.css" rel="stylesheet" type="text/css" media="all"/>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		
		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="amcDesign/styles/Footer-with-map.css">
<!-- Custom Theme files -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta name="keywords" content="Clean Plans and Pricing Tables  Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!--web-fonts-->
<script>
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
<style type="text/css">
	/*
/* Created by Filipe Pina
 * Specific styles of signin, register, component
 */
/*
 * General styles
 */

body, html{
     height: 100%;
 	background-repeat: no-repeat;
 	font-family: 'Oxygen', sans-serif;
}

.navbar-default {
    background-color: #146eb4;
    border-color: #146eb4;
}
.logo
{
	color:white !important;
	font-size: 36px;
}
.navbar-default .navbar-nav > li > a
{
	color: white;
	line-height: 25px;
    font-size: 18px;
    letter-spacing: 0.5px;
    padding: 0 15px;
    margin: 10px;
}


h1.title { 
	font-size: 50px;
	font-family: 'Passion One', cursive; 
	font-weight: 400; 
}

	hr{
		width: 10%;
		color: #fff;
	}

.form-group{
	margin-bottom: 15px;
}

label{
	margin-bottom: 15px;
}

input,
input::-webkit-input-placeholder {
    font-size: 11px;
    padding-top: 3px;
}

.main-login{
 	background-color: #fff;
    /* shadows and rounded borders */
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    /*-moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);*/
   /* -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);*/

}

.main-center{
 	margin-top: 30px;
 	margin: 0 auto;
    padding: 40px 40px;

}

.login-button{
	margin-top: 5px;
}

.login-register{
	font-size: 11px;
	text-align: center;
}

.styleColor{
    background: #009edf;
    text-align: center;
    font-size: 34px;
    color: white;

}
.attr{
	color: #c10841 !important;
}
#price{
	color:  #c10841 !important;
}
.data ul li {
	color:  #c10841 !important;
}
.dataBtn
{
	background-color:  #c10841 !important;
	border-color:  #c10841 !important;
}

.attr1{
	color: #0098ca !important;
}
#price1{
	color:  #0098ca !important;
}
.data1 ul li {
	color:  #0098ca !important;
}
.dataBtn1  
{
	background-color:  #0098ca !important;
	border-color:  #0098ca !important;
}
.attr2{
	color: #738d00 !important;
}
#price2{
	color:  #738d00 !important;
}
.data2 ul li {
	color:  #738d00 !important;
}
.dataBtn2
{
	background-color:  #738d00 !important;
	border-color:  #738d00 !important;
}
.attr3{
	color: #c10841 !important;
}
#price3{
	color:  #c10841 !important;
}
.data3 ul li {
	color:  #c10841 !important;
}
.dataBtn3
{
	background-color:  #c10841 !important;
	border-color:  #c10841 !important;
}
}
 .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }

</style>
</head>
<body>

<nav class="navbar navbar-default" role="navigation">
      <div class="container ">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand logo" href="#" >AMC</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
      
      <ul class="nav navbar-nav navbar-right">
          <li class=""><a href="#">Home</a></li>
        <li><a href="#">About Us</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services</a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
        <li><a href="#">Portfolio</a></li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Login</a>
   
        </li>
        
        
      </ul>
    </div><!-- /.navbar-collapse -->
      </div><!-- /.container-collapse -->
  </nav>

<div class="modal fade" id="myRegister" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header styleColor">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title ">Registration</h4>
        </div>
        <div class="modal-body">
				<div class="main-login main-center">
					<form class="form-horizontal" method="post" action="#">
						
						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Apartment Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-building-o fa-lg" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="name" id="name"  placeholder="Enter your Name"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Location</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="email" id="email"  placeholder="Enter your Email"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Address</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-location-arrow fa-lg" aria-hidden="true"></i></span>
									<textarea class="form-control" rows="5" id="comment"></textarea>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">No.of Flats</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-list-ol fa-lg" aria-hidden="true"></i></span>
									<input type="number" class="form-control" name="password" id="password"  placeholder="Enter your No.of Flats"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Plan Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-tasks fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Amount to be Paid</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-money fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password"/>
								</div>
							</div>
						</div>

						<div class="form-group text-center">
							<a href = "plans.html"><button type="button" class="btn btn-primary btn-lg">Submit</button></a>
						</div>
						<!-- <div class="login-register">
				            <a href="index.php">Login</a>
				         </div> -->
					</form>
				</div>
		
	
        </div>
     
      </div>
      
    </div>
  </div>

<div class = "container">
   <div class="well row">
        <div class="btn-group btn-breadcrumb">
            <a href="#" class="btn btn-default"><i class="glyphicon glyphicon-home"></i></a>
            <a href="#" class="btn btn-default">Plumbing & Electrcian</a>
            <a href="#" class="btn btn-default">NewWork</a>
            <a href="#" class="btn btn-default">Apartment</a>

        </div>
	</div>
</div>
		<div class="header">
		 <h2 style="font-size: 30px;">AMC <span style="color:#146eb4;">Clean Plans & Pricing Tables</span> </h2>
		</div>


		<div class="main">
			<div class="pricing-table">
				<div class="pricing">
					<div class="price-top">
						<h2 class="attr">For 3ys Appartment</h2>
					</div>
					<div class="price-bottom data">
						<h3 id = "price">Rs.100</h3>
						<p>per flat</p>
						<ul>
							<li><i class="fa fa-check"></i> &nbsp;Below <b>5years</b> </li>
							<br>
							<li><i class="fa fa-check"></i> &nbsp;GST Inclusive</li>
							<br>
							<li><i class="fa fa-check"></i> &nbsp;<b>Discount</li>
							<br>
							
						</ul>
						<button type="button" class="btn btn-info dataBtn" data-toggle="modal" data-target="#myRegister">Sign Up</button>
					</div>
				</div>
				<div class="pricing">
					<div class="price-top top2">
						<h2 class="attr1">For 3ys Appartment</h2>
					</div>
					<div class="price-bottom data1">
						<h3 id = "price1">X X X </h3>
						
						<ul>
							<li><i class="fa fa-check"></i> &nbsp;Above <b>5years</b> </li>
							<br>
							<li><i class="fa fa-check"></i> &nbsp;GST Inclusive</li>
							<br>
							<li><i class="fa fa-check"></i> &nbsp;<b>Discount</li>
							<br>
						</ul>
						<button type="button" class="btn btn-info dataBtn1" data-toggle="modal" data-target="#myRegister">Sign Up</button>
					</div>
				</div>
				<div class="pricing">
					<div class="price-top top3">
						<h2 class="attr2">For 1ys Appartment</h2>
					</div>
					<div class="price-bottom data2">
						<h3 id = "price2">Rs.100</h3>
						<p>per flat</p>
						<ul>
							<li><i class="fa fa-check"></i> &nbsp;Below <b>5years</b> </li>
							<br>
							<li><i class="fa fa-check"></i> &nbsp;GST Inclusive</li>
							<br>
							<li><i class="fa fa-check"></i> &nbsp;<b>Discount</li>
							<br>
						</ul>
						<button type="button" class="btn btn-info dataBtn2" data-toggle="modal" data-target="#myRegister">Sign Up</button>
					</div>
				</div>
				<div class="pricing">
					<div class="price-top top4">
						<h2 class="attr3">For 1ys Appartment</h2>
					</div>
					<div class="price-bottom data3">
						<h3 id = "price3">X X X</h3>
						
						<ul>
							<li><i class="fa fa-check"></i> &nbsp;Above <b>5years</b> </li>
							<br>
							<li><i class="fa fa-check"></i> &nbsp;GST Inclusive</li>
							<br>
							<li><i class="fa fa-check"></i> &nbsp;<b>Discount</li>
							<br>
						</ul>
						<button type="button" class="btn btn-info dataBtn3" data-toggle="modal" data-target="#myRegister">Sign Up</button>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			
		</div>	

		<div class="container demo">

    
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class="more-less glyphicon glyphicon-plus"></i>
                        Select Payment Method
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                      <form>
    <label class="radio-inline">
      <input type="radio" name="optradio">3 Years
    </label>
    <label class="radio-inline">
      <input type="radio" name="optradio">1 Year
    </label>
    <label class="radio-inline">
      <input type="radio" name="optradio">Half Year
    </label>
    <label class="radio-inline">
      <input type="radio" name="optradio">Quarterly
    </label>
    <label class="radio-inline">
      <input type="radio" name="optradio">Monthly
    </label>
  </form>
                </div>
            </div>
        </div>

     <div class="well">Generate coupon for next service : #JHCODE123
    <span style="float: right;"> Total Amount to be piad : INR  12345</span>
     </div>
 <div class="well"> </div>


    </div><!-- panel-group -->
    
    
</div><
	 <footer id="myFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h5>Get started</h5>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Sign up</a></li>
                        <li><a href="#">Downloads</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>About us</h5>
                    <ul>
                        <li><a href="#">Company Information</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Reviews</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>Support</h5>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Help desk</a></li>
                        <li><a href="#">Forums</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h5>Legal</h5>
                    <ul>
                        <li><a href="#">Terms of Service</a></li>
                        <li><a href="#">Terms of Use</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <!-- Here we use the Google Embed API to show Google Maps. -->
            <!-- In order for this to work in your project you will need to generate a unique API key.  -->
            <iframe id="map-container" frameborder="0" style="border:0"
                src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJOwg_06VPwokRYv534QaPC8g&key=AIzaSyBdJm9Amm4KALkKlZObWn40dcpRyH119zg" >
            </iframe>
        </div>
        <div class="social-networks">
            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
            <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
        </div>
        <div class="footer-copyright">
            <p>© 2016 Copyright Text </p>
        </div>
    </footer>

</body>
</html>