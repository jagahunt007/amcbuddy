package com.amcbuddy.daoImpl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.amcbuddy.command.ServiceRequestCommand;
import com.amcbuddy.dao.FrontendDao;
import com.amcbuddy.model.Registration;
import com.amcbuddy.model.RequestService;



@Repository
@Transactional

public class FrontendDaoImpl implements FrontendDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void saveForm(ServiceRequestCommand cmd) {
		
		System.out.println("daoiml........................"+cmd);
		// TODO Auto-generated method stub
		Registration reg = new Registration();
		reg.setEmail(cmd.getEmail());
		reg.setUsername(cmd.getUsername());
		reg.setPhonenumber(cmd.getPhonenumber());
		reg.setPincode(cmd.getPincode());
		reg.setDescription(cmd.getDescription());
		reg.setLocation(cmd.getLocation());
	    reg.setService_id(cmd.getService_id()); 
				
	int i=	(Integer) sessionFactory.openSession().save(reg);
	System.out.println("save reg..............."+i);
	RequestService serv = new RequestService();

	serv.setReg_id(i);
	serv.setService_id(cmd.getService_id());
	serv.setTotal_sqft(cmd.getTotal_sqft());
	serv.setTypeofproperty(cmd.getTypeofproperty());
	sessionFactory.openSession().save(serv);
		
	}

}
