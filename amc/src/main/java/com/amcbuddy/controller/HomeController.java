package com.amcbuddy.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.amcbuddy.command.ServiceRequestCommand;
import com.amcbuddy.service.AdminService;
import com.amcbuddy.service.FrontendService;


@Lazy
@Controller
public class HomeController {
	
	@Autowired
	private FrontendService frntService;
	
	@RequestMapping(value="homepageaaa")
	  public String homeModify(){  
		
		
	    return "homepage";  
	  }
	
	@RequestMapping(value="ElecandPlumb")
	  public String ElecandPlumb(){  
		
		
	    return "ElecandPlumb";  
	  }
	
	
	@RequestMapping(value="register")
	  public String reg_form(HttpServletRequest req){
		
		
	 int id =  Integer.parseInt(req.getParameter("serviceId"));
		
	 System.out.println(".test......................................."+id);
	    return "register";  
	  }
	
	
	
	@RequestMapping(value="first")
	  public String firstForm(){  
		
		
	    return "firstForm";  
	  }
	
	@RequestMapping(value="ratecard")
	public String ratecard()
	{
		return "ratecard";
	}
	@RequestMapping(value="form2")
	public String form2()
	{
		return "form2";
		
	}
	
	@RequestMapping(value="form3")
	public String form3()
	{
		return "form3";
		
	}
	@RequestMapping(value="form4")
	public String form4()
	{
		return "form4";
		
	}
	
	
	@RequestMapping(value="propManage")
	  public String propForm(){ 
		
		
		
	    return "PropertyForm";  
	  }
	
	@RequestMapping(value="savePropManage")
	  public String savepropForm(@ModelAttribute("propObj") ServiceRequestCommand cmd){ 
		
		int service_id = 4;
		cmd.setService_id(service_id);
		
		System.out.println("command.................."+cmd);
		
		frntService.saveForm(cmd);
		
		
	    return "homepage";  
	  }
	
	@RequestMapping(value="securityForm")
	  public String securityForm(){ 
		
		
		
	    return "securityForm";  
	  }
	
	@RequestMapping(value="saveSecurity")
	  public String saveSecurityForm(@ModelAttribute("propObj") ServiceRequestCommand cmd){ 
		
		int service_id = 6;
		cmd.setService_id(service_id);
		
		System.out.println("command.................."+cmd);
		
		frntService.saveForm(cmd);
		
		
	    return "homepage";  
	  }
	
	
	@RequestMapping(value="E&PCommercial")
	  public String elecPlumbComForm(){ 
		
		
		
	    return "E&PCommercial";  
	  }
	
	@RequestMapping(value="saveE&PCommercial")
	  public String SaveelecPlumbComForm(@ModelAttribute("propObj") ServiceRequestCommand cmd){ 
		
		int service_id = 1;
		cmd.setService_id(service_id);
		
		System.out.println("command.................."+cmd.getTypeofproperty());
		
		frntService.saveForm(cmd);
		
	    return "homepage";  
	  }
	

	@RequestMapping(value="PestCommercial")
	  public String pestForm(){ 
		
		
		
	    return "CommercialPestContrl";  
	  }
	
	
	
	@RequestMapping(value="savePestCom")
	  public String savePestCom(@ModelAttribute("propObj") ServiceRequestCommand cmd){ 
		
		int service_id = 3;
		cmd.setService_id(service_id);
		
		System.out.println("command.................."+cmd.getTypeofproperty());
		
		frntService.saveForm(cmd);
		
	    return "homepage";  
	  }
	
	

	@RequestMapping(value="plan")
	  public String planDetails(){ 
		
	    return "planDetail";  
	  }
	
	

}
