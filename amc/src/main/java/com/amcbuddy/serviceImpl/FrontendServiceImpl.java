package com.amcbuddy.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.amcbuddy.command.ServiceRequestCommand;
import com.amcbuddy.dao.FrontendDao;
import com.amcbuddy.service.FrontendService;


@Lazy
@Service
@Transactional
public class FrontendServiceImpl implements FrontendService{
	@Autowired
	private FrontendDao frntDao;

	@Override
	public void saveForm(ServiceRequestCommand cmd) {
		// TODO Auto-generated method stub
	System.out.println("frntDao.........................."+cmd);	
		   frntDao.saveForm(cmd);
		
	}

}
