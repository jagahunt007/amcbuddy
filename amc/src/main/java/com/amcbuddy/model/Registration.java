package com.amcbuddy.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;


	   @Entity  
	   @Table(name="amc_registration")
		public class Registration implements Serializable{
		
		@Id
		@Column(name="reg_id")
		@GenericGenerator(name = "generator", strategy = "increment")
		@GeneratedValue(strategy=GenerationType.AUTO)
		private int reg_id;
		
		
		private String username;
		private String email;
		private String phonenumber;
		private String adders;
		private int unique_id;
		private int service_id;
		



		private int pincode;
		private String location;
		private String description;
		
		
		public int getPincode() {
			return pincode;
		}
		public void setPincode(int pincode) {
			this.pincode = pincode;
		}
		public String getLocation() {
			return location;
		}
		public void setLocation(String location) {
			this.location = location;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}

		public int getService_id() {
			return service_id;
		}
		public void setService_id(int service_id) {
			this.service_id = service_id;
		}

		@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
		private LocalDateTime created_by;
		@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
		private LocalDateTime modified_by;
		
		private String followup_date;
		public String getFollowup_date() {
			return followup_date;
		}
		public void setFollowup_date(String followup_date) {
			this.followup_date = followup_date;
		}
	
	
		public LocalDateTime getCreated_by() {
			return created_by;
		}
		public void setCreated_by(LocalDateTime created_by) {
			this.created_by = created_by;
		}
		public LocalDateTime getModified_by() {
			return modified_by;
		}
		public void setModified_by(LocalDateTime modified_by) {
			this.modified_by = modified_by;
		}
		public int getReg_id() {
			return reg_id;
		}
		public void setReg_id(int reg_id) {
			this.reg_id = reg_id;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		
		
		public String getPhonenumber() {
			return phonenumber;
		}
		public void setPhonenumber(String phonenumber) {
			this.phonenumber = phonenumber;
		}
		public String getAdders() {
			return adders;
		}
		public void setAdders(String adders) {
			this.adders = adders;
		}
		public int getUnique_id() {
			return unique_id;
		}
		public void setUnique_id(int unique_id) {
			this.unique_id = unique_id;
		}



		@LazyCollection(LazyCollectionOption.FALSE)
		@OneToMany(targetEntity=RequestService.class,orphanRemoval = true,cascade=CascadeType.ALL)
		@JoinColumn(name="reg_id", referencedColumnName="reg_id")
		private List<RequestService> reqService=new ArrayList<RequestService>();
		public List<RequestService> getReqService() {
			return reqService;
		}
		public void setReqService(List<RequestService> reqService) {
			this.reqService = reqService;
		}
		@Override
		public String toString() {
			return "Registration [reg_id=" + reg_id + ", username=" + username
					+ ", email=" + email + ", phonenumber=" + phonenumber
					+ ", adders=" + adders + ", unique_id=" + unique_id
					+ ", created_by=" + created_by + ", modified_by="
					+ modified_by + ", followup_date=" + followup_date
					+ ", reqService=" + reqService + "]";
		}

	
	
	

}
