package com.amcbuddy.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="amc_requestservice")
public class RequestService implements Serializable {
	
	
	
	@Override
	public String toString() {
		return "RequestService [form_id=" + form_id + ", customer_id="
				+ reg_id + ", typeofproperty=" + typeofproperty
				+ ", total_sqft=" + total_sqft + ", service_id=" + service_id
				+ "]";
	}
	
	
	@Id
	@Column(name="form_id")
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int form_id;
	
	private int reg_id;
	
	private String typeofproperty;
	
	private int total_sqft;
	
	private int service_id;

	public int getForm_id() {
		return form_id;
	}

	public void setForm_id(int form_id) {
		this.form_id = form_id;
	}

	

	public int getReg_id() {
		return reg_id;
	}

	public void setReg_id(int reg_id) {
		this.reg_id = reg_id;
	}

	public String getTypeofproperty() {
		return typeofproperty;
	}

	public void setTypeofproperty(String typeofproperty) {
		this.typeofproperty = typeofproperty;
	}

	public int getTotal_sqft() {
		return total_sqft;
	}

	public void setTotal_sqft(int total_sqft) {
		this.total_sqft = total_sqft;
	}

	public int getService_id() {
		return service_id;
	}

	public void setService_id(int service_id) {
		this.service_id = service_id;
	}
	
	
	
	
	
	

}
