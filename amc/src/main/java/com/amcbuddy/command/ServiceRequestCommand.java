package com.amcbuddy.command;

public class ServiceRequestCommand {
	//request service table
	
	  private int service_id;
	  private int cust_id;
	  private String typeofproperty;
	  private int total_sqft;
	  private String location;
	  private int pincode;
	  private String address;
	  private String description;
	  
	
	
	public int getTotal_sqft() {
		return total_sqft;
	}
	public void setTotal_sqft(int total_sqft) {
		this.total_sqft = total_sqft;
	}
	public int getService_id() {
		return service_id;
	}
	public void setService_id(int service_id) {
		this.service_id = service_id;
	}
	public int getCust_id() {
		return cust_id;
	}
	public void setCust_id(int cust_id) {
		this.cust_id = cust_id;
	}
	public String getTypeofproperty() {
		return typeofproperty;
	}
	public void setTypeofproperty(String typeofproperty) {
		this.typeofproperty = typeofproperty;
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	  
	
	
//fields for register table values
	
	private String username;
	private String email;
	private String phonenumber;
	private String adders;
	private int unique_id;
	private int reg_id;



	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getAdders() {
		return adders;
	}
	public void setAdders(String adders) {
		this.adders = adders;
	}
	public int getUnique_id() {
		return unique_id;
	}
	public void setUnique_id(int unique_id) {
		this.unique_id = unique_id;
	}
	public int getReg_id() {
		return reg_id;
	}
	public void setReg_id(int reg_id) {
		this.reg_id = reg_id;
	}
	@Override
	public String toString() {
		return "ServiceRequestCommand [service_id=" + service_id + ", cust_id="
				+ cust_id + ", typeofproperty=" + typeofproperty
				+ ", total_sqft=" + total_sqft + ", location=" + location
				+ ", pincode=" + pincode + ", address=" + address
				+ ", description=" + description + ", username=" + username
				+ ", email=" + email + ", phonenumber=" + phonenumber
				+ ", adders=" + adders + ", unique_id=" + unique_id
				+ ", reg_id=" + reg_id + "]";
	}
	
	  
	  
	  
	  
	  

}
